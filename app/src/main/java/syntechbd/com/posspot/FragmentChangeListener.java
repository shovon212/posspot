package syntechbd.com.posspot;

import android.app.Fragment;

public interface FragmentChangeListener {

    public void replaceFragment(Fragment fragment);

}
