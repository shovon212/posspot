package syntechbd.com.posspot;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TabHost;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import syntechbd.com.posspot.app.DBHelper;
import syntechbd.com.posspot.modal.SalesInfoModal;

import static syntechbd.com.posspot.FragmentDiscount.PromotionId;
import static syntechbd.com.posspot.FragmentSelectCustomer.CustomersBalance;
import static syntechbd.com.posspot.FragmentSelectCustomer.CustomersPoints;
import static syntechbd.com.posspot.FragmentSelectCustomer.Name;
import static syntechbd.com.posspot.FragmentSelectCustomer.PhoneNo;


public class FragmentPayment extends Fragment {

    Spinner paymentBySpinner, CardTypeSpinner, BankTypeSpinner, MobileBankSpinnerSP;

    TextView cashTXT, cardNoTXT, cardPayTXT, cardTypeTXT, BankNameTV, TOTAL_AMMOUNT_TXT;
    TextView AmountMobileTV, MobileBankSpinnerTV, TransactionNoTV, ReceiverNoTV;
    TextView PaidAmountTextView, DueAmountTextView, RoundAmountTextView;

    TextView showMessageTV;

    EditText AmountMobileET, TransactionNoET, ReceiverNoET;
    EditText cashEDT, cardNoEDT, cardPayET;


    RelativeLayout CardTypeSpinnerLayout, BankTypeSpinnerLayout, MobileBankTypeSpinnerLayout;

    LinearLayout PaymentOptionLayoutLW, totalAmountLW, textEDtHolderLW;

    Switch round;

    private ArrayList<String> pamentBYList = new ArrayList<>();
    private ArrayList<String> cardTypeList = new ArrayList<>();

    private ArrayList<String> BankList = new ArrayList<>();
    private ArrayList<String> MobileBankList = new ArrayList<>();

    private ArrayList<String> BankDetails = new ArrayList<>();
    private ArrayList<String> MobileBankDetails = new ArrayList<>();
    private ArrayList<SalesInfoModal> salesInfoModals = new ArrayList<>();
    private ArrayList<SalesInfoModal> salesInfoModals2 = new ArrayList<>();


    private String bank_id;
    private String mobile_bank_id;
    private String bank_name;
    private String mobile_bank_name;
    private String acc_type_id;
    private String id_account;
    private String mobile_id_account;
    private String account_no;
    private String mobile_account_no = "";
    private String cashEDText;
    private String FinallyAmountPaidInCashEDT;
    private String GrandTotalToPay = "0";

    private String cash = "";
    private String card_cash = "";
    private String mob_cash = "";
    private String cash_acc_id = "1";


    private String original_product_priceTotal = "0";
    private Double org_product_priceTotal = 0.00;


    Button GenGrandTotalBTN, SaleNowBTN;

    DBHelper db;
    private int NumOfRow;
    private String card_type = "0";
    private String bank_nameForPost = "0";
    private String mobile_bank_nameForPost = "0";
    private String cardNoETString = "";
    private String transaction_no = "";
    private String payment_type = "cash";
    private String DueAmmountInTakaFinally;

    SharedPreferences pref;
    Context context;
    private String YourUserName;
    private String store_from;
    private String station_id;
    private String store_fromNew = "1";
    private String station_idNew = "1";


    public FragmentPayment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_payment, container, false);
        context = getContext();
        pref = context.getSharedPreferences("UserInfo", 0);
        YourUserName = pref.getString("user_name", null);
        cash_acc_id = pref.getString("station_acc_id", null);
        store_fromNew = pref.getString("store_id", null);
        station_idNew = pref.getString("station_id", null);

        loadAutoSuggestionDataForBANKtype("http://" + YourUserName + ".posspot.com/api/sales_api/bank_name_all", "1");

        paymentBySpinner = (Spinner) v.findViewById(R.id.paymentTypeSP);
        CardTypeSpinner = (Spinner) v.findViewById(R.id.cardTypeSp);
        cashTXT = (TextView) v.findViewById(R.id.cashTV);
        cashEDT = (EditText) v.findViewById(R.id.cashInTakaET);

        cardNoTXT = (TextView) v.findViewById(R.id.cardNumberTV);
        cardNoEDT = (EditText) v.findViewById(R.id.cardNumET);

        cardPayTXT = (TextView) v.findViewById(R.id.cardPayTV);
        cardPayET = (EditText) v.findViewById(R.id.cardPayET);

        BankNameTV = (TextView) v.findViewById(R.id.BankNameTV);
        BankTypeSpinner = (Spinner) v.findViewById(R.id.bankTypeSp);

        AmountMobileTV = (TextView) v.findViewById(R.id.AmountMobileTV);
        AmountMobileET = (EditText) v.findViewById(R.id.AmountMobileET);

        MobileBankSpinnerTV = (TextView) v.findViewById(R.id.MobileBankSpinnerTV);
        MobileBankSpinnerSP = (Spinner) v.findViewById(R.id.MobileBankSpinnerSP);

        TransactionNoTV = (TextView) v.findViewById(R.id.TransactionNoTV);
        TransactionNoET = (EditText) v.findViewById(R.id.TransactionNoET);

        ReceiverNoTV = (TextView) v.findViewById(R.id.ReceiverNoTV);
        ReceiverNoET = (EditText) v.findViewById(R.id.ReceiverNoET);

        cardTypeTXT = (TextView) v.findViewById(R.id.cardTypeTV);
        TOTAL_AMMOUNT_TXT = (TextView) v.findViewById(R.id.totalAmountTV);


        PaidAmountTextView = (TextView) v.findViewById(R.id.paidAmountTV);
        RoundAmountTextView = (TextView) v.findViewById(R.id.roundAmountTV);
        DueAmountTextView = (TextView) v.findViewById(R.id.totalDueTV);

        CardTypeSpinnerLayout = (RelativeLayout) v.findViewById(R.id.CardTypeSpinnerLayoutID);
        BankTypeSpinnerLayout = (RelativeLayout) v.findViewById(R.id.bankTypeSpLayoutID);
        MobileBankTypeSpinnerLayout = (RelativeLayout) v.findViewById(R.id.MobilebankTypeSpLayoutID);
        GenGrandTotalBTN = (Button) v.findViewById(R.id.GenGrandTotalBTN);
        SaleNowBTN = (Button) v.findViewById(R.id.SaleNowBTN);

        totalAmountLW = (LinearLayout) v.findViewById(R.id.totalAmountLW);
        textEDtHolderLW = (LinearLayout) v.findViewById(R.id.textEDtHolderLW);
        PaymentOptionLayoutLW = (LinearLayout) v.findViewById(R.id.PaymentOptionLayoutLW);

        showMessageTV = (TextView) v.findViewById(R.id.showMSG);


        round = (Switch) v.findViewById(R.id.roundSW);

        totalAmountLW.setVisibility(View.GONE);
        textEDtHolderLW.setVisibility(View.GONE);
        PaymentOptionLayoutLW.setVisibility(View.GONE);

        pamentBYList.clear();
        cardTypeList.clear();

        pamentBYList.add(0, "Cash");
        pamentBYList.add(1, "Card");
        pamentBYList.add(2, "Mobile");

        cardTypeList.add(0, "Select One");
        cardTypeList.add(1, "Amex");
        cardTypeList.add(2, "Mastercard");
        cardTypeList.add(3, "Nexus");
        cardTypeList.add(4, "Visa");

        ArrayAdapter<String> paymentByAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, pamentBYList);
        paymentByAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        paymentBySpinner.setAdapter(paymentByAdapter);
        paymentBySpinner.setSelection(0);

        ArrayAdapter<String> cardTypeAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, cardTypeList);
        cardTypeAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        CardTypeSpinner.setAdapter(cardTypeAdapter);
        CardTypeSpinner.setSelection(0);


        GenGrandTotalBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                db = new DBHelper(getContext());
                GrandTotalToPay = new DecimalFormat("##.##").format(Double.parseDouble(db.getGrandTotal(1).trim()));
                GenGrandTotalBTN.setVisibility(View.GONE);
                SaleNowBTN.setVisibility(View.VISIBLE);
                totalAmountLW.setVisibility(View.VISIBLE);
                textEDtHolderLW.setVisibility(View.VISIBLE);
                PaymentOptionLayoutLW.setVisibility(View.VISIBLE);
                TOTAL_AMMOUNT_TXT.setText(String.valueOf(GrandTotalToPay));
                DueAmountTextView.setText(String.valueOf(GrandTotalToPay));

                salesInfoModals = db.getOverviewList2();
                salesInfoModals2 = db.getOverviewList3();


            }
        });


        cashEDT.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(cashEDText)) {
                    cashEDText = "0";
                }


                FinallyAmountPaidInCashEDT = new DecimalFormat("##.##").format(Double.valueOf(cashEDText));

                PaidAmountTextView.setText(FinallyAmountPaidInCashEDT);
                DueAmmountInTakaFinally = new DecimalFormat("##.##").format(Double.valueOf(GrandTotalToPay) - Double.valueOf(FinallyAmountPaidInCashEDT));
                DueAmountTextView.setText(DueAmmountInTakaFinally);


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                cashEDText = cashEDT.getText().toString();


            }
        });
        cardPayET.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(cashEDText)) {
                    cashEDText = "0";
                }


                FinallyAmountPaidInCashEDT = new DecimalFormat("##.##").format(Double.valueOf(cashEDText));

                PaidAmountTextView.setText(FinallyAmountPaidInCashEDT);
                DueAmmountInTakaFinally = new DecimalFormat("##.##").format(Double.valueOf(GrandTotalToPay) - Double.valueOf(FinallyAmountPaidInCashEDT));
                DueAmountTextView.setText(DueAmmountInTakaFinally);


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                cashEDText = cardPayET.getText().toString();


            }
        });
        AmountMobileET.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(cashEDText)) {
                    cashEDText = "0";
                }


                FinallyAmountPaidInCashEDT = new DecimalFormat("##.##").format(Double.valueOf(cashEDText));

                PaidAmountTextView.setText(FinallyAmountPaidInCashEDT);
                DueAmmountInTakaFinally = new DecimalFormat("##.##").format(Double.valueOf(GrandTotalToPay) - Double.valueOf(FinallyAmountPaidInCashEDT));

                DueAmountTextView.setText(DueAmmountInTakaFinally);


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                cashEDText = AmountMobileET.getText().toString();


            }
        });

        paymentBySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                //String selection = (String) adapterView.getItemAtPosition(i);
                //  StoreName = Integer.toString(i + 1);
                switch (i) {

                    case 0:

                        payment_type = "cash";

                        cashTXT.setVisibility(View.VISIBLE);
                        cashEDT.setVisibility(View.VISIBLE);

                        cardNoTXT.setVisibility(View.GONE);
                        cardNoEDT.setVisibility(View.GONE);

                        cardPayTXT.setVisibility(View.GONE);
                        cardPayET.setVisibility(View.GONE);

                        cardTypeTXT.setVisibility(View.GONE);
                        CardTypeSpinnerLayout.setVisibility(View.GONE);

                        BankNameTV.setVisibility(View.GONE);
                        BankTypeSpinnerLayout.setVisibility(View.GONE);

                        AmountMobileTV.setVisibility(View.GONE);
                        AmountMobileET.setVisibility(View.GONE);

                        MobileBankSpinnerTV.setVisibility(View.GONE);
                        MobileBankTypeSpinnerLayout.setVisibility(View.GONE);


                        TransactionNoTV.setVisibility(View.GONE);
                        TransactionNoET.setVisibility(View.GONE);

                        ReceiverNoTV.setVisibility(View.GONE);
                        ReceiverNoET.setVisibility(View.GONE);

                        round.setChecked(false);

                        cardPayET.setText("");
                        cardNoEDT.setText("");
                        CardTypeSpinner.setSelection(0);
                        BankTypeSpinner.setSelection(0);

                        AmountMobileET.setText("");
                        MobileBankSpinnerSP.setSelection(0);
                        TransactionNoET.setText("");
                        ReceiverNoET.setText("");


                        break;

                    case 1:

                        payment_type = "card";

                        cashTXT.setVisibility(View.GONE);
                        cashEDT.setVisibility(View.GONE);

                        cardNoTXT.setVisibility(View.VISIBLE);
                        cardNoEDT.setVisibility(View.VISIBLE);

                        cardPayTXT.setVisibility(View.VISIBLE);
                        cardPayET.setVisibility(View.VISIBLE);

                        cardTypeTXT.setVisibility(View.VISIBLE);
                        CardTypeSpinnerLayout.setVisibility(View.VISIBLE);

                        BankNameTV.setVisibility(View.VISIBLE);
                        BankTypeSpinnerLayout.setVisibility(View.VISIBLE);

                        AmountMobileTV.setVisibility(View.GONE);
                        AmountMobileET.setVisibility(View.GONE);

                        MobileBankSpinnerTV.setVisibility(View.GONE);
                        MobileBankTypeSpinnerLayout.setVisibility(View.GONE);


                        TransactionNoTV.setVisibility(View.GONE);
                        TransactionNoET.setVisibility(View.GONE);

                        ReceiverNoTV.setVisibility(View.GONE);
                        ReceiverNoET.setVisibility(View.GONE);
                        round.setChecked(false);


                        cashEDT.setText("");

                        AmountMobileET.setText("");
                        MobileBankSpinnerSP.setSelection(0);
                        TransactionNoET.setText("");
                        ReceiverNoET.setText("");


                        break;
                    case 2:

                        payment_type = "mobile";

                        cashTXT.setVisibility(View.GONE);
                        cashEDT.setVisibility(View.GONE);

                        cardNoTXT.setVisibility(View.GONE);
                        cardNoEDT.setVisibility(View.GONE);

                        cardPayTXT.setVisibility(View.GONE);
                        cardPayET.setVisibility(View.GONE);

                        cardTypeTXT.setVisibility(View.GONE);
                        CardTypeSpinnerLayout.setVisibility(View.GONE);

                        BankNameTV.setVisibility(View.GONE);
                        BankTypeSpinnerLayout.setVisibility(View.GONE);

                        AmountMobileTV.setVisibility(View.VISIBLE);
                        AmountMobileET.setVisibility(View.VISIBLE);

                        MobileBankSpinnerTV.setVisibility(View.VISIBLE);
                        MobileBankTypeSpinnerLayout.setVisibility(View.VISIBLE);


                        TransactionNoTV.setVisibility(View.VISIBLE);
                        TransactionNoET.setVisibility(View.VISIBLE);

                        ReceiverNoTV.setVisibility(View.VISIBLE);
                        ReceiverNoET.setVisibility(View.VISIBLE);
                        round.setChecked(false);

                        cashEDT.setText("");

                        cardPayET.setText("");
                        cardNoEDT.setText("");
                        CardTypeSpinner.setSelection(0);
                        BankTypeSpinner.setSelection(0);


                        break;

                    default:

                        payment_type = "cash";

                        cashTXT.setVisibility(View.VISIBLE);
                        cashEDT.setVisibility(View.VISIBLE);

                        cardNoTXT.setVisibility(View.GONE);
                        cardNoEDT.setVisibility(View.GONE);

                        cardPayTXT.setVisibility(View.GONE);
                        cardPayET.setVisibility(View.GONE);

                        cardTypeTXT.setVisibility(View.GONE);
                        CardTypeSpinnerLayout.setVisibility(View.GONE);

                        BankNameTV.setVisibility(View.GONE);
                        BankTypeSpinnerLayout.setVisibility(View.GONE);

                        AmountMobileTV.setVisibility(View.GONE);
                        AmountMobileET.setVisibility(View.GONE);

                        MobileBankSpinnerTV.setVisibility(View.GONE);
                        MobileBankTypeSpinnerLayout.setVisibility(View.GONE);


                        TransactionNoTV.setVisibility(View.GONE);
                        TransactionNoET.setVisibility(View.GONE);

                        ReceiverNoTV.setVisibility(View.GONE);
                        ReceiverNoET.setVisibility(View.GONE);
                        round.setChecked(false);

                        break;


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });


        TOTAL_AMMOUNT_TXT.setText(String.valueOf(GrandTotalToPay));
        DueAmountTextView.setText(String.valueOf(GrandTotalToPay));


        BankTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                //String selection = (String) adapterView.getItemAtPosition(i);
                //  StoreName = Integer.toString(i + 1);
                String selection = BankDetails.get(i);
                List<String> elephantList = Arrays.asList(selection.split(","));
             //   Log.e("elephantList:", elephantList.toString());


                //Title = elephantList.get(0);
                bank_id = elephantList.get(0);
                acc_type_id = elephantList.get(1);
                bank_name = elephantList.get(2);
                id_account = elephantList.get(3);
                account_no = elephantList.get(4);

                bank_nameForPost = bank_id + "@" + id_account;


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        MobileBankSpinnerSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                //String selection = (String) adapterView.getItemAtPosition(i);
                //  StoreName = Integer.toString(i + 1);
                String selection = MobileBankDetails.get(i);
                List<String> elephantList = Arrays.asList(selection.split(","));
             //   Log.e("elephantList:", elephantList.toString());


                //Title = elephantList.get(0);
                mobile_bank_id = elephantList.get(0);
                acc_type_id = elephantList.get(1);
                mobile_bank_name = elephantList.get(2);
                mobile_id_account = elephantList.get(3);
                mobile_account_no = elephantList.get(4);

                mobile_bank_nameForPost = mobile_bank_id + "@" + mobile_id_account;


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });


//        cashEDT.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//
//                if (TextUtils.isEmpty(prevAmmountInCashET)) {
//                    prevAmmountInCashET = "0";
//                } else {
//                    prevAmmountInCashET = prevAmmountInCashET;
//                }
//
//
//                prevAmmountInTotalTV = TOTAL_AMMOUNT_TXT.getText().toString().trim();
//                prevAmmountInTotalDue = DueAmountTextView.getText().toString().trim();
//                FinalAmmountInTaka = Double.valueOf(prevAmmountInTotalDue) - Double.valueOf(prevAmmountInCashET);
//
//                DueAmountTextView.setText(String.valueOf(FinalAmmountInTaka));
//
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start,
//                                          int count, int after) {
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start,
//                                      int before, int count) {
//
//                prevAmmountInCashET = cashEDT.getText().toString();
//
//
//            }
//        });
        round.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Double DueTx = Double.valueOf(DueAmountTextView.getText().toString().trim());
                    Double DueT = 0 - DueTx;
                    RoundAmountTextView.setText(String.valueOf(DueT));
                    DueAmountTextView.setText(String.valueOf("0"));

                } else if (!isChecked) {

                    Double RueTx = Double.valueOf(RoundAmountTextView.getText().toString().trim());
                    Double RueT = 0 - RueTx;

                    DueAmountTextView.setText(String.valueOf(RueT));

                    RoundAmountTextView.setText("0");
                }
            }
        });


        SaleNowBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cash = cashEDT.getText().toString().trim();
                card_cash = cardPayET.getText().toString().trim();
                mob_cash = AmountMobileET.getText().toString().trim();

                if (!TextUtils.isEmpty(cash) | !TextUtils.isEmpty(card_cash) | !TextUtils.isEmpty(mob_cash)) {
                    NumOfRow = db.numberOfRowsAdd();
                    String[] pro_id = new String[NumOfRow]
                            , batch = new String[NumOfRow]
                            , qty = new String[NumOfRow]
                            , stock_id = new String[NumOfRow]
                            , cat_id = new String[NumOfRow]
                            , subcat_id = new String[NumOfRow]
                            , brand_id = new String[NumOfRow]
                            , unit_id = new String[NumOfRow]
                            , unit_price = new String[NumOfRow]
                            , discount = new String[NumOfRow]
                            , discount_type = new String[NumOfRow]
                            , total_price = new String[NumOfRow]
                            , pro_sale_id = new String[NumOfRow]
                            , store_from = new String[NumOfRow]
                            , station_id = new String[NumOfRow]
                            , def_vat = new String[NumOfRow]
                            , def_vat_amt = new String[NumOfRow]
                            , original_product_price = new String[NumOfRow];

                    String[] pro_name = new String[NumOfRow];
                    String[] pro_code = new String[NumOfRow];
                    String[] total_qty = new String[NumOfRow];
                    String grand_total = null, cart_total = null, special_dis = null, cus_dis = null, pur_dis = null, card_dis = null, customer_id = null, customer_code = null, ck_sp_dis = null, ck_cus_dis = null, ck_pur_dis = null, ck_cart_dis = null, paid_amt = null;


               //     Log.e("Total Item in cart", String.valueOf(NumOfRow));

                    for (int i = 0; i < NumOfRow; i++) {
                        pro_id[i] = salesInfoModals.get(i).getPro_id();
                        batch[i] = salesInfoModals.get(i).getBatch();
                        qty[i] = salesInfoModals.get(i).getQty();
                        stock_id[i] = salesInfoModals.get(i).getStock_id();
                        cat_id[i] = salesInfoModals.get(i).getCat_id();
                        subcat_id[i] = salesInfoModals.get(i).getSubcat_id();
                        brand_id[i] = salesInfoModals.get(i).getBrand_id();
                        unit_id[i] = salesInfoModals.get(i).getUnit_id();
                        unit_price[i] = salesInfoModals.get(i).getUnit_price();
                        discount[i] = salesInfoModals.get(i).getDiscount();
                        discount_type[i] = salesInfoModals.get(i).getDiscount_type();
                        total_price[i] = salesInfoModals.get(i).getTotal_price();
                        pro_sale_id[i] = salesInfoModals.get(i).getPro_sale_id();
                        store_from[i] = salesInfoModals.get(i).getStore_from();
                        station_id[i] = salesInfoModals.get(i).getStation_id();
                        def_vat[i] = salesInfoModals.get(i).getDef_vat();
                        def_vat_amt[i] = salesInfoModals.get(i).getDef_vat_amt();
                        original_product_price[i] = salesInfoModals.get(i).getOriginal_product_price();
                        org_product_priceTotal = org_product_priceTotal + Double.valueOf(original_product_price[i]);

                        pro_name[i] = salesInfoModals.get(i).getPro_name();
                        pro_code[i] = salesInfoModals.get(i).getPro_code();
                        total_qty[i] = salesInfoModals.get(i).getTotal_qty();


                        //showMessageTV.setText(salesInfoModals.get(i).getBatch()+" "+salesInfoModals2.get(i).getGrand_total());
                    }

                    original_product_priceTotal = String.valueOf(org_product_priceTotal);

                    int i = 0;

                    grand_total = salesInfoModals2.get(i).getGrand_total();
                    cart_total = salesInfoModals2.get(i).getCart_total();
                    special_dis = salesInfoModals2.get(i).getSpecial_dis();
                    cus_dis = salesInfoModals2.get(i).getCus_dis();
                    pur_dis = salesInfoModals2.get(i).getPur_dis();
                    card_dis = salesInfoModals2.get(i).getCard_dis();
                    customer_id = salesInfoModals2.get(i).getCustomer_id();
                    customer_code = salesInfoModals2.get(i).getCustomer_code();
                    ck_sp_dis = salesInfoModals2.get(i).getCk_sp_dis();
                    ck_cus_dis = salesInfoModals2.get(i).getCk_cus_dis();
                    ck_pur_dis = salesInfoModals2.get(i).getCk_pur_dis();
                    ck_cart_dis = salesInfoModals2.get(i).getCk_cart_dis();


                    cardNoETString = cardNoEDT.getText().toString().trim();
                    transaction_no = TransactionNoET.getText().toString().trim();
                    paid_amt = FinallyAmountPaidInCashEDT + "@" + RoundAmountTextView.getText().toString().trim() + "@" + DueAmountTextView.getText().toString().trim();


                    setSpinnerData_For_CardPromotionSpinner(pro_id, batch, qty, stock_id, cat_id
                            , subcat_id, brand_id, unit_id, unit_price
                            , discount, discount_type, total_price, pro_sale_id
                            , store_from, station_id, def_vat, def_vat_amt
                            , original_product_price, grand_total, cart_total, special_dis, cus_dis, pur_dis, card_dis
                            , customer_id, customer_code, ck_sp_dis
                            , ck_cus_dis, ck_pur_dis, ck_cart_dis, cash, cash_acc_id, paid_amt, original_product_priceTotal, pro_name, pro_code, total_qty);


                } else {
                    Snackbar.make(v, "No Money Paid !", Snackbar.LENGTH_LONG)
                            .setAction("No action", null).show();
                }
            }
        });

        CardTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String selection = (String) adapterView.getItemAtPosition(i);

                switch (selection) {

                    case "Select One":
                        card_type = "0";
                        break;
                    case "Visa":
                        card_type = "1";
                        break;

                    case "Mastercard":
                        card_type = "2";
                        break;

                    case "Amex":
                        card_type = "3";
                        break;

                    case "Nexus":
                        card_type = "4";
                        break;


                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });


        return v;
    }

    private void setSpinnerData_For_CardPromotionSpinner(final String[] pro_id, final String[] batch, final String[] qty, final String[] stock_id, final String[] cat_id
            , final String[] subcat_id, final String[] brand_id, final String[] unit_id, final String[] unit_price
            , final String[] discount, final String[] discount_type, final String[] total_price, final String[] pro_sale_id
            , final String[] store_from, final String[] station_id, final String[] def_vat, final String[] def_vat_amt
            , final String[] original_product_price
            , final String grand_total, final String cart_total, final String special_dis, final String cus_dis, final String pur_dis, final String card_dis
            , final String customer_id, final String customer_code, final String ck_sp_dis
            , final String ck_cus_dis, final String ck_pur_dis, final String ck_cart_dis, final String cash
            , final String cash_acc_id
            , final String paid_amt
            , final String original_product_priceTotal
            , final String[] pro_name
            , final String[] pro_code
            , final String[] total_qty) {


        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
      //  Log.e("Product_IDs", String.valueOf(pro_id));

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://"+YourUserName+".posspot.com/api/sales_api/sales_add_submit", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                String result = "false";

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    //  JSONObject jsonObject2 = jsonObject.getJSONObject("response");

                    //JSONArray jsonArray = jsonObject.getJSONArray("response");

                    result = jsonObject.getString("response");

               //     Log.e("Response from Pos", result);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (result.equals("success")) {

                    final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(getContext())
                            .setMessage("Successfully Sold!")
                            .setPositiveButton("Sell More", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogDel, int which) {

                                    //  SaleNowActivity.viewPager.setCurrentItem(0);

                                    dialogDel.dismiss();
                                    DBHelper db = new DBHelper(getContext());
                                    db.delete();
                                    getActivity().finish();
                                    startActivity(new Intent(getContext(), SaleNowActivity.class));

//                                    TabLayout tabs = (TabLayout)((SaleNowActivity)getActivity()).findViewById(R.id.tabs);
//                                    tabs.getTabAt(0).select();

                                }

                            })

                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogdel1, int which) {
                                    // viewPager.setCurrentItem(0);
                                    dialogdel1.dismiss();
//                                    DBHelper db = new DBHelper(getContext());
//                                    db.delete();
//                                   // TabHost host = (TabHost)((SaleNowActivity)getActivity()).findViewById(R.id.tabhost);
                                    getActivity().finish();
                                }
                            });
                    dialogDelete.show();
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage("Error to connect!");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "DISSMISS",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                error.printStackTrace();
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                for (int i = 0; i < NumOfRow; i++) {
                    params.put("pro_id[" + i + "]", String.valueOf(pro_id[i]));
                    params.put("batch[" + i + "]", String.valueOf(batch[i]));
                    params.put("qty[" + i + "]", String.valueOf(qty[i]));
                    params.put("stock_id[" + i + "]", String.valueOf(stock_id[i]));
                    params.put("cat_id[" + i + "]", String.valueOf(cat_id[i]));
                    params.put("subcat_id[" + i + "]", String.valueOf(subcat_id[i]));
                    params.put("brand_id[" + i + "]", String.valueOf(brand_id[i]));
                    params.put("unit_id[" + i + "]", String.valueOf(unit_id[i]));
                    params.put("unit_price[" + i + "]", String.valueOf(unit_price[i]));
                    params.put("discount[" + i + "]", String.valueOf(discount[i]));
                    params.put("discount_type[" + i + "]", String.valueOf(discount_type[i]));
                    params.put("total_price[" + i + "]", String.valueOf(total_price[i]));
                    params.put("pro_sale_id[" + i + "]", String.valueOf(pro_sale_id[i]));

                    params.put("def_vat[" + i + "]", String.valueOf(def_vat[i]));
                    params.put("def_vat_amt[" + i + "]", String.valueOf(def_vat_amt[i]));
                    params.put("pro_name[" + i + "]", String.valueOf(pro_name[i]));
                    params.put("pro_code[" + i + "]", String.valueOf(pro_code[i]));
                    params.put("total_qty[" + i + "]", String.valueOf(total_qty[i]));
                    // params.put("original_product_price", String.valueOf(original_product_price[i]));

                }


                //    store_from,station_id,original_product_price,grand_total,customer_id,paid_amt
                params.put("store_from", store_fromNew);
                params.put("station_id", station_idNew);

                params.put("special_dis", String.valueOf(special_dis));
                params.put("cus_dis", String.valueOf(cus_dis));
                params.put("pur_dis", String.valueOf(pur_dis));
                params.put("card_dis", String.valueOf(card_dis));


                params.put("grand_total", String.valueOf(grand_total));
                params.put("cart_total", String.valueOf(cart_total));//done
                params.put("customer_id", String.valueOf(customer_id));//done
                params.put("customer", String.valueOf(customer_id));  //done
                params.put("customer_code", String.valueOf(customer_code));//done
                params.put("paid_amt", String.valueOf(paid_amt));//done


                params.put("ck_sp_dis", String.valueOf(ck_sp_dis));
                params.put("ck_cus_dis", String.valueOf(ck_cus_dis));//done
                params.put("ck_pur_dis", String.valueOf(ck_pur_dis));
                params.put("ck_cart_dis", String.valueOf(ck_cart_dis));


                params.put("payment_type", payment_type);
                params.put("cash", cash);//done
                params.put("cash_acc_id", String.valueOf(cash_acc_id));

                params.put("customer_phone", String.valueOf(PhoneNo));//done --------------------------
                // params.put("customer_discount", String.valueOf("0.00"));//done
                params.put("customer_name", String.valueOf(Name));//done-------------------------------
                //  params.put("remit_point", "");//done
                //  params.put("remit_taka_val", "");//done
                //  params.put("cus_id", "");//done
                //  params.put("c_phone", "");//done
                params.put("points", CustomersPoints);//done --------------------------------balance
                params.put("balance", CustomersBalance);//done---------------------------------point
                //  params.put("per_point_balance", "0");//done
                //  params.put("percent", "");//done
                //  params.put("taka", "");//done

                params.put("card_promotion", PromotionId);//done keep promotion id here  -------------------------------------
                params.put("card_number", cardNoETString);//done
                params.put("card_payment", card_cash);//done
                params.put("card_type", card_type);//done
                params.put("bank_name", bank_nameForPost);//done

                params.put("mobile_amount", mob_cash);//done
                params.put("mob_bank_name", mobile_bank_nameForPost);//done
                params.put("transaction_no", transaction_no);//done
                params.put("mob_acc_no", mobile_account_no);//done

                //params.put("total_pur_dis", "");//done
                params.put("original_product_price", original_product_priceTotal);//done
                //        params.put("original_product_price", original_product_price[0]);//done
//                params.put("print_type", "sale");//done


            //    Log.e("Posting values", params.toString());

                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }


    private void loadAutoSuggestionDataForBANKtype(String banklistAPIurl, final String StoreId) {


        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        BankList.clear();
        BankDetails.clear();
        MobileBankList.clear();
        MobileBankDetails.clear();


        BankList.add(0, "Select One");
        MobileBankList.add(0, "Select One");

        BankDetails.add(0, "null" + "," + "0" + "," + "null" + "," + "null" + "," + "null");
        MobileBankDetails.add(0, "null" + "," + "0" + "," + "null" + "," + "null" + "," + "null");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, banklistAPIurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

             //   Log.e("Respnse:", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("response");
                    int countIndexBNK = 1;
                    int countIndexMBNK = 1;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String BankId = jsonObject1.getString("bank_id");
                        String BankAccTypeId = jsonObject1.getString("acc_type_id");
                        String BankName = jsonObject1.getString("bank_name");
                        String IdAccount = jsonObject1.getString("id_account");
                        String AccountNo = jsonObject1.getString("account_no");


                        switch (BankAccTypeId) {
                            case "1":
                                BankList.add(countIndexBNK, BankName);
                                BankDetails.add(countIndexBNK, BankId + "," + BankAccTypeId + "," + BankName + "," + IdAccount + "," + AccountNo);
                            //    Log.e("bank er nam:", BankName);
                                countIndexBNK++;
                                break;
                            case "3":
                                MobileBankList.add(countIndexMBNK, BankName+" ("+AccountNo+")");
                                MobileBankDetails.add(countIndexMBNK, BankId + "," + BankAccTypeId + "," + BankName + "," + IdAccount + "," + AccountNo);
                                countIndexMBNK++;
                                break;
                            default:
                                break;
                        }


                    }

                    ArrayAdapter<String> BankTypeSpinnerAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, BankList);
                    BankTypeSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                    BankTypeSpinner.setAdapter(BankTypeSpinnerAdapter);
                    BankTypeSpinner.setSelection(0);

                    ArrayAdapter<String> MobileBankSpinnerAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, MobileBankList);
                    MobileBankSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                    MobileBankSpinnerSP.setAdapter(MobileBankSpinnerAdapter);
                    MobileBankSpinnerSP.setSelection(0);

//                    BankList.clear();
//                    BankDetails.clear();
//
//                    MobileBankList.clear();
//                    MobileBankDetails.clear();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                error.printStackTrace();
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("store_id", StoreId);

                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onStop() {
        super.onStop();


    }


}
