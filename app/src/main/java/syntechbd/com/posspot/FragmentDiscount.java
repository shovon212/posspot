package syntechbd.com.posspot;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import syntechbd.com.posspot.app.DBHelper;

import static syntechbd.com.posspot.SaleNowActivity.cart_total;


public class FragmentDiscount extends Fragment {


    SharedPreferences pref;

    TextView totalAmountTV, normalDisTV, normalDisTV_, CardDiscountText, CusDisText, PusDisText;
    TextView CardDiscountText_, CusDisText_, PusDisText_;
    Switch NormalDiscountSwitch, CustomerDiscountSwitch, PurchaseDiscountSwitch, cardDiscountSwitch,AddAllDiscountSwitch;
    String NormalDiscountSwitchV, CustomerDiscountSwitchV, PurchaseDiscountSwitchV, cardDiscountSwitchV;
    EditText discountInPercentET, discountInTakaET;

    Spinner promotionSpinner;

    String pText, tText;
    boolean percent = true;
    boolean taka = true;
    String spinner_API_URL, cusDis_API_URL;

    ArrayList<String> promotionList;
    ArrayList<String> promotionDetails;

    String Title, TypeId, DiscountRate;
    static String PromotionId= "0";
    Context context;
    private String CustomerDiscountFromJson="0";
    private String Rate = "0";
    private String PurAmmountInTaka = "0";
    private String TotalAmmountFromDB = "0";
    private String TextGotFromDiscountInPercentBox= "0";
    private String TextGotFromDiscountInTakaBox= "0";

    SharedPreferences prefCUSTOMER;

    String CustomersBalance, CustomersPoints;
    //String CUSTOMER_ID;
    private String normalDisCountInTaka = "0";
    private double FinalAmmountInTaka;
    private String FinalAmmountInTakaSTRING = "0";
    private String CARDdiscount;
    private String prevAmmount;
    public static String cusDiscountResult;
    private String CUSTOMER_ID;
    public static boolean isItDiscountPage;
    private boolean allowRefresh = true;
    int count = 0;

    String special_dis, cus_dis, pur_dis, card_dis, grand_total;
    public static String GRAND_TOTAL;

    String special_dis_pre, special_dis_po, cus_dis_pre, cus_dis_po, pur_dis_pre, pur_dis_po, card_dis_pre, card_dis_po;
    private String YouruserName;
    private String store_id = "1";

    public FragmentDiscount() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        NormalDiscountSwitchV = "2";
        CustomerDiscountSwitchV = "2";
        PurchaseDiscountSwitchV = "2";
        cardDiscountSwitchV = "2";

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_discount, container, false);
        context = getContext();
        pref = context.getSharedPreferences("UserInfo", 0);
        YouruserName = pref.getString("user_name", null);
        store_id = pref.getString("store_id", null);
        final DBHelper dbHelper = new DBHelper(getContext());
        // TotalAmmountFromDB = dbHelper.getTotalAmountInCart();
        TotalAmmountFromDB = cart_total;
//        if(TotalAmmountFromDB.equals("0")) TotalAmmountFromDB = "0";

//        Log.i("TotalAmmountFromDB",TotalAmmountFromDB);
        FinalAmmountInTaka = Double.valueOf(TotalAmmountFromDB);
        FinalAmmountInTakaSTRING = new DecimalFormat("##.##").format(FinalAmmountInTaka);

        spinner_API_URL = "http://"+YouruserName+".posspot.com/api/sales_api/card_promotions";
        cusDis_API_URL = "http://"+YouruserName+".posspot.com/api/sales_api/check_discount_sales";

        prefCUSTOMER = context.getSharedPreferences("CustomerInfo", 0);


        CustomersBalance = prefCUSTOMER.getString("CustomersBalance", null);
        CustomersPoints = prefCUSTOMER.getString("CustomersPoints", null);


        totalAmountTV = (TextView) v.findViewById(R.id.totalAmountTV);
        CusDisText = (TextView) v.findViewById(R.id.cusDisTV);
        PusDisText = (TextView) v.findViewById(R.id.pusDisTV);

        CusDisText_ = (TextView) v.findViewById(R.id.cusDisTV_);
        PusDisText_ = (TextView) v.findViewById(R.id.pusDisTV_);

        normalDisTV = (TextView) v.findViewById(R.id.normalDisTV);
        normalDisTV_ = (TextView) v.findViewById(R.id.normalDisTV_);
        discountInPercentET = (EditText) v.findViewById(R.id.discountInPercentET);
        discountInTakaET = (EditText) v.findViewById(R.id.discountInTakaET);
        promotionSpinner = (Spinner) v.findViewById(R.id.promoSpinnerSP);
        CardDiscountText = (TextView) v.findViewById(R.id.cardDiscountTV);
        CardDiscountText_ = (TextView) v.findViewById(R.id.cardDiscountTV_);
        cardDiscountSwitch = (Switch) v.findViewById(R.id.cardDiscountSW);
        NormalDiscountSwitch = (Switch) v.findViewById(R.id.NormalDisSW);
        CustomerDiscountSwitch = (Switch) v.findViewById(R.id.CustomerDiscountSW);
        PurchaseDiscountSwitch = (Switch) v.findViewById(R.id.purchaseDisCountSW);
        AddAllDiscountSwitch = (Switch) v.findViewById(R.id.AddAllDiscountSW);


        normalDisTV_.setText("0");
        discountInPercentET.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {


                if (TextUtils.isEmpty(pText)) {
                    TextGotFromDiscountInPercentBox = "0";
                    discountInTakaET.setEnabled(true);
                } else {
                    TextGotFromDiscountInPercentBox = pText;
                    discountInTakaET.setEnabled(false);
                }

                normalDisTV_.setText(TextGotFromDiscountInPercentBox);

                Double dp = Double.valueOf(TotalAmmountFromDB) * Double.valueOf(TextGotFromDiscountInPercentBox) / 100;
                normalDisCountInTaka = new DecimalFormat("##.##").format(dp);

                normalDisTV.setText(normalDisCountInTaka);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                pText = discountInPercentET.getText().toString();


            }
        });

        discountInTakaET.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(tText)) {
                    TextGotFromDiscountInTakaBox = "0";
                    discountInPercentET.setEnabled(true);
                    normalDisTV_.setText("0");
                } else {
                    TextGotFromDiscountInTakaBox = tText;
                    discountInPercentET.setEnabled(false);
                    Double dp = Double.valueOf(TextGotFromDiscountInTakaBox) / Double.valueOf(TotalAmmountFromDB) * 100;
                    String normDi  = new DecimalFormat("##.##").format(dp);
                    normalDisTV_.setText(normDi);
                }


                normalDisCountInTaka = new DecimalFormat("##.##").format(Double.valueOf(TextGotFromDiscountInTakaBox));


//                if (!TextGotFromDiscountInTakaBox.equals("0")) {
//                    Double dp = Double.valueOf(TotalAmmountFromDB) / Double.valueOf(TextGotFromDiscountInTakaBox) * 100;
//                    normalDisTV_.setText(String.valueOf(dp));
//                }else normalDisTV_.setText("0");


                normalDisTV.setText(normalDisCountInTaka);


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                tText = discountInTakaET.getText().toString();


            }
        });


        totalAmountTV.setText(TotalAmmountFromDB);

        setSpinnerData_For_CardPromotionSpinner(spinner_API_URL, store_id);



        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //TODO your background code
                CUSTOMER_ID = dbHelper.getCustomerID(1);


//                Log.e("MY CUS ID", CUSTOMER_ID);

                setCustomerAndPurchaseDiscount(TotalAmmountFromDB, CUSTOMER_ID, store_id, cusDis_API_URL);

            }
        });



        promotionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                //String selection = (String) adapterView.getItemAtPosition(i);
                //  StoreName = Integer.toString(i + 1);
                String selection = promotionDetails.get(i);
                List<String> elephantList = Arrays.asList(selection.split(","));
             //   Log.e("elephantList:", elephantList.toString());


                //Title = elephantList.get(0);
                TypeId = elephantList.get(0);
                DiscountRate = elephantList.get(1);
                PromotionId = elephantList.get(2);


                if (DiscountRate.equals("null")) {
                    CardDiscountText.setText("00");
                    CardDiscountText_.setText(DiscountRate);
                    //cardDiscountSwitch.setChecked(false);
                } else {
                    Double xxx = Double.valueOf(TotalAmmountFromDB) * Double.valueOf(DiscountRate) / 100;
                    CARDdiscount = new DecimalFormat("##.##").format(xxx);
                    CardDiscountText.setText(CARDdiscount);
                    CardDiscountText_.setText(DiscountRate);
                    // cardDiscountSwitch.setChecked(true);
                }

           //     Log.e("GOT:", TypeId + "," + DiscountRate + "," + PromotionId);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });




        AddAllDiscountSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {


                    //TODO your background code

                    CUSTOMER_ID = String.valueOf(dbHelper.getCID("1"));
                    setCustomerAndPurchaseDiscount(TotalAmmountFromDB, CUSTOMER_ID, store_id, cusDis_API_URL);
                    CusDisText_.setText(CustomerDiscountFromJson);
                    CusDisText.setText(cusDiscountResult);



                    NormalDiscountSwitch.setChecked(true);

                    PurchaseDiscountSwitch.setChecked(true);
                    cardDiscountSwitch.setChecked(true);


                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            CustomerDiscountSwitch.setChecked(true);
                            CustomerDiscountSwitch.setChecked(false);
                            CustomerDiscountSwitch.setChecked(true);
                        }
                    }, 3000);


                } else if (!isChecked) {

                    NormalDiscountSwitch.setChecked(false);
                    CustomerDiscountSwitch.setChecked(false);
                    PurchaseDiscountSwitch.setChecked(false);
                    cardDiscountSwitch.setChecked(false);

                }
            }
        });

        NormalDiscountSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    NormalDiscountSwitchV = "1";
                    prevAmmount = totalAmountTV.getText().toString().trim();
                    FinalAmmountInTaka = Double.valueOf(prevAmmount) - Double.valueOf(normalDisCountInTaka);
                    FinalAmmountInTakaSTRING = new DecimalFormat("##.##").format(FinalAmmountInTaka);

                    totalAmountTV.setText(FinalAmmountInTakaSTRING);


                    special_dis_pre = normalDisTV_.getText().toString().trim();
                    special_dis_po = normalDisTV.getText().toString().trim();
                    special_dis = special_dis_pre + "@" + special_dis_po;

                    cus_dis_pre = CusDisText_.getText().toString().trim();
                    cus_dis_po = CusDisText.getText().toString().trim();
                    cus_dis = cus_dis_pre + "@" + cus_dis_po;

                    pur_dis_pre = PusDisText_.getText().toString().trim();
                    pur_dis_po = PusDisText.getText().toString().trim();
                    pur_dis = pur_dis_pre + "@" + pur_dis_po;

                    card_dis_pre = CardDiscountText_.getText().toString().trim();
                    card_dis_po = CardDiscountText.getText().toString().trim();
                    card_dis = card_dis_pre + "@" + card_dis_po;

                    grand_total = totalAmountTV.getText().toString().trim();

                    dbHelper.updateinsertCustomerInfo("1", special_dis, cus_dis, pur_dis, card_dis, grand_total,NormalDiscountSwitchV, CustomerDiscountSwitchV, PurchaseDiscountSwitchV, cardDiscountSwitchV,cart_total);


                } else if (!isChecked) {
                    NormalDiscountSwitchV = "2";
                    prevAmmount = totalAmountTV.getText().toString().trim();
                    FinalAmmountInTaka = Double.valueOf(prevAmmount) + Double.valueOf(normalDisCountInTaka);
                    FinalAmmountInTakaSTRING = new DecimalFormat("##.##").format(FinalAmmountInTaka);
                    totalAmountTV.setText(FinalAmmountInTakaSTRING);

                    special_dis_pre = normalDisTV_.getText().toString().trim();
                    special_dis_po = normalDisTV.getText().toString().trim();
                    special_dis = special_dis_pre + "@" + special_dis_po;

                    cus_dis_pre = CusDisText_.getText().toString().trim();
                    cus_dis_po = CusDisText.getText().toString().trim();
                    cus_dis = cus_dis_pre + "@" + cus_dis_po;

                    pur_dis_pre = PusDisText_.getText().toString().trim();
                    pur_dis_po = PusDisText.getText().toString().trim();
                    pur_dis = pur_dis_pre + "@" + pur_dis_po;

                    card_dis_pre = CardDiscountText_.getText().toString().trim();
                    card_dis_po = CardDiscountText.getText().toString().trim();
                    card_dis = card_dis_pre + "@" + card_dis_po;

                    grand_total = totalAmountTV.getText().toString().trim();

                    dbHelper.updateinsertCustomerInfo("1", special_dis, cus_dis, pur_dis, card_dis, grand_total,NormalDiscountSwitchV, CustomerDiscountSwitchV, PurchaseDiscountSwitchV, cardDiscountSwitchV,cart_total);


                }
            }
        });
        CustomerDiscountSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    CustomerDiscountSwitchV = "1";
                    CUSTOMER_ID = String.valueOf(dbHelper.getCID("1"));
                    setCustomerAndPurchaseDiscount(TotalAmmountFromDB, CUSTOMER_ID, store_id, cusDis_API_URL);
                    CusDisText_.setText(CustomerDiscountFromJson);
                    CusDisText.setText(cusDiscountResult);
                    prevAmmount = totalAmountTV.getText().toString().trim();
               //     Log.e("prevAmnt+cusDisResult", prevAmmount + "+" + cusDiscountResult);
                    FinalAmmountInTaka = Double.valueOf(prevAmmount) - Double.valueOf(cusDiscountResult);

                    FinalAmmountInTakaSTRING = new DecimalFormat("##.##").format(FinalAmmountInTaka);
                    totalAmountTV.setText(FinalAmmountInTakaSTRING);


                    special_dis_pre = normalDisTV_.getText().toString().trim();
                    special_dis_po = normalDisTV.getText().toString().trim();
                    special_dis = special_dis_pre + "@" + special_dis_po;

                    cus_dis_pre = CusDisText_.getText().toString().trim();
                    cus_dis_po = CusDisText.getText().toString().trim();
                    cus_dis = cus_dis_pre + "@" + cus_dis_po;

                    pur_dis_pre = PusDisText_.getText().toString().trim();
                    pur_dis_po = PusDisText.getText().toString().trim();
                    pur_dis = pur_dis_pre + "@" + pur_dis_po;

                    card_dis_pre = CardDiscountText_.getText().toString().trim();
                    card_dis_po = CardDiscountText.getText().toString().trim();
                    card_dis = card_dis_pre + "@" + card_dis_po;

                    grand_total = totalAmountTV.getText().toString().trim();

                    dbHelper.updateinsertCustomerInfo("1", special_dis, cus_dis, pur_dis, card_dis, grand_total,NormalDiscountSwitchV, CustomerDiscountSwitchV, PurchaseDiscountSwitchV, cardDiscountSwitchV,cart_total);


                } else if (!isChecked) {
                    CustomerDiscountSwitchV = "2";
                    prevAmmount = totalAmountTV.getText().toString().trim();
                    FinalAmmountInTaka = Double.valueOf(prevAmmount) + Double.valueOf(cusDiscountResult);
                    FinalAmmountInTakaSTRING = new DecimalFormat("##.##").format(FinalAmmountInTaka);
                    totalAmountTV.setText(FinalAmmountInTakaSTRING);

                    special_dis_pre = normalDisTV_.getText().toString().trim();
                    special_dis_po = normalDisTV.getText().toString().trim();
                    special_dis = special_dis_pre + "@" + special_dis_po;

                    cus_dis_pre = CusDisText_.getText().toString().trim();
                    cus_dis_po = CusDisText.getText().toString().trim();
                    cus_dis = cus_dis_pre + "@" + cus_dis_po;

                    pur_dis_pre = PusDisText_.getText().toString().trim();
                    pur_dis_po = PusDisText.getText().toString().trim();
                    pur_dis = pur_dis_pre + "@" + pur_dis_po;

                    card_dis_pre = CardDiscountText_.getText().toString().trim();
                    card_dis_po = CardDiscountText.getText().toString().trim();
                    card_dis = card_dis_pre + "@" + card_dis_po;

                    grand_total = totalAmountTV.getText().toString().trim();

                    dbHelper.updateinsertCustomerInfo("1", special_dis, cus_dis, pur_dis, card_dis, grand_total,NormalDiscountSwitchV, CustomerDiscountSwitchV, PurchaseDiscountSwitchV, cardDiscountSwitchV,cart_total);


                }
            }
        });
        PurchaseDiscountSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    PurchaseDiscountSwitchV = "1";
                    CUSTOMER_ID = String.valueOf(dbHelper.getCID("1"));
                    setCustomerAndPurchaseDiscount(TotalAmmountFromDB, CUSTOMER_ID, store_id, cusDis_API_URL);
                    prevAmmount = totalAmountTV.getText().toString().trim();
                    FinalAmmountInTaka = Double.valueOf(prevAmmount) - Double.valueOf(PurAmmountInTaka);
                    FinalAmmountInTakaSTRING = new DecimalFormat("##.##").format(FinalAmmountInTaka);
                    totalAmountTV.setText(FinalAmmountInTakaSTRING);

                    special_dis_pre = normalDisTV_.getText().toString().trim();
                    special_dis_po = normalDisTV.getText().toString().trim();
                    special_dis = special_dis_pre + "@" + special_dis_po;

                    cus_dis_pre = CusDisText_.getText().toString().trim();
                    cus_dis_po = CusDisText.getText().toString().trim();
                    cus_dis = cus_dis_pre + "@" + cus_dis_po;

                    pur_dis_pre = PusDisText_.getText().toString().trim();
                    pur_dis_po = PusDisText.getText().toString().trim();
                    pur_dis = pur_dis_pre + "@" + pur_dis_po;

                    card_dis_pre = CardDiscountText_.getText().toString().trim();
                    card_dis_po = CardDiscountText.getText().toString().trim();
                    card_dis = card_dis_pre + "@" + card_dis_po;

                    grand_total = totalAmountTV.getText().toString().trim();

                    dbHelper.updateinsertCustomerInfo("1", special_dis, cus_dis, pur_dis, card_dis, grand_total,NormalDiscountSwitchV, CustomerDiscountSwitchV, PurchaseDiscountSwitchV, cardDiscountSwitchV,cart_total);

                } else if (!isChecked) {
                    PurchaseDiscountSwitchV = "2";
                    prevAmmount = totalAmountTV.getText().toString().trim();
                    FinalAmmountInTaka = Double.valueOf(prevAmmount) + Double.valueOf(PurAmmountInTaka);
                    FinalAmmountInTakaSTRING = new DecimalFormat("##.##").format(FinalAmmountInTaka);
                    totalAmountTV.setText(FinalAmmountInTakaSTRING);


                    special_dis_pre = normalDisTV_.getText().toString().trim();
                    special_dis_po = normalDisTV.getText().toString().trim();
                    special_dis = special_dis_pre + "@" + special_dis_po;

                    cus_dis_pre = CusDisText_.getText().toString().trim();
                    cus_dis_po = CusDisText.getText().toString().trim();
                    cus_dis = cus_dis_pre + "@" + cus_dis_po;

                    pur_dis_pre = PusDisText_.getText().toString().trim();
                    pur_dis_po = PusDisText.getText().toString().trim();
                    pur_dis = pur_dis_pre + "@" + pur_dis_po;

                    card_dis_pre = CardDiscountText_.getText().toString().trim();
                    card_dis_po = CardDiscountText.getText().toString().trim();
                    card_dis = card_dis_pre + "@" + card_dis_po;

                    grand_total = totalAmountTV.getText().toString().trim();

                    dbHelper.updateinsertCustomerInfo("1", special_dis, cus_dis, pur_dis, card_dis, grand_total,NormalDiscountSwitchV, CustomerDiscountSwitchV, PurchaseDiscountSwitchV, cardDiscountSwitchV,cart_total);


                }
            }
        });
        cardDiscountSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cardDiscountSwitchV = "1";
                    prevAmmount = totalAmountTV.getText().toString().trim();
                    FinalAmmountInTaka = Double.valueOf(prevAmmount) - Double.valueOf(CARDdiscount);
                    FinalAmmountInTakaSTRING = new DecimalFormat("##.##").format(FinalAmmountInTaka);
                    totalAmountTV.setText(FinalAmmountInTakaSTRING);

                    special_dis_pre = normalDisTV_.getText().toString().trim();
                    special_dis_po = normalDisTV.getText().toString().trim();
                    special_dis = special_dis_pre + "@" + special_dis_po;

                    cus_dis_pre = CusDisText_.getText().toString().trim();
                    cus_dis_po = CusDisText.getText().toString().trim();
                    cus_dis = cus_dis_pre + "@" + cus_dis_po;

                    pur_dis_pre = PusDisText_.getText().toString().trim();
                    pur_dis_po = PusDisText.getText().toString().trim();
                    pur_dis = pur_dis_pre + "@" + pur_dis_po;

                    card_dis_pre = CardDiscountText_.getText().toString().trim();
                    card_dis_po = CardDiscountText.getText().toString().trim();
                    card_dis = card_dis_pre + "@" + card_dis_po;

                    grand_total = totalAmountTV.getText().toString().trim();

                    dbHelper.updateinsertCustomerInfo("1", special_dis, cus_dis, pur_dis, card_dis, grand_total,NormalDiscountSwitchV, CustomerDiscountSwitchV, PurchaseDiscountSwitchV, cardDiscountSwitchV,cart_total);

                } else if (!isChecked) {

                    cardDiscountSwitchV = "2";
                    prevAmmount = totalAmountTV.getText().toString().trim();
                    FinalAmmountInTaka = Double.valueOf(prevAmmount) + Double.valueOf(CARDdiscount);
                    FinalAmmountInTakaSTRING = new DecimalFormat("##.##").format(FinalAmmountInTaka);
                    totalAmountTV.setText(FinalAmmountInTakaSTRING);


                    special_dis_pre = normalDisTV_.getText().toString().trim();
                    special_dis_po = normalDisTV.getText().toString().trim();
                    special_dis = special_dis_pre + "@" + special_dis_po;

                    cus_dis_pre = CusDisText_.getText().toString().trim();
                    cus_dis_po = CusDisText.getText().toString().trim();
                    cus_dis = cus_dis_pre + "@" + cus_dis_po;

                    pur_dis_pre = PusDisText_.getText().toString().trim();
                    pur_dis_po = PusDisText.getText().toString().trim();
                    pur_dis = pur_dis_pre + "@" + pur_dis_po;

                    card_dis_pre = CardDiscountText_.getText().toString().trim();
                    card_dis_po = CardDiscountText.getText().toString().trim();
                    card_dis = card_dis_pre + "@" + card_dis_po;

                    grand_total = totalAmountTV.getText().toString().trim();

                    dbHelper.updateinsertCustomerInfo("1", special_dis, cus_dis, pur_dis, card_dis, grand_total,NormalDiscountSwitchV, CustomerDiscountSwitchV, PurchaseDiscountSwitchV, cardDiscountSwitchV,cart_total);


                }
            }
        });


        special_dis = "0@0";
        cus_dis = "0@0";
        pur_dis= "0@0";
        card_dis= "0@0";
        GRAND_TOTAL = totalAmountTV.getText().toString().trim();
        dbHelper.updateinsertCustomerInfo("1", special_dis, cus_dis, pur_dis, card_dis, GRAND_TOTAL,NormalDiscountSwitchV, CustomerDiscountSwitchV, PurchaseDiscountSwitchV, cardDiscountSwitchV,cart_total);



        return v;
    }

//    @Override
//    public void setUserVisibleHint(boolean isFragmentVisible_) {
//        super.setUserVisibleHint(_hasLoadedOnce);
//
//
//        if (this.isVisible()) {
//            // we check that the fragment is becoming visible
//            if (!isFragmentVisible_ && !_hasLoadedOnce) {
//                //run your async task here since the user has just focused on your fragment
//                _hasLoadedOnce = true;
//            }
//        }
//    }


    private void setSpinnerData_For_CardPromotionSpinner(String url, final String storeID) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        promotionList = new ArrayList<>();
        promotionDetails = new ArrayList<>();
        promotionList.add(0, "Select One");
        promotionDetails.add(0, "null" + "," + "0" + "," + "null");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("response");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String Title = jsonObject1.getString("title");
                        String TypeId = jsonObject1.getString("type_id");
                        String DiscountRate = jsonObject1.getString("discount_rate");
                        String PromotionId = jsonObject1.getString("promotion_id");

                        //customersList.add(i,sched.getFull_name()+","+sched.getCustomer_type_name()+","+sched.getPhone()+","+sched.getBalance()+","+sched.getPoints());
                        // collectionOfProductName.add(sched.getFull_name()+","+sched.getCustomer_type_name()+","+sched.getPhone()+","+sched.getBalance()+","+sched.getPoints());

                        // i = i+1;

                        promotionList.add(i + 1, Title);


                        promotionDetails.add(i + 1, TypeId + "," + DiscountRate + "," + PromotionId);
                    }

                    ArrayAdapter<String> customerTypeAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, promotionList);
                    customerTypeAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                    promotionSpinner.setAdapter(customerTypeAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                error.printStackTrace();
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               // params.put("store_ide", "1");
                // params.put("product_name", productId);
                // params.put("batch_s", productBatch);
                params.put("store_id", storeID);

                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void setCustomerAndPurchaseDiscount(final String tot_product_amt, final String customer_id, final String store_id, String url) {


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObject2 = jsonObject.getJSONObject("response");

                    //JSONArray jsonArray = jsonObject.getJSONArray("response");

                    CustomerDiscountFromJson = jsonObject2.getString("customer_dis");
                    if (CustomerDiscountFromJson.equals("null")) CustomerDiscountFromJson = "0";


                    JSONObject jsonObject3 = jsonObject2.getJSONObject("purchase_dis");
                    Rate = jsonObject3.getString("rate");
                    PurAmmountInTaka = jsonObject3.getString("amount");
                    if (PurAmmountInTaka.equals("")) PurAmmountInTaka = "0";
                    if (Rate.equals("")) Rate = "0";

              //      Log.e("Response", CustomerDiscountFromJson + " - " + " Rate:" + Rate + " Ammount : " + PurAmmountInTaka);

                    Double yyy = Double.valueOf(TotalAmmountFromDB) * Double.valueOf(CustomerDiscountFromJson) / 100;
                    cusDiscountResult = new DecimalFormat("##.##").format(yyy);

                    CusDisText_.setText(CustomerDiscountFromJson);
                    CusDisText.setText(cusDiscountResult);
                    PusDisText_.setText(Rate);
                    PusDisText.setText(PurAmmountInTaka);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                error.printStackTrace();
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tot_product_amt", tot_product_amt);
                params.put("customer_id", customer_id);
                params.put("store_id", store_id);

                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }


//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//
//            setCustomerAndPurchaseDiscount(TotalAmmountFromDB, CUSTOMER_ID, "1", cusDis_API_URL);
//            //Write down your refresh code here, it will call every time user come to this fragment.
//            //If you are using listview with custom adapter, just call notifyDataSetChanged().
//        }
//    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        if (allowRefresh)
//        {
//            count++;
//            if(count<4){
//                setCustomerAndPurchaseDiscount(TotalAmmountFromDB, CUSTOMER_ID, "1", cusDis_API_URL);
//            }else allowRefresh = false;
//
//            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
//        }
//    }



}