package syntechbd.com.posspot;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import syntechbd.com.posspot.adapter.AddToCartAdapter;
import syntechbd.com.posspot.app.AppSingleton;
import syntechbd.com.posspot.app.DBHelper;
import syntechbd.com.posspot.modal.AddTOCartModal;
import syntechbd.com.posspot.modal.CartModal;
import syntechbd.com.posspot.modal.Overview_ListView;
import syntechbd.com.posspot.modal.StockProductsModal;
import syntechbd.com.posspot.modal.StockProductsModal2;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSelectProduct extends Fragment {

    private static String TAG = FragmentSelectProduct.class.getSimpleName();

    RequestQueue requestQueue;

    static final int INTERNET_REQ = 23;
    static final String REQ_TAG = "VACTIVITY";

    Integer ids = 0;
    RadioButton p, b;
    Button next, next2;
    ImageButton cameraBtn, SrcBtn;
    public static AutoCompleteTextView ProductNameATV;
    String productlistURL, addtoCurtURL, YouruserName, Store_Id, productId, ProductNameInET;
    private static String productCode;
    SharedPreferences pref;
    ListView listView;
    AddToCartAdapter adapter;
    Context context;

    ArrayList<CartModal> productList = new ArrayList<CartModal>();

    ArrayAdapter<String> collectionOfProductNameAdapter;
    private ArrayList<String> collectionOfProductName = new ArrayList<>();

    public static ArrayList<String> batchList;
    public static String productBatch, selection;
    public static String productId_Batch;

    //todo...................
    private String discount, def_vat;

    ArrayList<CartModal> listOverview;

    public static String ProductName, ProductCode, ProductStock, TotalTaka, SelectedBatch, ProductQty, ProductPrice, ProductDiscount, ProductDiscountPromotionID;


    public static DBHelper db;

    private boolean Percent, Taka;
    Double discountInPercent, defaultVAT, est_price;
    public int qtyCounter;
    private String deFaUlT_discount = "0";
    private String deFaUlT_VAT = "0";
    boolean productSelected = false;

    String def_vat_amt, discount_type, stock_id, cat_id, subcat_id, brand_id, unit_id, store_from, cart_total, original_product_price, pro_id;

    String station_id = "1";
    private ImageButton SrcBtn_C;

    public FragmentSelectProduct() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment_select_product, container, false);

        context = getContext();
        db = new DBHelper(context);


        batchList = new ArrayList<>();

        pref = getActivity().getSharedPreferences("UserInfo", 0); // 0 - for private mode


        YouruserName = pref.getString("user_name", null);

        Store_Id = pref.getString("store_id", null);
        station_id = pref.getString("station_id", null);


        productlistURL = "http://" + YouruserName + ".posspot.com/api/sales_api/sale_products_auto_list";
        addtoCurtURL = "http://" + YouruserName + ".posspot.com/api/sales_api/add_cart_for_sales";
        loadAutoSuggestionData(productlistURL);

        p = (RadioButton) v.findViewById(R.id.productOrCOdeSelectenBTN);
        b = (RadioButton) v.findViewById(R.id.barCodeSelectedBTN);
        ProductNameATV = (AutoCompleteTextView) v.findViewById(R.id.productNameATV);
        listView = (ListView) v.findViewById(R.id.addtoCartLV);


        ProductNameATV.setThreshold(1);

        collectionOfProductNameAdapter = new ArrayAdapter<String>
                (context, R.layout.dropdown, collectionOfProductName);


        ProductNameATV.setAdapter(collectionOfProductNameAdapter);


        ProductNameATV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                selection = (String) adapterView.getItemAtPosition(i);

                int startIndex = selection.indexOf("~");
                int endIndex = selection.indexOf("[");


                productBatch = selection.substring(startIndex + 1, endIndex);
                productId = selection.substring(selection.indexOf("[") + 1, selection.indexOf("]"));



                int spaceIndex = selection.indexOf(" ");
                if (spaceIndex != -1) {
                    productCode = selection.substring(0, spaceIndex);
                } else productCode = "0";

                ProductNameInET = selection.substring(spaceIndex + 1, startIndex);


                productBatch = productBatch.trim();
                ProductNameATV.setText(ProductNameInET + " (" + productBatch + ")");
                productSelected = true;
                batchList.clear();


            }
        });


        cameraBtn = (ImageButton) v.findViewById(R.id.cam);
        SrcBtn = (ImageButton) v.findViewById(R.id.src);
        SrcBtn_C = (ImageButton) v.findViewById(R.id.src_c);


        if (p.isChecked()) {
            cameraBtn.setVisibility(View.GONE);
            SrcBtn.setVisibility(View.VISIBLE);

        } else if (b.isChecked()) {

            cameraBtn.setVisibility(View.VISIBLE);
            SrcBtn.setVisibility(View.GONE);
        }

        p.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.setChecked(false);
                SrcBtn.setVisibility(View.VISIBLE);
                cameraBtn.setVisibility(View.GONE);
                SrcBtn_C.setVisibility(View.GONE);
            }
        });

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p.setChecked(false);
                SrcBtn.setVisibility(View.GONE);
                cameraBtn.setVisibility(View.VISIBLE);
            }
        });

        next = (Button) v.findViewById(R.id.nextBTN);
        next2 = (Button) v.findViewById(R.id.nextBTN2);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        next2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraBtn.setVisibility(View.VISIBLE);

            }
        });

        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
//                startActivity(intent);

                //ScanBar(v);
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        //TODO your background code

                        Intent intent = new Intent(getContext(), ScanActivity.class);
                        startActivity(intent);

                    }
                });

                SrcBtn_C.setVisibility(View.VISIBLE);
                SrcBtn.setVisibility(View.GONE);


            }
        });

        SrcBtn_C.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String example = ProductNameATV.getText().toString().trim();
                productBatch = example.substring(example.lastIndexOf("-") + 1);
                AddToCartDataForCamera(ProductNameATV.getText().toString().trim(), productBatch, addtoCurtURL);


                ProductNameATV.setText("");
                SrcBtn_C.setVisibility(View.GONE);
            }
        });
        SrcBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //testPost();
                ProductNameATV.setText("");
                if (productSelected) {
                    AddToCartData(productId.trim(), productBatch.trim(), addtoCurtURL);


                    DBHelper db = new DBHelper(getContext());
                    listOverview = db.getOverviewList();
                    adapter = new AddToCartAdapter(getContext(), listOverview);

                   // adapter.notifyDataSetChanged();
                    listView.setAdapter(adapter);
                    listView.invalidateViews();

                    adapter.notifyDataSetChanged();


                    loadListView();
                } else {

                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                    alertDialog.setTitle("Alart!");
                    alertDialog.setMessage("Select a product first !");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();


                }


                productSelected = false;
            }
        });



        requestQueue = AppSingleton.getInstance(this.getContext())
                .getRequestQueue();

        listOverview = db.getOverviewList();
        adapter = new AddToCartAdapter(getContext(), listOverview);

      //  adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
        listView.invalidateViews();

        adapter.notifyDataSetChanged();


        return v;
    }



    public void loadListView() {
        DBHelper db = new DBHelper(getContext());
        listOverview = db.getOverviewList();
        adapter = new AddToCartAdapter(getContext(), listOverview);
        //adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
        listView.invalidateViews();


//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String name = listOverview.get(position).getProductQty().toString();
//                String amount = listOverview.get(position).getProductPrice().toString();
//                String date = listOverview.get(position).getProductDiscount().toString();
//                String note = listOverview.get(position).getTotalTaka().toString();
//                ids = listOverview.get(position).getColumnId();
//
//                ShowDialogBox(name, amount, date, note,  position);
//
//            }
//        });
    }



    public void updateInformation() {


        String finalDiscountInTaka = "0";

        discount = deFaUlT_discount ;
        //discount = "3 % 1";
        def_vat = deFaUlT_VAT;

        if (!discount.equals("0")) {


            String[] splited = discount.split("\\s+");

            String split_one = splited[0];
            String split_second = splited[1];
            String split_three = splited[2];


            if (split_second.equals("%")) {

                Percent = true;
                discount_type = "%";
                //Toast.makeText(context, "Got % :" + split_second, Toast.LENGTH_LONG).show();
                discountInPercent = Double.valueOf(split_one.trim()) / 100;
                discount = String.valueOf(discountInPercent);
                ProductDiscountPromotionID = split_three.trim();
//                Log.i("Product Price", ProductPrice);
                // Toast.makeText(context,"Product Price: "+ ProductPrice, Toast.LENGTH_LONG).show();
                Double discoun = qtyCounter * Double.valueOf(ProductPrice) * discountInPercent;
                finalDiscountInTaka = String.valueOf(discoun);

                //  Toast.makeText(context, "Discount :" + discount + ",PromotionID :" + ProductDiscountPromotionID, Toast.LENGTH_LONG).show();
            }
            if (split_second.equals("TK")) {

                discount_type = "TK";
                // Toast.makeText(context, "Got TAKA :" + split_second, Toast.LENGTH_LONG).show();
                discount = split_one.trim();
                ProductDiscountPromotionID = split_three.trim();
                finalDiscountInTaka = discount;
                //  Toast.makeText(context, "Discount :" + discount + "Takka,PromotionID :" + ProductDiscountPromotionID, Toast.LENGTH_LONG).show();
            }

            ProductDiscount = finalDiscountInTaka;


        } else {

            discount_type = "";
            ProductDiscount = discount;
            ProductDiscountPromotionID = "";
        }


        defaultVAT = Double.valueOf(def_vat) / 100;
        est_price = Double.valueOf(ProductPrice) - Double.valueOf(ProductDiscount);
        def_vat_amt = String.valueOf(defaultVAT * est_price);

        Double totalTakaInSelectProductRow = est_price + Double.valueOf(def_vat_amt);
        TotalTaka = String.valueOf(totalTakaInSelectProductRow);

        cart_total = "0";

        ProductQty = String.valueOf(qtyCounter);

        original_product_price = String.valueOf(qtyCounter * Double.valueOf(ProductPrice));
//def_vat_amt,discount_type,stock_id,cat_id,subcat_id,brand_id,unit_id,store_from,station_id,cart_total,original_product_price,pro_id
//def_vat_amt,discount_type,,,,,,station_id,cart_total,original_product_price,
        CartModal cartModal = new CartModal();
        cartModal.setProductName(ProductName);
        cartModal.setProductCode(ProductCode);
        cartModal.setProductStock(ProductStock);
        cartModal.setTotalTaka(TotalTaka);
        cartModal.setSelectedBatch(SelectedBatch);
        cartModal.setProductQty(ProductQty);
        cartModal.setProductPrice(ProductPrice);
        cartModal.setProductDiscount(ProductDiscount);
        cartModal.setProductDiscountPromotionID(ProductDiscountPromotionID);
        cartModal.setVat(def_vat_amt);
        cartModal.setDefaultDiscount(deFaUlT_discount);
        cartModal.setDeFaUlT_VAT(deFaUlT_VAT);

        cartModal.setDiscount_type(discount_type); //No value here
        cartModal.setStock_id(stock_id);
        cartModal.setCat_id(cat_id);
        cartModal.setSubcat_id(subcat_id);
        cartModal.setBrand_id(brand_id);
        cartModal.setUnit_id(unit_id);
        cartModal.setStore_from(store_from);
        cartModal.setStation_id(station_id); // todo: add station id from login session
        cartModal.setCart_total(cart_total); //No value here
        cartModal.setOriginal_product_price(original_product_price); // No value here
        cartModal.setPro_id(pro_id);

        productList.add(cartModal);



        if (qtyCounter > Double.valueOf(ProductStock)) {
            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setTitle("Stock Unavailable!");
            alertDialog.setMessage("This product is not available!");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        } else

            db.updateAddeedItem(ProductCode, ProductName, SelectedBatch, ProductPrice, ProductStock, ProductQty, ProductDiscount, ProductDiscountPromotionID, def_vat_amt, TotalTaka, deFaUlT_discount, deFaUlT_VAT, discount_type, stock_id, cat_id, subcat_id, brand_id, unit_id, store_from, station_id, original_product_price, pro_id);

        DBHelper db = new DBHelper(getContext());
        listOverview = db.getOverviewList();
        adapter = new AddToCartAdapter(getContext(), listOverview);

        //adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
        listView.invalidateViews();

        adapter.notifyDataSetChanged();

        qtyCounter = 0;
    }


    public void setInformation() {


        String finalDiscountInTaka = "0";

        discount = deFaUlT_discount ;
        //discount = "3 % 1";
        def_vat = deFaUlT_VAT;

        if (!discount.equals("0")) {


            String[] splited = discount.split("\\s+");

            String split_one = splited[0];
            String split_second = splited[1];
            String split_three = splited[2];


            if (split_second.equals("%")) {

                Percent = true;
                discount_type = "%";
                discountInPercent = Double.valueOf(split_one.trim()) / 100;
                discount = String.valueOf(discountInPercent);
                ProductDiscountPromotionID = split_three.trim();
//                Log.i("Product Price", ProductPrice);
                Double discoun = qtyCounter * Double.valueOf(ProductPrice) * discountInPercent;
                finalDiscountInTaka = String.valueOf(discoun);

                //  Toast.makeText(context, "Discount :" + discount + ",PromotionID :" + ProductDiscountPromotionID, Toast.LENGTH_LONG).show();
            }
            if (split_second.equals("TK")) {

                discount_type = "TK";
                Toast.makeText(context, "Got TAKA :" + split_second, Toast.LENGTH_LONG).show();
                discount = split_one.trim();
                ProductDiscountPromotionID = split_three.trim();
                finalDiscountInTaka = discount;
                //  Toast.makeText(context, "Discount :" + discount + "Takka,PromotionID :" + ProductDiscountPromotionID, Toast.LENGTH_LONG).show();
            }

            ProductDiscount = finalDiscountInTaka;


        } else {

            discount_type = "";
            ProductDiscount = discount;
            ProductDiscountPromotionID = "";
        }


        defaultVAT = Double.valueOf(def_vat) / 100;
        est_price = Double.valueOf(ProductPrice) - Double.valueOf(ProductDiscount);
        def_vat_amt = String.valueOf(defaultVAT * est_price);

        Double totalTakaInSelectProductRow = est_price + Double.valueOf(def_vat_amt);
        TotalTaka = String.valueOf(totalTakaInSelectProductRow);

        cart_total = "0";

        ProductQty = String.valueOf(qtyCounter);

        original_product_price = String.valueOf(qtyCounter * Double.valueOf(ProductPrice));
        CartModal cartModal = new CartModal();
        cartModal.setProductName(ProductName);
        cartModal.setProductCode(ProductCode);
        cartModal.setProductStock(ProductStock);
        cartModal.setTotalTaka(TotalTaka);
        cartModal.setSelectedBatch(SelectedBatch);
        cartModal.setProductQty(ProductQty);
        cartModal.setProductPrice(ProductPrice);
        cartModal.setProductDiscount(ProductDiscount);
        cartModal.setProductDiscountPromotionID(ProductDiscountPromotionID);
        cartModal.setVat(def_vat_amt);
        cartModal.setDefaultDiscount(deFaUlT_discount);
        cartModal.setDeFaUlT_VAT(deFaUlT_VAT);

        cartModal.setDiscount_type(discount_type); //No value here
        cartModal.setStock_id(stock_id);
        cartModal.setCat_id(cat_id);
        cartModal.setSubcat_id(subcat_id);
        cartModal.setBrand_id(brand_id);
        cartModal.setUnit_id(unit_id);
        cartModal.setStore_from(store_from);
        cartModal.setStation_id(station_id); // todo: add station id from login session
        cartModal.setCart_total(cart_total); //No value here
        cartModal.setOriginal_product_price(original_product_price); // No value here
        cartModal.setPro_id(pro_id);

        productList.add(cartModal);

//String def_vat_amt,String discount_type,String stock_id,String cat_id,String subcat_id,String brand_id,String unit_id,String store_from,String station_id,String cart_total,String original_product_price,String pro_id

        db.insertAdd_To_Cart(ProductCode, ProductName, SelectedBatch, ProductPrice, ProductStock, ProductQty, ProductDiscount, ProductDiscountPromotionID, def_vat_amt, TotalTaka, deFaUlT_discount, deFaUlT_VAT, discount_type, stock_id, cat_id, subcat_id, brand_id, unit_id, store_from, station_id, original_product_price, pro_id);

        qtyCounter = 0;

        DBHelper db = new DBHelper(getContext());
        listOverview = db.getOverviewList();
        adapter = new AddToCartAdapter(getContext(), listOverview);

        //adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
        listView.invalidateViews();

        adapter.notifyDataSetChanged();

    }


    @Override
    public void onStop() {
        super.onStop();
        if (requestQueue != null) {
            requestQueue.cancelAll(REQ_TAG);
        }
    }

    private void loadAutoSuggestionData(String productlistURL) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, productlistURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("response");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String name = jsonObject1.getString("product_name");
                        String batchNo = jsonObject1.getString("batch_no");
                        String id = jsonObject1.getString("id_product");
                        String code = jsonObject1.getString("product_code");

                        StockProductsModal2 stockProductsModal = new StockProductsModal2();
                        stockProductsModal.setProduct_batch(batchNo);
                        stockProductsModal.setProduct_id(id);
                        stockProductsModal.setProduct_name(name);
                        stockProductsModal.setProduct_code(code);


                        //   String productNameList =  stockProductsModal.getProductName();

                        //   collectionOfVarsityName.add(stockProductsModal.getProduct_name());
                        collectionOfProductName.add(code + " " + name + "~" + batchNo + " " + "[" + id + "]");

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                error.printStackTrace();
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("request", "");
                params.put("store_id", "1");

                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }


    private void AddToCartData(final String productId, final String productBatch, String url) {


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObject2 = jsonObject.getJSONObject("response");

                    deFaUlT_discount = jsonObject2.getString("discount");

                    JSONObject jsonObjectX = new JSONObject(response);
                    JSONObject jsonObject3 = jsonObjectX.getJSONObject("response");
                    deFaUlT_VAT = jsonObject3.getString("def_vat");


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                try {

                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonArray2 = jsonObject.getJSONObject("response");
                    JSONArray jsonArray = jsonArray2.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {


                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        stock_id = jsonObject1.getString("id_stock");
                        pro_id = jsonObject1.getString("id_product");
                        ProductCode = jsonObject1.getString("product_code");
                        ProductName = jsonObject1.getString("product_name");
                        cat_id = jsonObject1.getString("cat_id");
                        subcat_id = jsonObject1.getString("subcat_id");
                        brand_id = jsonObject1.getString("brand_id");
                        unit_id = jsonObject1.getString("unit_id");
                        String is_vatable = jsonObject1.getString("is_vatable");
                        store_from = jsonObject1.getString("store_id");
                        SelectedBatch = jsonObject1.getString("batch_no");
                        ProductStock = jsonObject1.getString("total_qty");
                        ProductPrice = jsonObject1.getString("selling_price_est");
                        String discount_amt = jsonObject1.getString("discount_amt");


                    //    Log.e("Response", ProductCode + ProductName + SelectedBatch + ProductStock + ProductPrice);

                        DBHelper db = new DBHelper(context.getApplicationContext());

                        int rowID;
                        int batchCounter;

                        batchCounter = db.ifFoundBatch(productBatch);
//                        Log.i("I got %d number row", batchCounterX);
                        if (batchCounter == 1) {
                            //  Snackbar.make(v, "Found multiple batches in row. Use another batch ", Snackbar.LENGTH_LONG).setAction("No action", null).show();
                            qtyCounter = db.getProductQty(productBatch);
                            qtyCounter = qtyCounter + 1;
                            updateInformation();


                        } else if (batchCounter > 1) {
//                            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//                            alertDialog.setTitle("Alert");
//                            alertDialog.setMessage("Found multiple batches in row. Delete All & Add Again");
//                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            dialog.dismiss();
//                                        }
//                                    });
//                            alertDialog.show();

                            qtyCounter = db.getProductQty(productBatch);
                            qtyCounter = qtyCounter + 1;
                            updateInformation();
                        } else {

                            qtyCounter = 1;
                            setInformation();
                        }


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                error.printStackTrace();
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("store_ide", "1");
                params.put("product_name", productId + "-" + productBatch);
                params.put("batch_s", productBatch);
                params.put("acc_type", "Yes");

                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void AddToCartDataForCamera(final String productId_Batch, final String productBatch, String url) {


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObject2 = jsonObject.getJSONObject("response");
                    //JSONArray jsonArray = jsonObject.getJSONArray("response");

                    deFaUlT_discount = jsonObject2.getString("discount");

                    JSONObject jsonObjectX = new JSONObject(response);
                    JSONObject jsonObject3 = jsonObjectX.getJSONObject("response");
                    deFaUlT_VAT = jsonObject3.getString("def_vat");


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                try {

                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonArray2 = jsonObject.getJSONObject("response");
                    JSONArray jsonArray = jsonArray2.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {


                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        stock_id = jsonObject1.getString("id_stock");
                        pro_id = jsonObject1.getString("id_product");
                        ProductCode = jsonObject1.getString("product_code");
                        ProductName = jsonObject1.getString("product_name");
                        cat_id = jsonObject1.getString("cat_id");
                        subcat_id = jsonObject1.getString("subcat_id");
                        brand_id = jsonObject1.getString("brand_id");
                        unit_id = jsonObject1.getString("unit_id");
                        String is_vatable = jsonObject1.getString("is_vatable");
                        store_from = jsonObject1.getString("store_id");
                        SelectedBatch = jsonObject1.getString("batch_no");
                        ProductStock = jsonObject1.getString("total_qty");
                        ProductPrice = jsonObject1.getString("selling_price_est");
                        String discount_amt = jsonObject1.getString("discount_amt");


                        Log.e("Response", ProductCode + ProductName + SelectedBatch + ProductStock + ProductPrice);

                        DBHelper db = new DBHelper(context.getApplicationContext());

                        int rowID;
                        int batchCounter;


                        batchCounter = db.ifFoundBatch(productBatch);
//                        Log.i("I got %d number row", batchCounterX);
                        if (batchCounter == 1) {
                            //  Snackbar.make(v, "Found multiple batches in row. Use another batch ", Snackbar.LENGTH_LONG).setAction("No action", null).show();
                            qtyCounter = db.getProductQty(productBatch);
                            qtyCounter = qtyCounter + 1;
                            updateInformation();


                        } else if (batchCounter > 1) {
//                            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//                            alertDialog.setTitle("Alert");
//                            alertDialog.setMessage("Found multiple batches in row. Delete All & Add Again");
//                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            dialog.dismiss();
//                                        }
//                                    });
//                            alertDialog.show();

                            qtyCounter = db.getProductQty(productBatch);
                            qtyCounter = qtyCounter + 1;
                            updateInformation();
                        } else {

                            qtyCounter = 1;
                            setInformation();
                        }
                    }
                    //productList.add(addTOCartModal);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                error.printStackTrace();
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("store_ide", "1");
                params.put("product_name", productId_Batch);
                //   params.put("batch_s", productBatch);
                params.put("acc_type", "No");

                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private Response.Listener<String> getPostResponseListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        };
    }

    private Response.ErrorListener getPostErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        };
    }


}