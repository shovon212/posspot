package syntechbd.com.posspot;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import syntechbd.com.posspot.app.AppController;
import syntechbd.com.posspot.modal.DashboardModals;

import static syntechbd.com.posspot.app.AppController.TAG;

public class LoginFragmenta extends AppCompatActivity {

    EditText username, password;
    Button login;
    static TextView statusTV;
    private String baseUrl;
    private String YouruserName;
    private String YourpassWorD;
    RequestQueue requestQueue;
    private String jsonResponse;
    private DashboardModals dashboardModals;

    SharedPreferences pref  ; // 0 - for private mode
    SharedPreferences.Editor editor ;

    Context context;

    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getApplicationContext();




         pref = getApplicationContext().getSharedPreferences("UserInfo", 0); // 0 - for private mode



        try {
            if(pref.getBoolean("keep_logged_in", Boolean.parseBoolean(null))){


                finish();

                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        setContentView(R.layout.activity_login_fragmenta);


        dashboardModals = new DashboardModals();



        username = (EditText) findViewById(R.id.unameET);
        password = (EditText) findViewById(R.id.passET);
        login = (Button) findViewById(R.id.logMeInBTN);
        statusTV = (TextView) findViewById(R.id.statusTV);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        requestQueue = Volley.newRequestQueue(this);
        progressBar.setVisibility(View.GONE);


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isNetworkConnected()) {
                    progressBar.setVisibility(View.VISIBLE);


                    YouruserName = username.getText().toString().trim();
                    YourpassWorD = password.getText().toString().trim();


                    if (YouruserName.equals("") || YourpassWorD.equals("")) {
                        statusTV.setText("Type Username & Password first!");

                        username.setText("");
                        password.setText("");

                        progressBar.setVisibility(View.GONE);
                    } else {
                        String MotherApi = "http://posspot.com/api/authentication/check_user_auth?userName=";
                        String addingUsername = MotherApi.concat(YouruserName + "&passWorD=");
                        baseUrl = addingUsername.concat(YourpassWorD + "&userType=1");


//                pDialog = new ProgressDialog(t);
//                pDialog.setMessage("Logging...");
//                pDialog.setCancelable(false);
                        makeJsonObjectRequest();

                    }

                }else statusTV.setText("Check Your Internet Connection!");




            }
        });
    }


    private void makeJsonObjectRequest() {

        //showpDialog();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                baseUrl, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject info = response.getJSONObject("response");
                    String user_name = info.getString("user_name");
                    String user_id = info.getString("user_id");
                    String user_full_name = info.getString("user_full_name");
                    String user_email = info.getString("user_email");
                    String user_phone = info.getString("user_phone");
                    String store_id = info.getString("store_id");
                    String store_name = info.getString("store_name");
                    String station_id = info.getString("station_id");
                    String station_name = info.getString("station_name");
                    String logged_in = info.getString("logged_in");
                    String station_acc_id = info.getString("station_acc_id");

                    JSONObject InnerInfo = info.getJSONObject("info");


                    String total_stock = InnerInfo.getString("total_stock");
                    int total_station = Integer.parseInt(InnerInfo.getString("total_station"));
                    int used_station = Integer.parseInt(InnerInfo.getString("used_station"));
                    int total_store = Integer.parseInt(InnerInfo.getString("total_store"));
                    int used_store = Integer.parseInt(InnerInfo.getString("used_store"));
                    int total_user = Integer.parseInt(InnerInfo.getString("total_user"));
                    int used_user = Integer.parseInt(InnerInfo.getString("used_user"));



                    jsonResponse = "";
                    jsonResponse += "user_full_name: " + user_full_name + "\n\n";
                    jsonResponse += "total_stock: " + total_stock + "\n\n";
                    jsonResponse += "total_station: " + total_station + "\n\n";

                   // statusTV.setText(jsonResponse);



                    editor = pref.edit();

                    editor.putBoolean("keep_logged_in", true); // Storing boolean - true/false
                    editor.putString("user_full_name", user_full_name); // Storing string
                    editor.putString("user_email", user_email); // Storing string
                    editor.putString("user_name", YouruserName); // Storing string
                    editor.putString("user_password", YourpassWorD); // Storing string
                    editor.putString("store_id", store_id);
                    editor.putString("station_id", station_id);
                    editor.putString("station_acc_id", station_acc_id);



                    editor.putString("total_stock", total_stock); // Storing string
                    editor.putInt("total_station", total_station); // Storing integer
                    editor.putInt("used_station", used_station); // Storing integer


                    editor.putString("store_name", store_name); // Storing string
                    editor.putString("station_name", station_name); // Storing string


                    editor.putInt("total_store", total_store); // Storing integer
                    editor.putInt("used_store", used_store); // Storing integer


                    editor.putInt("total_user", total_user); // Storing integer
                    editor.putInt("used_user", used_user); // Storing integer


                    editor.apply();


                    finish();
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));

                    //progressdialog.dismiss();

                    progressBar.setVisibility(View.GONE);


                } catch (JSONException e) {
                    e.printStackTrace();
                    statusTV.setText("Wrong UserName or PassWord! Try Again!");
//                    Toast.makeText(getApplicationContext(),
//                            "Error: " + e.getMessage(),
//                            Toast.LENGTH_LONG).show();



                }


                // hidepDialog();

                progressBar.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
//                Toast.makeText(getApplicationContext(),
//                        error.getMessage(), Toast.LENGTH_SHORT).show();
                statusTV.setText("Wrong UserName or PassWord! Try Again!");

                username.setText("");
                password.setText("");
                // hide the progress dialog
                //hidepDialog();

                progressBar.setVisibility(View.GONE);
            }
        }) {
//            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                String userName = "pos01";
//                String password = "12345";
//                params.put("userName", userName);
//                params.put("passWorD", password);
//                return params;
//
//            }


            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();


//                String userName = "pos01";
//                String password = "12345";


//                headers.put("userName", userName);
//                headers.put("passWorD", password);
//                headers.put("Content-Type", "application/json");
//                 headers.put("Accept", "application/json");
//                headers.put("Host", "sms.posspot.com");
//               headers.put("Accept-Encoding", "utf-8");
                String creds = String.format("%s:%s", "posspot", "synt3ch");
                System.out.println(creds);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);
                return headers;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue

        AppController.getInstance().getRequestQueue().getCache().remove(baseUrl);
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }


    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


}
