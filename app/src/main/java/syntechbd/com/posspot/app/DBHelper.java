package syntechbd.com.posspot.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import syntechbd.com.posspot.modal.AddTOCartModal;
import syntechbd.com.posspot.modal.CartModal;
import syntechbd.com.posspot.modal.SalesInfoModal;

import static com.android.volley.VolleyLog.TAG;

public class DBHelper extends SQLiteOpenHelper {

// Creating String Name for DB info

    public static final String DATABASE_NAME = "SalesManager.db";
    public static final String CUSTOMER_TABLE_NAME = "CustomerTable";
    public static final String CUSTOMER_TABLE_COLUMN_ID = "Column_id";
    public static final String CUSTOMER_TABLE_COLUMN_CUSTOMERS_ID = "customers_id"; //'customer_id'
    public static final String CUSTOMER_TABLE_COLUMN_CUSTOMERS_NAME = "customers_name";
    public static final String CUSTOMER_TABLE_COLUMN_CUSTOMERS_MEMBER_TYPE = "member_type";
    public static final String CUSTOMER_TABLE_COLUMN_CUSTOMERS_CELL_NO = "cell_no";
    public static final String CUSTOMER_TABLE_COLUMN_CUSTOMERS_BALANCE = "balance";
    public static final String CUSTOMER_TABLE_COLUMN_CUSTOMERS_POINT = "point";
    public static final String CUSTOMER_special_dis = "special_dis"; //  special_dis'
    public static final String CUSTOMER_cus_dis = "cus_dis"; // ''cus_dis''
    public static final String CUSTOMER_pur_dis = "pur_dis"; // '''pur_dis''
    public static final String CUSTOMER_card_dis = "card_dis"; // ''card_dis''
    public static final String CUSTOMER_grand_total = "grand_total"; // ' grand_total '
    public static final String CUSTOMER_customer_code = "customer_code"; // 'customer_code'
    public static final String CUSTOMER_paid_amt = "paid_amt"; // 'paid_amt''

    public static final String CUSTOMER_ck_sp_dis = "ck_sp_dis"; // 'ck_sp_dis''
    public static final String CUSTOMER_ck_cus_dis = "ck_cus_dis"; // 'ck_cus_dis''
    public static final String CUSTOMER_ck_pur_dis = "ck_pur_dis"; // 'ck_pur_dis''
    public static final String CUSTOMER_ck_cart_dis = "ck_cart_dis"; // 'ck_cart_dis''


    public static final String CART_cart_total = "cart_total"; //'''cart_total''
  //  public static final String CART_card_promotion = "card_promotion"; //'''card_promotion''


    public static final String EXPENSE_COLUMN_CATEGORY_IDENTIFIER = "identifier";

    public static final String CART_TABLE_ADD = "Add_To_Cart";
    public static final String CART_ADD_COLUMN_ID = "columnId";   //INDEX

    public static final String CART_ADD_COLUMN_PRODUCT_CODE = "ProductCode"; //code
    public static final String CART_ADD_COLUMN_PRODUCT_NAME = "ProductName"; //name

    public static final String CART_ADD_COLUMN_SELECTED_BATCH = "SelectedBatch"; //'batch'
    public static final String CART_ADD_COLUMN_PRODUCT_PRICE = "ProductPrice"; //'unit_price'
    public static final String CART_ADD_COLUMN_PRODUCT_STOCK = "ProductStock"; //totoal_qty
    public static final String CART_ADD_COLUMN_PRODUCT_QTY = "ProductQty"; //'qty'


    public static final String CART_ADD_COLUMN_PRODUCT_DISCOUNT = "ProductDiscount"; //di sc  ou nt
    public static final String CART_ADD_COLUMN_PRODUCT_DISCOUNT_PROMOTION_ID = "ProductDiscountPromotionID"; //'pro_sale_id'
    public static final String CART_ADD_COLUMN_VAT = "Vat";//def_vat_amt'  (Ex: 300 er 5%  15 )
    public static final String CART_ADD_COLUMN_TOTALTAKA = "TotalTaka"; //'total_price'
    public static final String CART_ADD_COLUMN_DEFAULT_DISCOUNT = "DefaultDiscount"; //'discount er value ta just
    public static final String CART_ADD_COLUMN_DEFAULT_VAT = "DefaultVat"; //def_vat  (Ex:   5)
    public static final String CART_discount_type = "discount_type"; //'discount_type  (Ex:  %  or  TK )
    public static final String CART_stock_id = "stock_id"; //'stock_id''
    public static final String CART_cat_id = "cat_id"; // 'cat_id'
    public static final String CART_subcat_id = "subcat_id"; //' subcat_id'
    public static final String CART_brand_id = "brand_id"; // 'brand_id'
    public static final String CART_unit_id = "unit_id"; // 'unit_id'
    public static final String CART_store_from = "store_from"; //'store_from'
    public static final String CART_station_id = "station_id"; //''station_id'
    public static final String CART_original_product_price = "original_product_price"; //''original_product_price'
    public static final String CART_pro_id = "pro_id"; //pro_id


    private HashMap hp;
    private Context con;
    public static String ProductCodes;

    public DBHelper(Context context) {
        super(context, "SalesManager", null, 1);
        con = context;
    }




    // Creating Table. .. .. . ... . . ..

    @Override
    public void onCreate(SQLiteDatabase db) {


        String createTableOld = "create table CustomerTable (Column_id integer primary key , customers_id,customers_name,member_type,cell_no,balance,point,identifier);";
        String createTable = "create table CustomerTable (Column_id integer primary key UNIQUE, customers_id,customers_name,member_type,cell_no,balance,point,special_dis,cus_dis,pur_dis,card_dis,grand_total,customer_code,paid_amt,ck_sp_dis,ck_cus_dis,ck_pur_dis,ck_cart_dis,cart_total,identifier);";
        db.execSQL(createTable);

        String createTableAddOld = "create table Add_To_Cart (columnId integer primary key AUTOINCREMENT UNIQUE, ProductCode, ProductName, SelectedBatch,ProductPrice,ProductStock,ProductQty,ProductDiscount,ProductDiscountPromotionID,Vat,TotalTaka,DefaultDiscount,DefaultVat);";
        String createTableAdd = "create table Add_To_Cart (columnId integer primary key AUTOINCREMENT UNIQUE, ProductCode, ProductName, SelectedBatch,ProductPrice,ProductStock,ProductQty,ProductDiscount,ProductDiscountPromotionID,Vat,TotalTaka,DefaultDiscount,DefaultVat,discount_type,stock_id,cat_id,subcat_id,brand_id,unit_id,store_from,station_id,original_product_price,pro_id);";
        db.execSQL(createTableAdd);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS!");
        onCreate(db);
    }


    // todo: Adding Customers Information --------------------

    //    public void insertCustomerInfoOLD(String Column_id, String customers_id, String customers_name, String member_type, String cell_no, String balance, String point) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(CUSTOMER_TABLE_COLUMN_ID, Column_id);//product_id name, product_id value
//        values.put(CUSTOMER_TABLE_COLUMN_CUSTOMERS_ID, customers_id);//product_id name, product_id value
//        values.put(CUSTOMER_TABLE_COLUMN_CUSTOMERS_NAME, customers_name);//batch_no name, batch_no value
//        values.put(CUSTOMER_TABLE_COLUMN_CUSTOMERS_MEMBER_TYPE, member_type);
//        values.put(CUSTOMER_TABLE_COLUMN_CUSTOMERS_CELL_NO, cell_no);
//        values.put(CUSTOMER_TABLE_COLUMN_CUSTOMERS_BALANCE, balance);
//        values.put(CUSTOMER_TABLE_COLUMN_CUSTOMERS_POINT, point);
//
//
//        int id = (int) db.insertWithOnConflict(CUSTOMER_TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
//        if (id == -1) {
//            db.update(CUSTOMER_TABLE_NAME, values, "Column_id=?", new String[] {"1"});  // number 1 is the _id here, update to variable for your code
//        }
//
//
//        // Inserting Row
//       // db.insert(CUSTOMER_TABLE_NAME, null, values);//tableName, nullColumnHack, ContentValues
//
//        Log.e("Batch Inserted to db",values.toString());
//        db.close(); // Closing database connection
//
//    }
//
    public void insertCustomerInfo(String Column_id, String customers_id, String customers_name, String member_type, String cell_no, String balance, String point, String special_dis, String cus_dis, String pur_dis, String card_dis, String grand_total, String customer_code, String paid_amt, String ck_sp_dis, String ck_cus_dis, String ck_pur_dis, String ck_cart_dis, String cart_total) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CUSTOMER_TABLE_COLUMN_ID, Column_id);//product_id name, product_id value
        values.put(CUSTOMER_TABLE_COLUMN_CUSTOMERS_ID, customers_id);//product_id name, product_id value
        values.put(CUSTOMER_TABLE_COLUMN_CUSTOMERS_NAME, customers_name);//batch_no name, batch_no value
        values.put(CUSTOMER_TABLE_COLUMN_CUSTOMERS_MEMBER_TYPE, member_type);
        values.put(CUSTOMER_TABLE_COLUMN_CUSTOMERS_CELL_NO, cell_no);
        values.put(CUSTOMER_TABLE_COLUMN_CUSTOMERS_BALANCE, balance);
        values.put(CUSTOMER_TABLE_COLUMN_CUSTOMERS_POINT, point);


        values.put(CUSTOMER_special_dis, special_dis); //  special_dis'
        values.put(CUSTOMER_cus_dis, cus_dis); // ''cus_dis''
        values.put(CUSTOMER_pur_dis, pur_dis); // '''pur_dis''
        values.put(CUSTOMER_card_dis, card_dis); // ''card_dis''
        values.put(CUSTOMER_grand_total, grand_total); // ' grand_total '
        values.put(CUSTOMER_customer_code, customer_code); // 'customer_code'
        values.put(CUSTOMER_paid_amt, paid_amt); // 'paid_amt''

        values.put(CUSTOMER_ck_sp_dis, ck_sp_dis);
        values.put(CUSTOMER_ck_cus_dis, ck_cus_dis);
        values.put(CUSTOMER_ck_pur_dis, ck_pur_dis);
        values.put(CUSTOMER_ck_cart_dis, ck_cart_dis);


        values.put(CART_cart_total, cart_total); //'''cart_total''


        int id = (int) db.insertWithOnConflict(CUSTOMER_TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        if (id == -1) {
            db.update(CUSTOMER_TABLE_NAME, values, "Column_id=?", new String[]{"1"});  // number 1 is the _id here, update to variable for your code
        }

        // Inserting Row
        // db.insert(CUSTOMER_TABLE_NAME, null, values);//tableName, nullColumnHack, ContentValues

        Log.e("Batch Inserted to db", values.toString());
        db.close(); // Closing database connection

    }

    public void updateinsertCustomerInfo(String Column_id, String special_dis, String cus_dis, String pur_dis, String card_dis, String grand_total, String ck_sp_dis, String ck_cus_dis, String ck_pur_dis, String ck_cart_dis, String cart_total) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(CUSTOMER_TABLE_COLUMN_ID, Column_id);


        values.put(CUSTOMER_special_dis, special_dis); //  special_dis'
        values.put(CUSTOMER_cus_dis, cus_dis); // ''cus_dis''
        values.put(CUSTOMER_pur_dis, pur_dis); // '''pur_dis''
        values.put(CUSTOMER_card_dis, card_dis); // ''card_dis''
        values.put(CUSTOMER_grand_total, grand_total); // ' grand_total '

        values.put(CUSTOMER_ck_sp_dis, ck_sp_dis);
        values.put(CUSTOMER_ck_cus_dis, ck_cus_dis);
        values.put(CUSTOMER_ck_pur_dis, ck_pur_dis);
        values.put(CUSTOMER_ck_cart_dis, ck_cart_dis);

        values.put(CART_cart_total, cart_total); //'''cart_total''


        Log.e("Updated Customer In db", values.toString());
        // db.update("Add_To_Cart", values,CART_ADD_COLUMN_SELECTED_BATCH+ "=" +SelectedBatch , null );
       // db.update(CUSTOMER_TABLE_NAME, values, CUSTOMER_TABLE_COLUMN_ID + "= ?", new String[]{Column_id});
        //  return true;

        int id = (int) db.insertWithOnConflict(CUSTOMER_TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        if (id == -1) {
            db.update(CUSTOMER_TABLE_NAME, values, "Column_id=?", new String[]{Column_id});  // number 1 is the _id here, update to variable for your code
        }

    }

    public void updateinsertCustomerInfoGrandTotal(String Column_id, String grand_total) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(CUSTOMER_TABLE_COLUMN_ID, Column_id);


        values.put(CUSTOMER_grand_total, grand_total); // ' grand_total '


        Log.e("Updated GrandTotal", values.toString());
        // db.update("Add_To_Cart", values,CART_ADD_COLUMN_SELECTED_BATCH+ "=" +SelectedBatch , null );
        db.update(CUSTOMER_TABLE_NAME, values, CUSTOMER_TABLE_COLUMN_ID + "= ?", new String[]{Column_id});
        //  return true;

        int id = (int) db.insertWithOnConflict(CUSTOMER_TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        if (id == -1) {
            db.update(CUSTOMER_TABLE_NAME, values, "Column_id=?", new String[]{Column_id});  // number 1 is the _id here, update to variable for your code
        }
    }


    // todo: Adding Cart Information -------------------------------------------------------------

    //    public void insertAdd_To_CartOld(String ProductCode, String ProductName, String SelectedBatch, String ProductPrice,String ProductStock, String ProductQty, String ProductDiscount,String ProductDiscountPromotionID, String Vat, String TotalTaka, String DefaultDiscount, String DefaultVat  ) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//
//        values.put(CART_ADD_COLUMN_PRODUCT_CODE, ProductCode);
//        values.put(CART_ADD_COLUMN_PRODUCT_NAME, ProductName);
//        values.put(CART_ADD_COLUMN_SELECTED_BATCH, SelectedBatch);
//        values.put(CART_ADD_COLUMN_PRODUCT_PRICE, ProductPrice);
//        values.put(CART_ADD_COLUMN_PRODUCT_STOCK, ProductStock);
//        values.put(CART_ADD_COLUMN_PRODUCT_QTY, ProductQty);
//
//        values.put(CART_ADD_COLUMN_PRODUCT_DISCOUNT, ProductDiscount);
//        values.put(CART_ADD_COLUMN_PRODUCT_DISCOUNT_PROMOTION_ID, ProductDiscountPromotionID);
//        values.put(CART_ADD_COLUMN_VAT, Vat);
//        values.put(CART_ADD_COLUMN_TOTALTAKA, TotalTaka);
//        values.put(CART_ADD_COLUMN_DEFAULT_DISCOUNT, DefaultDiscount);
//        values.put(CART_ADD_COLUMN_DEFAULT_VAT, DefaultVat);
//
////        for(int i=0;i<category_add.length;i++){
////            values.put(EXPENSE_ADD_COLUMN_CATEGORY_ADD, category_add[i]);
////        }
//        Log.e("Inserted to db",values.toString());
//        db.insert(CART_TABLE_ADD, null, values);
//        db.close();
//    }
//
    public void insertAdd_To_Cart(String ProductCode, String ProductName, String SelectedBatch, String ProductPrice, String ProductStock, String ProductQty, String ProductDiscount, String ProductDiscountPromotionID, String Vat, String TotalTaka, String DefaultDiscount, String DefaultVat, String discount_type, String stock_id, String cat_id, String subcat_id, String brand_id, String unit_id, String store_from, String station_id, String original_product_price, String pro_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(CART_ADD_COLUMN_PRODUCT_CODE, ProductCode);
        values.put(CART_ADD_COLUMN_PRODUCT_NAME, ProductName);
        values.put(CART_ADD_COLUMN_SELECTED_BATCH, SelectedBatch);
        values.put(CART_ADD_COLUMN_PRODUCT_PRICE, ProductPrice);
        values.put(CART_ADD_COLUMN_PRODUCT_STOCK, ProductStock);
        values.put(CART_ADD_COLUMN_PRODUCT_QTY, ProductQty);

        values.put(CART_ADD_COLUMN_PRODUCT_DISCOUNT, ProductDiscount);
        values.put(CART_ADD_COLUMN_PRODUCT_DISCOUNT_PROMOTION_ID, ProductDiscountPromotionID);
        values.put(CART_ADD_COLUMN_VAT, Vat);//def_vat_amt'  (Ex: 300 er 5%  15 )
        values.put(CART_ADD_COLUMN_TOTALTAKA, TotalTaka);
        values.put(CART_ADD_COLUMN_DEFAULT_DISCOUNT, DefaultDiscount);
        values.put(CART_ADD_COLUMN_DEFAULT_VAT, DefaultVat);

        values.put(CART_discount_type, discount_type); //'discount_type  (Ex:  %  or  TK )
        values.put(CART_stock_id, stock_id); //'stock_id''
        values.put(CART_cat_id, cat_id); // 'cat_id'
        values.put(CART_subcat_id, subcat_id); //' subcat_id'
        values.put(CART_brand_id, brand_id); // 'brand_id'
        values.put(CART_unit_id, unit_id); // 'unit_id'
        values.put(CART_store_from, store_from); //'store_from'
        values.put(CART_station_id, station_id); //''station_id'
        values.put(CART_original_product_price, original_product_price); //''original_product_price'
        values.put(CART_pro_id, pro_id); //pro_id

//        for(int i=0;i<category_add.length;i++){
//            values.put(EXPENSE_ADD_COLUMN_CATEGORY_ADD, category_add[i]);
//        }
        Log.e("Inserted to db", values.toString());
        db.insert(CART_TABLE_ADD, null, values);
        db.close();
    }

    // todo: UPDATING Cart Information when same BATCH PRODUCT added -------------------------------------------------------------


    public void updateAddeedItem(String ProductCode, String ProductName, String SelectedBatch, String ProductPrice, String ProductStock, String ProductQty, String ProductDiscount, String ProductDiscountPromotionID, String Vat, String TotalTaka, String DefaultDiscount, String DefaultVat, String discount_type, String stock_id, String cat_id, String subcat_id, String brand_id, String unit_id, String store_from, String station_id, String original_product_price, String pro_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(CART_ADD_COLUMN_PRODUCT_CODE, ProductCode);
        values.put(CART_ADD_COLUMN_PRODUCT_NAME, ProductName);
        values.put(CART_ADD_COLUMN_SELECTED_BATCH, SelectedBatch);
        values.put(CART_ADD_COLUMN_PRODUCT_PRICE, ProductPrice);
        values.put(CART_ADD_COLUMN_PRODUCT_STOCK, ProductStock);
        values.put(CART_ADD_COLUMN_PRODUCT_QTY, ProductQty);

        values.put(CART_ADD_COLUMN_PRODUCT_DISCOUNT, ProductDiscount);
        values.put(CART_ADD_COLUMN_PRODUCT_DISCOUNT_PROMOTION_ID, ProductDiscountPromotionID);
        values.put(CART_ADD_COLUMN_VAT, Vat);//def_vat_amt'  (Ex: 300 er 5%  15 )
        values.put(CART_ADD_COLUMN_TOTALTAKA, TotalTaka);
        values.put(CART_ADD_COLUMN_DEFAULT_DISCOUNT, DefaultDiscount);
        values.put(CART_ADD_COLUMN_DEFAULT_VAT, DefaultVat);

        values.put(CART_discount_type, discount_type); //'discount_type  (Ex:  %  or  TK )
        values.put(CART_stock_id, stock_id); //'stock_id''
        values.put(CART_cat_id, cat_id); // 'cat_id'
        values.put(CART_subcat_id, subcat_id); //' subcat_id'
        values.put(CART_brand_id, brand_id); // 'brand_id'
        values.put(CART_unit_id, unit_id); // 'unit_id'
        values.put(CART_store_from, store_from); //'store_from'
        values.put(CART_station_id, station_id); //''station_id'
        values.put(CART_original_product_price, original_product_price); //''original_product_price'
        values.put(CART_pro_id, pro_id); //pro_id


        Log.e("Updated In db", values.toString());
        // db.update("Add_To_Cart", values,CART_ADD_COLUMN_SELECTED_BATCH+ "=" +SelectedBatch , null );
        db.update("Add_To_Cart", values, CART_ADD_COLUMN_SELECTED_BATCH + "= ?", new String[]{SelectedBatch});
        //  return true;
    }


    // todo: Updating Cart Information FROM ROW -------------------------------------------------------------

    public void updateFromAdapter(String SelectedBatch, String ProductPrice,  String ProductQty, String Vat, String ProductDiscount, String TotalTaka, String original_product_price) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CART_ADD_COLUMN_SELECTED_BATCH, SelectedBatch);
        values.put(CART_ADD_COLUMN_PRODUCT_PRICE, ProductPrice);
       // values.put(CART_ADD_COLUMN_PRODUCT_STOCK, ProductStock);
        values.put(CART_ADD_COLUMN_PRODUCT_QTY, ProductQty);

        values.put(CART_ADD_COLUMN_PRODUCT_DISCOUNT, ProductDiscount);
        values.put(CART_ADD_COLUMN_VAT, Vat);
        values.put(CART_ADD_COLUMN_TOTALTAKA, TotalTaka);
        values.put(CART_original_product_price, original_product_price);

        Log.e("Updated From ROW", values.toString());

        // db.update("Add_To_Cart", values,CART_ADD_COLUMN_SELECTED_BATCH+ "=" +SelectedBatch , null );
        db.update("Add_To_Cart", values, CART_ADD_COLUMN_SELECTED_BATCH + "= ?", new String[]{SelectedBatch});
        db.close();
        Log.e("updated:", "YES");
        //  return true;
    }


    // todo: Getiing Information for CART Adapter -------------------------------------------------------------

    public ArrayList<CartModal> getOverviewList() {
        CartModal overList = null;

        ArrayList<CartModal> listOverview = new ArrayList<>();
        hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT *  FROM Add_To_Cart ORDER by columnId asc ", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            overList = new CartModal(cursor.getInt(0)
                    , cursor.getString(cursor.getColumnIndexOrThrow("ProductName")),
                    cursor.getString(cursor.getColumnIndexOrThrow("ProductCode"))
                    , cursor.getString(cursor.getColumnIndexOrThrow("ProductStock"))
                    , cursor.getString(cursor.getColumnIndexOrThrow("TotalTaka"))
                    , cursor.getString(cursor.getColumnIndexOrThrow("SelectedBatch"))
                    , cursor.getString(cursor.getColumnIndexOrThrow("ProductQty"))
                    , cursor.getString(cursor.getColumnIndexOrThrow("ProductPrice"))
                    , cursor.getString(cursor.getColumnIndexOrThrow("ProductDiscount"))
                    , cursor.getString(cursor.getColumnIndexOrThrow("ProductDiscountPromotionID"))
                    , cursor.getString(cursor.getColumnIndexOrThrow("Vat"))
                    , cursor.getString(cursor.getColumnIndexOrThrow("DefaultDiscount"))
                    , cursor.getString(cursor.getColumnIndexOrThrow("DefaultVat"))

            );
            listOverview.add(overList);
            cursor.moveToNext();

        }
        cursor.close();
        db.close();
        return listOverview;
    }

    public ArrayList<SalesInfoModal> getOverviewList2() {
        SalesInfoModal overList = null;

        ArrayList<SalesInfoModal> listOverview = new ArrayList<>();
        hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT *  FROM Add_To_Cart ORDER by columnId desc ", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            overList = new SalesInfoModal(cursor.getString(cursor.getColumnIndexOrThrow("pro_id")),
                    cursor.getString(cursor.getColumnIndexOrThrow("SelectedBatch")),
                    cursor.getString(cursor.getColumnIndexOrThrow("ProductQty")),
                    cursor.getString(cursor.getColumnIndexOrThrow("stock_id")),
                    cursor.getString(cursor.getColumnIndexOrThrow("cat_id")),
                    cursor.getString(cursor.getColumnIndexOrThrow("subcat_id")),
                    cursor.getString(cursor.getColumnIndexOrThrow("brand_id")),
                    cursor.getString(cursor.getColumnIndexOrThrow("unit_id")),

                    cursor.getString(cursor.getColumnIndexOrThrow("ProductPrice")),
                    cursor.getString(cursor.getColumnIndexOrThrow("ProductDiscount")),
                    cursor.getString(cursor.getColumnIndexOrThrow("discount_type")),
                    cursor.getString(cursor.getColumnIndexOrThrow("TotalTaka")),
                    cursor.getString(cursor.getColumnIndexOrThrow("ProductDiscountPromotionID")),
                    cursor.getString(cursor.getColumnIndexOrThrow("store_from")),
                    cursor.getString(cursor.getColumnIndexOrThrow("station_id")),
                    cursor.getString(cursor.getColumnIndexOrThrow("DefaultVat")),
                    cursor.getString(cursor.getColumnIndexOrThrow("Vat")),
                    cursor.getString(cursor.getColumnIndexOrThrow("original_product_price")),
                    cursor.getString(cursor.getColumnIndexOrThrow(CART_ADD_COLUMN_PRODUCT_NAME)),
                    cursor.getString(cursor.getColumnIndexOrThrow(CART_ADD_COLUMN_PRODUCT_CODE)),
                    cursor.getString(cursor.getColumnIndexOrThrow(CART_ADD_COLUMN_PRODUCT_STOCK))
                    );

            listOverview.add(overList);
            cursor.moveToNext();

        }
        cursor.close();
        db.close();
        return listOverview;
    }

    public ArrayList<SalesInfoModal> getOverviewList3() {
        SalesInfoModal overList = null;

        ArrayList<SalesInfoModal> listOverview = new ArrayList<>();
        hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT *  FROM CustomerTable", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            overList = new SalesInfoModal(cursor.getString(cursor.getColumnIndexOrThrow("grand_total")),
                    cursor.getString(cursor.getColumnIndexOrThrow("cart_total")),
                    cursor.getString(cursor.getColumnIndexOrThrow("special_dis")),
                    cursor.getString(cursor.getColumnIndexOrThrow("cus_dis")),
                    cursor.getString(cursor.getColumnIndexOrThrow("pur_dis")),
                    cursor.getString(cursor.getColumnIndexOrThrow("card_dis")),
                    cursor.getString(cursor.getColumnIndexOrThrow("customers_id")),
                    cursor.getString(cursor.getColumnIndexOrThrow("customer_code")),
                    cursor.getString(cursor.getColumnIndexOrThrow("ck_sp_dis")),
                    cursor.getString(cursor.getColumnIndexOrThrow("ck_cus_dis")),
                    cursor.getString(cursor.getColumnIndexOrThrow("ck_pur_dis")),
                    cursor.getString(cursor.getColumnIndexOrThrow("ck_cart_dis"))
            );

            listOverview.add(overList);
            cursor.moveToNext();

        }
        cursor.close();
        db.close();
        return listOverview;
    }


    // todo: DELETE Information by clicking ROW -------------------------------------------------------------

    public Integer deleteAddToCartItem(int idss) {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.i("Checking in ", "DB");
        return db.delete(CART_TABLE_ADD, CART_ADD_COLUMN_ID + "=" + idss, null);


    }

    public void deleteRow(String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + CART_TABLE_ADD + " WHERE " + CART_ADD_COLUMN_ID + "='" + value + "'");
        db.close();
    }

    public void deleteAddedProduct(String batch) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(CART_TABLE_ADD, "columnId = ?", new String[]{batch});
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }


    // todo: CHECK if batch found to increase Product quantity-------------------------------------------------------------

    public int ifFoundBatch(String selectedBatch) {

        SQLiteDatabase db = getWritableDatabase();
        String selectString = "SELECT * FROM " + CART_TABLE_ADD + " WHERE " + CART_ADD_COLUMN_SELECTED_BATCH + " =?";

        Cursor cursor = db.rawQuery(selectString, new String[]{selectedBatch});
        int count;
        count = cursor.getCount();

        Log.e("Searching String: ", selectedBatch);
        Log.d(TAG, String.format("%d records got", count));


        cursor.close();          // Dont forget to close your cursor
        db.close();              //AND your Database!
        return count;
    }

    // todo: TOTAL from all rows-------------------------------------------------------------

    public String getTotalAmountInCart() {
        String totalAmmount = "0";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(TotalTaka) AS TotalTaka  FROM Add_To_Cart ", null);
        //Cursor cursor = db.rawQuery("UPDATE Add_Expense SET amount= '0'", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            totalAmmount = cursor.getString(cursor.getColumnIndexOrThrow("TotalTaka"));
            cursor.moveToNext();

        }
        cursor.close();
        db.close();
        return totalAmmount;
    }


    public String getGrandTotal(int index) {
        SQLiteDatabase sqldb = this.getReadableDatabase();

        String grandT = "";
        Cursor c = sqldb.rawQuery("SELECT grand_total from " + CUSTOMER_TABLE_NAME + " where " + "Column_id" + " = " + index, null);


        if (c.moveToFirst()) {
            do {
                grandT = c.getString(c.getColumnIndex("grand_total"));
            } while (c.moveToNext());
        }


        return grandT;
    }


    // todo: FINDING Quantity to decide qtyCounter-------------------------------------------------------------


    public int getProductQty(String selectedBatch) {

        SQLiteDatabase db = getWritableDatabase();
        String selectString = "SELECT * FROM " + CART_TABLE_ADD + " WHERE " + CART_ADD_COLUMN_SELECTED_BATCH + " =?";

        Cursor c = db.rawQuery(selectString, new String[]{selectedBatch});
        int count = 0;

        if (c.moveToFirst()) {
            do {
                count = Integer.valueOf(c.getString(c.getColumnIndex(CART_ADD_COLUMN_PRODUCT_QTY)));
            } while (c.moveToNext());
        }


        Log.e("Searching String:--- ", selectedBatch);
        Log.d(TAG, String.format("%d RECORDS got", count));


        c.close();          // Dont forget to close your cursor
        db.close();              //AND your Database!
        return count;
    }


//    public boolean CheckBatch(String bAtch) {
//        SQLiteDatabase db = getWritableDatabase();
//        String selectString = "SELECT * FROM " + BATCH_TABLE_NAME + " WHERE " + BATCH_COLUMN_BATCH_NO + " =?";
//
//        // Add the String you are searching by here.
//        // Put it in an array to avoid an unrecognized token error
//        Cursor cursor = db.rawQuery(selectString, new String[] {bAtch});
//        int count = 0;
//        boolean hasObject = false;
//        if(cursor.moveToFirst()){
//            hasObject = true;
//
//            //region if you had multiple records to check for, use this region.
//
//
//            while(cursor.moveToNext()){
//                count++;
//            }
//
//            if(count<=0){
//                hasObject = true;
//            }else hasObject = false;
//            //here, count is records found
//
//
//            //endregion
//
//        }
////        Log.d(TAG, String.format("Searching for String %d", bAtch));
//        Log.e("Searching Batch: ",bAtch);
//        Log.d(TAG, String.format("%d batch records found", count));
//
//
//        cursor.close();          // Dont forget to close your cursor
//        db.close();              //AND your Database!
//        return hasObject;
//    }

    public boolean CheckIsDataAlreadyInDBorNot(String pCode) {
        SQLiteDatabase db = getWritableDatabase();
        String selectString = "SELECT * FROM " + CART_TABLE_ADD + " WHERE " + CART_ADD_COLUMN_PRODUCT_CODE + " =?";

        // Add the String you are searching by here.
        // Put it in an array to avoid an unrecognized token error
        Cursor cursor = db.rawQuery(selectString, new String[]{pCode});
        int count = 0;
        boolean hasObject = false;
        if (cursor.moveToFirst()) {
            hasObject = true;

            //region if you had multiple records to check for, use this region.


            while (cursor.moveToNext()) {
                count++;
            }

            if (count <= 0) {
                hasObject = false;
            } else hasObject = true;
            //here, count is records found


            //endregion

        }
//        Log.d(TAG, String.format("Searching for String %d", pCode));
        Log.e("Searching String: ", pCode);
        Log.d(TAG, String.format("%d records found", count));


        cursor.close();          // Dont forget to close your cursor
        db.close();              //AND your Database!
        return hasObject;
    }

    public String getCID(String cid) {
        String CID = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT " +
                CUSTOMER_TABLE_COLUMN_CUSTOMERS_ID +
                " FROM " + CUSTOMER_TABLE_NAME
                + " WHERE " +
                CUSTOMER_TABLE_COLUMN_ID + "=?";


        Cursor cursor = db.rawQuery(selectQuery, new String[]{cid});


        if (cursor.moveToFirst()) {
            do {

                CID = String.valueOf(cursor.getString(cursor.getColumnIndex("customers_id")));


            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return CID;
    }

    public int CheckSelectedSpinner(String selectedBatch) {
        int rowId = -1;
        SQLiteDatabase db = getWritableDatabase();
        String selectString = "SELECT * FROM " + CART_TABLE_ADD + " WHERE " + CART_ADD_COLUMN_SELECTED_BATCH + " =?";
        // String selectStringROW = "SELECT SelectedBatch FROM " + CART_TABLE_ADD + " WHERE " + CART_ADD_COLUMN_SELECTED_BATCH + " =?";
        Cursor cursor = db.rawQuery(selectString, new String[]{selectedBatch});

        if (cursor.moveToFirst()) {
            do {

                rowId = cursor.getInt(0);


            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
//        Log.e("RowId:", rowId);

        // Add the String you are searching by here.
        // Put it in an array to avoid an unrecognized token error
//        Cursor cursor = db.rawQuery(selectString, new String[] {selectedBatch});
//        int count;
//        count = cursor.getCount();
////        boolean hasObject = false;
////        if(cursor.moveToFirst()){
////            hasObject = true;
////
////            //region if you had multiple records to check for, use this region.
////
////
////            while(cursor.moveToNext()){
////                count++;
////
////            }
////
////            if(count<=0){
////                hasObject = false;
////            }else hasObject = true;
////            //here, count is records found
////
////
////            //endregion
////
////        }
////        Log.d(TAG, String.format("Searching for String %d", selectedBatch));
//        Log.e("Searching String: ",selectedBatch);
//        Log.d(TAG, String.format("%d records got", count));
//
//
//        cursor.close();          // Dont forget to close your cursor
//        db.close();              //AND your Database!
        return rowId;
    }

    public String getCustomerID(int index) {
        SQLiteDatabase sqldb = this.getReadableDatabase();

        String CustomerID = "";
        Cursor c = sqldb.rawQuery("SELECT customers_id from " + CUSTOMER_TABLE_NAME + " where " + "Column_id" + " = " + index, null);


        if (c.moveToFirst()) {
            do {
                CustomerID = c.getString(c.getColumnIndex("customers_id"));
            } while (c.moveToNext());
        }
//
//        cursor.moveToFirst();
//        while (!cursor.isAfterLast()) {
//            productCode = cursor.getString(cursor.getColumnIndex("ProductCode"));
//            ProductCodes = productCode;
//            cursor.moveToNext();
//        }
//
//        cursor.close();
//        sqldb.close();

//        if(!cursor.moveToFirst()){
//
////            do {
////                //productCode = cursor.getString(1);
////                productCode = cursor.getString(cursor.getColumnIndex("ProductCode"));
////                ProductCodes = productCode;
////                //do something which you want and break
////                break;
////            } while (cursor.moveToLast());
//        }
//        else{
//
//            do {
//                //productCode = cursor.getString(1);
//                productCode = cursor.getString(cursor.getColumnIndex("ProductCode"));
//                ProductCodes = productCode;
//
//                    //do something which you want and break
//                    break;
//            } while (cursor.moveToLast());
//
//        }


        return CustomerID;

//        Cursor cursor = null;
//        String productCode = "";
//        try {
//            cursor = sqldb.rawQuery("SELECT * from " + CART_TABLE_ADD + " where " + CART_ADD_COLUMN_ID+" =?", new String[]{index});
//         //   db.rawQuery("SELECT ProductCode from " + CART_TABLE_ADD + " where " + CART_ADD_COLUMN_ID+" =?", new String[]{index});
//            if(cursor.getCount() > 0) {
//                cursor.moveToFirst();
//                productCode = cursor.getString(cursor.getColumnIndex("ProductCode"));
//            }
//            return productCode;
//        }finally {
//            cursor.close();
//        }
    }


    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from Category where id=" + id + "", null);
        return res;
    }

    public Cursor getDataAdd(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor_add = db.rawQuery("select * from Category where id_add=" + id + "", null);
        return cursor_add;
    }

    public Cursor getsumExpenseAmount(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor_EA = db.rawQuery("select sum(amount) from Add_Expense where category_add = '$category_add' ", null);
        return cursor_EA;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, CART_TABLE_ADD);
        return numRows;
    }

    public int numberOfRowsAdd() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRowsAdd = (int) DatabaseUtils.queryNumEntries(db, CART_TABLE_ADD);
        return numRowsAdd;
    }

//    public boolean updateCategory(Integer id, String name) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues contentValues = new ContentValues();
//        contentValues.put("product_id", id);
//        contentValues.put("batch_no", name);
//
//        db.update("BatchTable", contentValues, BATCH_COLUMN_PRODUCT_ID+ "=" +id, null);
//        return true;
//    }

//    public boolean updateAddExpense(Integer id, String name) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues contentValues = new ContentValues();
//        contentValues.put("product_id", id);
//        contentValues.put("category_add", name);
//
//        db.update("Add_To_Cart", contentValues, BATCH_COLUMN_PRODUCT_ID + "=" +id , new String[]{name});
//        return true;
//    }

    public boolean sss(Integer id, String qty) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("product_id", id);
        contentValues.put("category_add", qty);

        db.update("Add_To_Cart", contentValues, CART_ADD_COLUMN_ID + "=" + id, new String[]{qty});
        return true;
    }


    public boolean updateCategoryAdd(Integer id, String category_add, String amount, String date, String note) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("columnId", id);
        contentValues.put("category_add", category_add);
        contentValues.put("amount", amount);
        contentValues.put("date", date);
        contentValues.put("note", note);

        db.update("Add_To_Cart", contentValues, CART_ADD_COLUMN_ID + "=" + id, null);
        return true;
    }

    public boolean deleteCategoryAdd(Integer id, String category_add, String amount, String date, String note) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        //contentValues.remove("columnId", product_id);
        contentValues.put("category_add", category_add);
        contentValues.put("amount", amount);
        contentValues.put("date", date);
        contentValues.put("note", note);

        //db.delete(EXPENSE_TABLE_ADD, contentValues,EXPENSE_ADD_COLUMN_ID+ "=" +product_id , null );
        return true;
    }


    public boolean deleteAddCategory(String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(CART_TABLE_ADD, CART_ADD_COLUMN_SELECTED_BATCH + "='" + value + "' ;", null) > 0;
    }

    public boolean deleteAddItem(String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(CART_TABLE_ADD, CART_ADD_COLUMN_ID + "='" + value + "' ;", null) > 0;
    }


        public void delete(){
            SQLiteDatabase db = this.getReadableDatabase();
            db.delete(CUSTOMER_TABLE_NAME,null,null);
            db.delete(CART_TABLE_ADD,null,null);
        }


    public ArrayList<AddTOCartModal> getProductCodes() {
        AddTOCartModal overList = null;

        ArrayList<AddTOCartModal> listOverview = new ArrayList<>();
        hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM ProductCode", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            overList = new AddTOCartModal(cursor.getInt(0), cursor.getString(2));
            listOverview.add(overList);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return listOverview;
    }

}