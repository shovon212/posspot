package syntechbd.com.posspot;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import syntechbd.com.posspot.adapter.DrawerAdapter;
import syntechbd.com.posspot.app.AppController;
import syntechbd.com.posspot.modal.CustomerWiseSellModal;
import syntechbd.com.posspot.modal.DrawerItem;

import static syntechbd.com.posspot.app.AppController.TAG;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    SharedPreferences pref; // 0 - for private mode
    SharedPreferences.Editor editor;


    private static final String TAG = MainActivity.class.getSimpleName();
    private DrawerLayout mDrawerLayout;
    private ArrayList<DrawerItem> mDrawerItemList;
    private RecyclerView mRecyclerView;

    public int ItemPosition = 1;

    String YourUserName,YourPassword;
    private String baseUrl;
    private String jsonResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (!isNetworkConnected()){
            Toast.makeText(this,"Connection Lost !! Check your internet connection! ",Toast.LENGTH_LONG).show();
        }

        pref = getApplicationContext().getSharedPreferences("UserInfo", 0); // 0 - for private mode
        YourUserName= pref.getString("user_name",null);
        YourPassword= pref.getString("user_password",null);

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //TODO your background code

                String MotherApi = "http://posspot.com/api/authentication/check_user_auth?userName=";
                String addingUsername = MotherApi.concat(YourUserName + "&passWorD=");
                baseUrl = addingUsername.concat(YourPassword + "&userType=1");
                makeJsonObjectRequest();

            }
        });

        DashBoardFragment dashBoardFragment = new DashBoardFragment();
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mainContainer, dashBoardFragment).commit();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        addDummyData();

        //Get The recuyclerView
        mRecyclerView = (RecyclerView) findViewById(R.id.drawerRecyclerView);
        final DrawerAdapter adapter = new DrawerAdapter(mDrawerItemList);
        final GridLayoutManager manager = new GridLayoutManager(this, 6);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
               // return (position == 0) || position==5 || position==8 ? 2 : 1;
                return  position==0 ? 6 : 2;

        }
        });

        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(adapter);

        adapter.setOnItemClickLister(new DrawerAdapter.OnItemSelecteListener() {
            @Override
            public void onItemSelected(View v, int position) {

                if (position == 1) {
                   // Log.e(TAG, "DashBoardFragment");
                    DashBoardFragment dashBoardFragment = new DashBoardFragment();
                    android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
                } else if (position == 2) {
                    //Log.e(TAG, "StockSummary");

                    StockSummary dashBoardFragment = new StockSummary();
                    android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
                } else if (position == 3) {
                   // Log.e(TAG, "CustomerView");

                    CustomerView dashBoardFragment = new CustomerView();
                    android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
                }else if (position == 4) {
                    FragmentSupplierView dashBoardFragment = new FragmentSupplierView();
                    android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
                }else if (position == 11) {
                    SupplierTransactionReportFragment dashBoardFragment = new SupplierTransactionReportFragment();
                    android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
                }else if (position == 12) {
                    finish();
                    try {
                        editor = pref.edit();
                        editor.putBoolean("keep_logged_in", false); // Storing boolean - true/false
                        editor.apply();
                        startActivity(new Intent(getApplicationContext(), LoginFragmenta.class));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (position == 13) {

                    startActivity(new Intent(getApplicationContext(),SaleNowActivity.class));
                    //finish();

                }else if (position == 10) {
                   // Log.e(TAG, "SupplierPayableReport_Fragment");

                    SupplierPayableReport_Fragment dashBoardFragment = new SupplierPayableReport_Fragment();
                    android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
                }else if (position == 5) {
                   // Log.e(TAG, "SalesReportFragment");

                    SalesReportFragment dashBoardFragment = new SalesReportFragment();
                    android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
                } else if (position == 6) {
                  //  Log.e(TAG, "PurchaseReportFragment");

                    PurchaseReportFragment dashBoardFragment = new PurchaseReportFragment();
                    android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
                }else if (position == 7) {
                   // Log.e(TAG, "StockInReportFragment");

                    StockInReportFragment dashBoardFragment = new StockInReportFragment();
                    android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
                }else if (position == 8) {
                   // Log.e(TAG, "StockOutReportFragment");

                    StockOutReportFragment dashBoardFragment = new StockOutReportFragment();
                    android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
                }else if (position == 9) {
                   // Log.e(TAG, "Customerwise Sell Report");

                    CustomerWiseSellReportFragment dashBoardFragment = new CustomerWiseSellReportFragment();
                    android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
                }

//                else {
//                    int pos = position - 1;
//                    if (ItemPosition == pos) {
//                        Log.e(TAG, "Atis alet atieno");
//                        DashBoardFragment dashBoardFragment = new DashBoardFragment();
//                        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//                        transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
//                    } else {
//                        resetPreviuosClicked(ItemPosition);
//                        showItemClicked(pos);
//                        adapter.notifyDataSetChanged();
//
//                    }
//                }
                //TODO change if the position has changed
                //Toast.makeText(MainActivity.this, "You clicked at position: " + position, Toast.LENGTH_SHORT).show();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });


 //       SharedPreferences pref = this.getSharedPreferences("UserInfo", 0); // 0 - for private mode

//        View hView = navigationView.getHeaderView(0);
//        TextView nav_users_fullName = (TextView) hView.findViewById(R.id.usersFullNameTV);
//        TextView nav_users_Email = (TextView) hView.findViewById(R.id.usersEmailTV);
//        nav_users_fullName.setText(pref.getString("user_full_name", null));
//        nav_users_Email.setText(pref.getString("user_email", null));

    }


    private void resetPreviuosClicked(int ItemPositionPrevious) {
        mDrawerItemList.get(ItemPositionPrevious).setLayoutBgColor(Color.WHITE);


    }

    private void showItemClicked(int i) {

        mDrawerItemList.get(i).setTitle("Home");
//        mDrawerItemList.get(i).setIcon(R.drawable.ic_menu_share);
//        mDrawerItemList.get(i).setLayoutBgColor(R.color.colorClickedItem);
        ItemPosition = i;

    }

    private void addDummyData() {
        //Dummy Data
        mDrawerItemList = new ArrayList<DrawerItem>();
        DrawerItem item = new DrawerItem();
        item.setIcon(R.drawable.ic_icon_home);
        item.setTitle("Dashboard");
        mDrawerItemList.add(item);

        DrawerItem item2 = new DrawerItem();
        item2.setIcon(R.drawable.ic_icon_stock_summery);
        item2.setTitle("Stock Summary");
        mDrawerItemList.add(item2);

        DrawerItem item3 = new DrawerItem();
        item3.setIcon(R.drawable.ic_icon_customers);
        item3.setTitle("Customers");
        mDrawerItemList.add(item3);

        DrawerItem item4 = new DrawerItem();
        item4.setIcon(R.drawable.ic_icon_supplier_report);
        item4.setTitle("Suppliers Report");
        mDrawerItemList.add(item4);

        DrawerItem item5 = new DrawerItem();
        item5.setIcon(R.drawable.ic_icon_sell_invoice_report);
        item5.setTitle("Sell Invoice Report");
        mDrawerItemList.add(item5);

        DrawerItem item6 = new DrawerItem();
        item6.setIcon(R.drawable.ic_icon_purchase_report);
        item6.setTitle("Purchase Report");
        mDrawerItemList.add(item6);

        DrawerItem item7 = new DrawerItem();
        item7.setIcon(R.drawable.ic_icon_stock_in_report);
        item7.setTitle("Stock In Report");
        mDrawerItemList.add(item7);

        DrawerItem item8 = new DrawerItem();
        item8.setIcon(R.drawable.ic_icon_stock_out_report);
        item8.setTitle("Stock out Report");
        mDrawerItemList.add(item8);

        DrawerItem item9 = new DrawerItem();
        item9.setIcon(R.drawable.ic_icon_cutomer_wise_sell_report);
        item9.setTitle("Customer wise Sell Report");
        mDrawerItemList.add(item9);

        DrawerItem item10  = new DrawerItem();
        item10.setIcon(R.drawable.ic_icon_payable_supllier);
        item10.setTitle("Supplier Payable Report");
        mDrawerItemList.add(item10);

        DrawerItem item11  = new DrawerItem();
        item11.setIcon(R.drawable.ic_icon_supplier_transaction);
        item11.setTitle("Supplier Transaction Report");
        mDrawerItemList.add(item11);

        DrawerItem item13  = new DrawerItem();
        item13.setIcon(R.drawable.ic_icon_logout);
        item13.setTitle("Log Out");
        mDrawerItemList.add(item13);

        DrawerItem item14 = new DrawerItem();
        item14.setIcon(R.drawable.ic_icon_purchase_report);
        item14.setTitle("SALES");
        mDrawerItemList.add(item14);

//        DrawerItem item14  = new DrawerItem();
//        item14.setIcon(R.drawable.ic_menu_manage);
//        item14.setTitle("three");
//        mDrawerItemList.add(item14);
//
//
//        DrawerItem item15  = new DrawerItem();
//        item15.setIcon(R.drawable.ic_menu_slideshow);
//        item15.setTitle("Inbox");
//        mDrawerItemList.add(item15);
//
//        DrawerItem item16  = new DrawerItem();
//        item16.setIcon(R.drawable.ic_menu_camera);
//        item16.setTitle("Send");
//        mDrawerItemList.add(item16);
//
//        DrawerItem item17  = new DrawerItem();
//        item17.setIcon(R.drawable.ic_menu_manage);
//        item17.setTitle("three");
//        mDrawerItemList.add(item17);
//
//        DrawerItem item18  = new DrawerItem();
//        item18.setIcon(R.drawable.ic_menu_manage);
//        item18.setTitle("three");
//        mDrawerItemList.add(item18);
//
//        DrawerItem item19  = new DrawerItem();
//        item19.setIcon(R.drawable.ic_menu_manage);
//        item19.setTitle("three");
//        mDrawerItemList.add(item19);





    }
// new

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
//            if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
//                ((ActivityManager)this.getSystemService(ACTIVITY_SERVICE))
//                        .clearApplicationUserData(); // note: it has a return value!
//            } else {
//                // use old hacky way, which can be removed
//                // once minSdkVersion goes above 19 in a few years.


//            }

            finish();
            try {
                editor = pref.edit();
                editor.putBoolean("keep_logged_in", false); // Storing boolean - true/false
                editor.apply();
                startActivity(new Intent(this, LoginFragmenta.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action

            DashBoardFragment dashBoardFragment = new DashBoardFragment();
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
            //Toast.makeText(this,"Done",Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_slideshow) {

            StockSummary dashBoardFragment = new StockSummary();
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
            //Toast.makeText(this,"Sales Done",Toast.LENGTH_SHORT).show();

        } else if (id == R.id.cutomers_view) {

            CustomerView dashBoardFragment = new CustomerView();
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
            //Toast.makeText(this,"Sales Done",Toast.LENGTH_SHORT).show();

        }else if (id == R.id.SellInvoiceReport) {

            SalesReportFragment dashBoardFragment = new SalesReportFragment();
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
            //Toast.makeText(this,"Sales Done",Toast.LENGTH_SHORT).show();

        }else if (id == R.id.SupplierPayableReport) {

            SupplierPayableReport_Fragment dashBoardFragment = new SupplierPayableReport_Fragment();
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.mainContainer, dashBoardFragment).commit();
            //Toast.makeText(this,"Sales Done",Toast.LENGTH_SHORT).show();

        }
// else if (id == R.id.nav_senwqed) {
//
//            SupplierPayableReport_Fragment supplierPayableReport_fragment = new SupplierPayableReport_Fragment();
//            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//            transaction.replace(R.id.mainContainer, supplierPayableReport_fragment).commit();
//
//        }
        else if (id == R.id.nav_share) {


            finish();

            try {
                editor = pref.edit();
                editor.putBoolean("keep_logged_in", false); // Storing boolean - true/false
                editor.apply();
                startActivity(new Intent(this, LoginFragmenta.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
            startActivity(new Intent(this, LoginFragmenta.class));

//            if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
//                ((ActivityManager)this.getSystemService(ACTIVITY_SERVICE))
//                        .clearApplicationUserData(); // note: it has a return value!
//            } else {
//                // use old hacky way, which can be removed
//                // once minSdkVersion goes above 19 in a few years.
//            }

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void makeJsonObjectRequest() {

        //showpDialog();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                baseUrl, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject info = response.getJSONObject("response");
                    String user_name = info.getString("user_name");
                    String user_id = info.getString("user_id");
                    String user_full_name = info.getString("user_full_name");
                    String user_email = info.getString("user_email");
                    String user_phone = info.getString("user_phone");
                    String store_id = info.getString("store_id");
                    String store_name = info.getString("store_name");
                    String station_id = info.getString("station_id");
                    String station_name = info.getString("station_name");
                    String logged_in = info.getString("logged_in");
                    String station_acc_id = info.getString("station_acc_id");

                    JSONObject InnerInfo = info.getJSONObject("info");


                    String total_stock = InnerInfo.getString("total_stock");
                    int total_station = Integer.parseInt(InnerInfo.getString("total_station"));
                    int used_station = Integer.parseInt(InnerInfo.getString("used_station"));
                    int total_store = Integer.parseInt(InnerInfo.getString("total_store"));
                    int used_store = Integer.parseInt(InnerInfo.getString("used_store"));
                    int total_user = Integer.parseInt(InnerInfo.getString("total_user"));
                    int used_user = Integer.parseInt(InnerInfo.getString("used_user"));



                    jsonResponse = "";
                    jsonResponse += "user_full_name: " + user_full_name + "\n\n";
                    jsonResponse += "total_stock: " + total_stock + "\n\n";
                    jsonResponse += "total_station: " + total_station + "\n\n";

                    // statusTV.setText(jsonResponse);



                    editor = pref.edit();

                    editor.putBoolean("keep_logged_in", true); // Storing boolean - true/false
                    editor.putString("user_full_name", user_full_name); // Storing string
                    editor.putString("user_email", user_email); // Storing string
                    editor.putString("user_name", YourUserName); // Storing string
                    editor.putString("user_password", YourPassword); // Storing string
                    editor.putString("store_id", store_id);
                    editor.putString("station_id", station_id);
                    editor.putString("station_acc_id", station_acc_id);



                    editor.putString("total_stock", total_stock); // Storing string
                    editor.putInt("total_station", total_station); // Storing integer
                    editor.putInt("used_station", used_station); // Storing integer


                    editor.putString("store_name", store_name); // Storing string
                    editor.putString("station_name", station_name); // Storing string


                    editor.putInt("total_store", total_store); // Storing integer
                    editor.putInt("used_store", used_store); // Storing integer


                    editor.putInt("total_user", total_user); // Storing integer
                    editor.putInt("used_user", used_user); // Storing integer


                    editor.apply();




                } catch (JSONException e) {
                    e.printStackTrace();
                    editor = pref.edit();

                    editor.putBoolean("keep_logged_in", false);

                    editor.apply();
                    finish();
                    startActivity(new Intent(getApplicationContext(), SplashActivity.class));
                    Toast.makeText(getApplicationContext(),
                            "Wrong Username or password! Log in again!",
                            Toast.LENGTH_LONG).show();



                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                editor = pref.edit();

                editor.putBoolean("keep_logged_in", false);

                editor.apply();
                finish();
                startActivity(new Intent(getApplicationContext(), SplashActivity.class));

            }
        }) {
//            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                String userName = "pos01";
//                String password = "12345";
//                params.put("userName", userName);
//                params.put("passWorD", password);
//                return params;
//
//            }


            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();


//                String userName = "pos01";
//                String password = "12345";


//                headers.put("userName", userName);
//                headers.put("passWorD", password);
//                headers.put("Content-Type", "application/json");
//                 headers.put("Accept", "application/json");
//                headers.put("Host", "sms.posspot.com");
//               headers.put("Accept-Encoding", "utf-8");
                String creds = String.format("%s:%s", "posspot", "synt3ch");
                System.out.println(creds);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);
                return headers;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue

        AppController.getInstance().getRequestQueue().getCache().remove(baseUrl);
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


}
