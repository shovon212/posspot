package syntechbd.com.posspot;


import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import syntechbd.com.posspot.adapter.StockSummaryAdapter;
import syntechbd.com.posspot.app.AppController;
import syntechbd.com.posspot.modal.StockProduct;

import static syntechbd.com.posspot.app.AppController.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class StockSummary extends Fragment {

    ListView listView;
    StockSummaryAdapter adapter;
    ArrayList<StockProduct> productList = new ArrayList<StockProduct>();
    EditText prdctNameInProductSummaryTV;


    SharedPreferences pref;
    String YouruserName;


    public StockSummary() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (!isNetworkConnected()){
            Toast.makeText(getContext(),"Connection Lost !! Check your internet connection! ",Toast.LENGTH_LONG).show();
        }
        View v =  inflater.inflate(R.layout.fragment_stock_summary, container, false);

        getActivity().setTitle("Stock Summary");

        pref = getActivity().getSharedPreferences("UserInfo", 0); // 0 - for private mode


        YouruserName = pref.getString("user_name", null);

        makeJsonObjecttRequestForDailySales();

        listView = (ListView) v.findViewById(R.id.StockSummeryLV);
        prdctNameInProductSummaryTV= (EditText) v.findViewById(R.id.prdctNameInProductSummaryTV);
        adapter = new StockSummaryAdapter(getActivity(), productList);
        listView.setTextFilterEnabled(true);
        listView.setAdapter(adapter);
        listView.setVisibility(View.INVISIBLE);

        prdctNameInProductSummaryTV.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub
                StockSummary.this.adapter.getFilter().filter(arg0);
                listView.setVisibility(View.VISIBLE);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }
        });


        return v;
    }






    public void makeJsonObjecttRequestForDailySales() {

        String urlJsonObj = "http://"+YouruserName+".posspot.com/api/DP_api/stock_summary" ;
        // done

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlJsonObj, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONArray info = response.getJSONArray("response");

                    for (int i = 0; i < info.length(); i++) {

                        JSONObject jsonObject = info.getJSONObject(i);

                        StockProduct sched = new StockProduct();

                        String name = jsonObject.getString("product_name");
                        sched.setProductName(jsonObject.getString("product_name"));
                        sched.setProductCode(jsonObject.getString("product_code"));
                        sched.setProductQty(jsonObject.getString("stock_qty"));

                        productList.add(sched);

                        // retrieve the values like this so on..

                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                // hide the progress dialog
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);


    }


    @Override
    public void onResume() {

        super.onResume();



        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            private Boolean exit = false;
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button


                    if (exit) {
                        getActivity().finish(); // finish activity
                    } else {
                        Toast.makeText(getActivity(),"Press Back again to Exit", Toast.LENGTH_SHORT).show();
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 3 * 1000);

                    }





                    return true;

                }

                return false;
            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

}
