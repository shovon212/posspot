package syntechbd.com.posspot;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import syntechbd.com.posspot.app.AppController;
import syntechbd.com.posspot.modal.DashboardModals;

import static syntechbd.com.posspot.app.AppController.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashBoardFragment extends Fragment {

    TextView store_name, station_name, stock_value, total_station, used_station, available_station, total_store, used_store, available_store, total_user, used_user, available_user;
    TextView dailySaleStoreName,dailySaleAmount,CurrentDay;
    DashboardModals dashboardModals;

    String baseUrl;
    String YouruserName  ;
    String YourpassWorD ;




    String DailySaleStoreName;
    String DailySaleAmount;

    String jsonResponse;
    SharedPreferences pref; // 0 - for private mode
    SharedPreferences.Editor editor;
    String total_stock;
    public DashBoardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (!isNetworkConnected()){
            Toast.makeText(getContext(),"Connection Lost !! Check your internet connection! ",Toast.LENGTH_LONG).show();
        }


        final View v = inflater.inflate(R.layout.fragment_dash_board, container, false);
        // Inflate the layout for this fragment


        pref = getActivity().getSharedPreferences("UserInfo", 0); // 0 - for private mode

        YouruserName = pref.getString("user_name", null);
        YourpassWorD = pref.getString("user_password", null);
        String MotherApi = "http://posspot.com/api/authentication/check_user_auth?userName=";
        String addingUsername= MotherApi.concat(YouruserName + "&passWorD=");
        baseUrl= addingUsername.concat(YourpassWorD + "&userType=1");

        getActivity().setTitle("Posspot | Dashboard");

        dashboardModals = new DashboardModals();

        stock_value = (TextView) v.findViewById(R.id.stock_value_id);
        total_station = (TextView) v.findViewById(R.id.total_station_id);
        used_station = (TextView) v.findViewById(R.id.used_station_id);
        available_station = (TextView) v.findViewById(R.id.available_station);
        total_store = (TextView) v.findViewById(R.id.total_store_id);
        used_store = (TextView) v.findViewById(R.id.used_store_id);
        available_store = (TextView) v.findViewById(R.id.available_store_id);
        total_user = (TextView) v.findViewById(R.id.total_user_id);
        used_user = (TextView) v.findViewById(R.id.used_user_id);
        available_user = (TextView) v.findViewById(R.id.available_user_id);
        store_name = (TextView) v.findViewById(R.id.store_nameTV);
        station_name = (TextView) v.findViewById(R.id.station_nameTV);
        dailySaleStoreName = (TextView) v.findViewById(R.id.dailySaleStoreNameTV);
        dailySaleAmount = (TextView) v.findViewById(R.id.dailySaleAmountTV);
        CurrentDay = (TextView) v.findViewById(R.id.todaysDateTV);



        try {
//            Toast.makeText(getContext(),String.valueOf(dashboardModals.getTotal_stock()),Toast.LENGTH_LONG).show();
//            //stock_value.setText(String.valueOf(dashboardModals.getTotal_stock()));
//            total_station .setText(String.valueOf(dashboardModals.getTotal_station()));
//            used_station .setText(String.valueOf(dashboardModals.getUsed_station()));
//            available_station .setText(String.valueOf(dashboardModals.getTotal_station()-dashboardModals.getUsed_station()));
//            total_store .setText(String.valueOf(dashboardModals.getTotal_store()));
//            used_store.setText(String.valueOf(dashboardModals.getUsed_store()));
//            available_store.setText(String.valueOf(dashboardModals.getTotal_store()-dashboardModals.getUsed_store()));
//            total_user .setText(String.valueOf(dashboardModals.getTotal_user()));
//            used_user .setText(String.valueOf(dashboardModals.getUsed_user()));
//            available_user .setText(String.valueOf(dashboardModals.getTotal_user()-dashboardModals.getUsed_user()));


            //SharedPreferences pref = getActivity().getSharedPreferences("UserInfo", 0); // 0 - for private mode

            if (pref.getBoolean("keep_logged_in", Boolean.parseBoolean(null))) {

//                YouruserName = "Trying from SP Uname: "+ pref.getString("user_name", null);
//                YourpassWorD = "Password: "+pref.getString("user_password", null);

//                Toast.makeText(getContext(),
//                        YouruserName, Toast.LENGTH_SHORT).show();
//                Toast.makeText(getContext(),
//                        YourpassWorD, Toast.LENGTH_SHORT).show();

                makeJsonObjectRequest();
            }

            makeJsonObjecttRequestForDailySales();


            int total_station_no = pref.getInt("total_station", 0);
            int used_station_no = pref.getInt("used_station", 0);
            int available_station_no = total_station_no - used_station_no;

            int total_store_no = pref.getInt("total_store", 0);
            int used_store_no = pref.getInt("used_store", 0);
            int available_store_no = total_store_no - used_store_no;

            int total_user_no = pref.getInt("total_user", 0);
            int used_user_no = pref.getInt("used_user", 0);
            int available_user_no = total_user_no - used_user_no;

            store_name.setText(pref.getString("store_name", null)); // getting String
            station_name.setText(pref.getString("station_name", null)); // getting String


            stock_value.setText(pref.getString("total_stock", null)+" Tk"); // getting String
            total_station.setText(String.valueOf(total_station_no)); // getting String
            used_station.setText(String.valueOf(used_station_no)); // getting String
            available_station.setText(String.valueOf(available_station_no)); // getting String

            total_store.setText(String.valueOf(total_store_no));
            used_store.setText(String.valueOf(used_store_no));
            available_store.setText(String.valueOf(available_store_no));

            total_user.setText(String.valueOf(total_user_no));
            used_user.setText(String.valueOf(used_user_no));
            available_user.setText(String.valueOf(available_user_no));


        } catch (Exception e) {
            e.printStackTrace();
        }


        return v;
    }


    public void makeJsonObjectRequest() {

        //showpDialog();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                baseUrl, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject info = response.getJSONObject("response");
                    String user_name = info.getString("user_name");
                    String user_id = info.getString("user_id");
                    String user_full_name = info.getString("user_full_name");
                    String user_email = info.getString("user_email");
                    String user_phone = info.getString("user_phone");
                    String store_id = info.getString("store_id");
                    String store_name = info.getString("store_name");
                    String station_id = info.getString("station_id");
                    String station_name = info.getString("station_name");
                    String logged_in = info.getString("logged_in");

                    JSONObject InnerInfo = info.getJSONObject("info");


                    total_stock = InnerInfo.getString("total_stock");
                    int total_station = Integer.parseInt(InnerInfo.getString("total_station"));
                    int used_station = Integer.parseInt(InnerInfo.getString("used_station"));
                    int total_store = Integer.parseInt(InnerInfo.getString("total_store"));
                    int used_store = Integer.parseInt(InnerInfo.getString("used_store"));
                    int total_user = Integer.parseInt(InnerInfo.getString("total_user"));
                    int used_user = Integer.parseInt(InnerInfo.getString("used_user"));


//                    dashboardModals = new DashboardModals(user_name, user_id, user_full_name, user_email, user_phone, store_id, store_name, station_id, station_name, logged_in, total_stock, total_station, used_station, total_store, used_store, total_user, used_user);
//
//                    dashboardModals.setUser_name(user_name);
//                    dashboardModals.setUser_id(user_id);
//                    dashboardModals.setUser_full_name(user_full_name);
//                    dashboardModals.setUser_email(user_email);
//                    dashboardModals.setUser_phone(user_phone);
//                    dashboardModals.setStore_id(store_id);
//                    dashboardModals.setStore_name(store_name);
//                    dashboardModals.setStation_id(station_id);
//                    dashboardModals.setStation_name(station_name);
//                    dashboardModals.setLogged_in(logged_in);
//
//
//                    dashboardModals.setTotal_stock(total_stock);
//                    dashboardModals.setTotal_station(total_station);
//                    dashboardModals.setUsed_station(used_station);
//                    dashboardModals.setTotal_store(total_store);
//                    dashboardModals.setUsed_store(used_store);
//                    dashboardModals.setTotal_user(total_user);
//                    dashboardModals.setUsed_user(used_user);

                    jsonResponse = "";
                    jsonResponse += "user_full_name: " + user_full_name + "\n\n";
                    jsonResponse += "total_stock: " + total_stock + "\n\n";
                    jsonResponse += "total_station: " + total_station + "\n\n";


                    editor = pref.edit();

                    editor.putBoolean("keep_logged_in", true); // Storing boolean - true/false
                    editor.putString("user_full_name", user_full_name); // Storing string
                    editor.putString("user_email", user_email); // Storing string
                    editor.putString("total_stock", total_stock); // Storing string
                    editor.putInt("total_station", total_station); // Storing integer
                    editor.putInt("used_station", used_station); // Storing integer


                    editor.putString("store_name", store_name); // Storing string
                    editor.putString("station_name", station_name); // Storing string


                    editor.putInt("total_store", total_store); // Storing integer
                    editor.putInt("used_store", used_store); // Storing integer


                    editor.putInt("total_user", total_user); // Storing integer
                    editor.putInt("used_user", used_user); // Storing integer


                    editor.commit(); // commit changes


                } catch (JSONException e) {
                    e.printStackTrace();



                }


                // hidepDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                Toast.makeText(getContext(),
                        "Check Internet Connection and your user name and password",
                        Toast.LENGTH_LONG).show();
                //hidepDialog();
            }
        }) {
//            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                String userName = "pos01";
//                String password = "12345";
//                params.put("userName", userName);
//                params.put("passWorD", password);
//                return params;
//
//            }


            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();


//                String userName = "pos01";
//                String password = "12345";


//                headers.put("userName", userName);
//                headers.put("passWorD", password);
//                headers.put("Content-Type", "application/json");
//                 headers.put("Accept", "application/json");
//                headers.put("Host", "sms.posspot.com");
//               headers.put("Accept-Encoding", "utf-8");
                String creds = String.format("%s:%s", "posspot", "synt3ch");
                System.out.println(creds);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);
                return headers;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue

        AppController.getInstance().getRequestQueue().getCache().remove(baseUrl);
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void makeJsonObjecttRequestForDailySales() {

        String m = "http://";
        String urlJsonObj= m.concat(YouruserName+ ".posspot.com/api/DP_api/daily_sale_reports");


        // done

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlJsonObj, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONArray info = response.getJSONArray("response");

                    for (int i = 0; i < info.length(); i++) {

                        JSONObject jsonObject = info.getJSONObject(i);

                        DailySaleStoreName = jsonObject.getString("store_name");
                        DailySaleAmount = jsonObject.getString("amount");

                        // retrieve the values like this so on..

                    }




                    jsonResponse = "";
                    jsonResponse += "StoreName: " + DailySaleStoreName + "\n\n";
                    jsonResponse += "Ammount" + DailySaleAmount + "\n\n";

                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                    CurrentDay.setText(date);
                    dailySaleStoreName.setText(DailySaleStoreName);
                    dailySaleAmount.setText(DailySaleAmount+" Tk");



                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                // hide the progress dialog
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }
    @Override
    public void onResume() {

        super.onResume();



        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            private Boolean exit = false;
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button


                    if (exit) {
                        getActivity().finish(); // finish activity
                    } else {
                        Toast.makeText(getActivity(),"Press Back again to Exit",Toast.LENGTH_SHORT).show();
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 3 * 1000);

                    }





                    return true;

                }

                return false;
            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



}
