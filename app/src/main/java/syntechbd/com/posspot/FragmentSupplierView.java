package syntechbd.com.posspot;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import syntechbd.com.posspot.adapter.SalesInvoiceAdapter;
import syntechbd.com.posspot.adapter.SupplierViewsAdapter;
import syntechbd.com.posspot.app.AppController;
import syntechbd.com.posspot.modal.Cutomers;
import syntechbd.com.posspot.modal.SaleInvoiceModal;
import syntechbd.com.posspot.modal.SuppliersModal;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSupplierView extends Fragment {

    private static String TAG = FragmentSupplierView.class.getSimpleName();

    EditText invoiceNoET, customerNameET;

    Calendar calendar;

    int year, month, day, date;

    Button Search;

    ListView listView;
    SupplierViewsAdapter adapter;
    ArrayList<SuppliersModal> productList = new ArrayList<SuppliersModal>();
    ArrayList<String> supplierList = new ArrayList<>();

    String SupplierName = "1";
    String SupplierListURL;

    Spinner supplierDropDownSP ;

    SharedPreferences pref;
    String YouruserName;
    ProgressDialog progressdialog;


    public FragmentSupplierView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        if (!isNetworkConnected()){
            Toast.makeText(getContext(),"Connection Lost !! Check your internet connection! ",Toast.LENGTH_LONG).show();
        }

        pref = getActivity().getSharedPreferences("UserInfo", 0); // 0 - for private mode


        YouruserName = pref.getString("user_name", null);


        SupplierListURL = "http://"+YouruserName+".posspot.com/api/DP_api/suppliers";




        getActivity().setTitle("Suppliers Report");

        View v = inflater.inflate(R.layout.fragment_supplier_view, container, false);


        Search = (Button) v.findViewById(R.id.btnSearchInSellesInvoices);
        listView = (ListView) v.findViewById(R.id.AllCustomersLV);
        supplierDropDownSP = (Spinner) v.findViewById(R.id.supplierListinSuppliersSP);


        loadSpinnerData(SupplierListURL);




        supplierDropDownSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SupplierName = Integer.toString(i + 1);
               // Toast.makeText(getContext(), StoreName, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });


       // makeJsonObjecttRequestForDailySales();


        // listView.setTextFilterEnabled(true);
        adapter = new SupplierViewsAdapter(getActivity(), productList);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
        productList.clear();


        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressdialog = new ProgressDialog(getActivity());
                progressdialog.setMessage("Searching...");
                progressdialog.setCancelable(false);
                progressdialog.show();

                makeJsonObjecttRequestForDailySales(SupplierName);

                // listView.setTextFilterEnabled(true);


                adapter = new SupplierViewsAdapter(getActivity(), productList);
                adapter.notifyDataSetChanged();
                listView.setAdapter(adapter);
                productList.clear();
                //progressdialog.dismiss();

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SuppliersModal cutomers = (SuppliersModal) parent.getItemAtPosition(position);
                Intent intent = new Intent(getContext(), SupplierDetailsActivity.class);
                intent.putExtra("id_supplier", cutomers.getId_supplier());
                intent.putExtra("supplier_code", cutomers.getSupplier_code());

                intent.putExtra("supplier_name", cutomers.getSupplier_name());
                intent.putExtra("contact_person", cutomers.getContact_person());
                intent.putExtra("phone", cutomers.getPhone());
                intent.putExtra("email", cutomers.getEmail());
                intent.putExtra("div_id", cutomers.getDiv_id());
                intent.putExtra("dist_id", cutomers.getDist_id());
                intent.putExtra("upz_id", cutomers.getUpz_id());
                intent.putExtra("unn_id", cutomers.getUnn_id());

                intent.putExtra("city_id", cutomers.getCity_id());
                intent.putExtra("area_id", cutomers.getArea_id());
                intent.putExtra("profile_img", cutomers.getProfile_img());
                intent.putExtra("post_code", cutomers.getPost_code());
                intent.putExtra("vat_reg_no", cutomers.getVat_reg_no());
                intent.putExtra("note", cutomers.getNote());
                intent.putExtra("addr_line_1", cutomers.getAddr_line_1());
                intent.putExtra("balance", cutomers.getBalance());
                intent.putExtra("dtt_add", cutomers.getDtt_add());
                intent.putExtra("uid_add", cutomers.getUid_add());
                intent.putExtra("dtt_mod", cutomers.getDtt_mod());
                intent.putExtra("uid_mod", cutomers.getUid_mod());
                intent.putExtra("version", cutomers.getVersion());
                intent.putExtra("stores", cutomers.getStores());

                //Toast.makeText(getContext(),cutomers.getSupplier_name(),Toast.LENGTH_LONG).show();
                startActivity(intent);

            }
        });



        return v;
    }


    private void loadSpinnerData(String url) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("response");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String country = jsonObject1.getString("supplier_name");

                        supplierList.add(country);
                    }

                    ArrayAdapter<String> customerTypeAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, supplierList);
                    customerTypeAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                    supplierDropDownSP.setAdapter(customerTypeAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }


//    public void makeJsonObjecttRequestForDailySales() {
//
//        String urlJsonObj = "http://"+YouruserName+".posspot.com/api/DP_api/sell_reports?invoice_no=&store_id=&FromDate=&ToDate=&customer_name=";
//        // done
//
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
//                urlJsonObj, null, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject response) {
//                Log.d(TAG, response.toString());
//
//                try {
//                    // Parsing json object response
//                    // response will be a json object
//                    JSONArray info = response.getJSONArray("response");
//
//                    for (int i = 0; i < info.length(); i++) {
//
//                        JSONObject jsonObject = info.getJSONObject(i);
//
//                        SaleInvoiceModal sched = new SaleInvoiceModal();
//
//                        sched.setInvoiceNo(jsonObject.getString("invoice_no"));
//                        sched.setDateOfSold(jsonObject.getString("sale_date"));
//                        sched.setInvoiceAmount(jsonObject.getString("total_amount"));
//                        //sched.setSoldBy(jsonObject.getString("store_id"));
//                        sched.setCustomerName(jsonObject.getString("sale_id"));
//                        //sched.setStationName(jsonObject.getString("email"));
//                        sched.setStoreName(jsonObject.getString("store_name"));
//
//                        productList.add(sched);
//
//                        // retrieve the values like this so on..
//
//                    }
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//
//                }
//
//                adapter.notifyDataSetChanged();
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                VolleyLog.d(TAG, "Error: " + error.getMessage());
//
//                // hide the progress dialog
//            }
//        });
//
//        // Adding request to request queue
//        AppController.getInstance().addToRequestQueue(jsonObjReq);
//
//
//    }

    public void makeJsonObjecttRequestForDailySales(String storeID) {

        // String urlJsonObj = "http://pos01.posspot.com/api/sell_reports?invoice_no=&store_id=&FromDate=&ToDate=&customer_name=";

        String incompleteUrl = "http://"+YouruserName+".posspot.com/api/DP_api/supplier_details?supplier_id=";
        String urlJsonObj = incompleteUrl.concat(storeID + "&store_id=");

       // Toast.makeText(getContext(), urlJsonObj, Toast.LENGTH_LONG).show();
        // done

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlJsonObj, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONArray info = response.getJSONArray("response");

                    for (int i = 0; i < info.length(); i++) {

                        JSONObject jsonObject = info.getJSONObject(i);

                        SuppliersModal sched = new SuppliersModal();

                        sched.setId_supplier(jsonObject.getString("id_supplier"));
                        sched.setSupplier_code(jsonObject.getString("supplier_code"));
                        sched.setSupplier_name(jsonObject.getString("supplier_name"));
                        sched.setContact_person(jsonObject.getString("contact_person"));
                        sched.setPhone(jsonObject.getString("phone"));
                        sched.setEmail(jsonObject.getString("email"));
                        sched.setDiv_id(jsonObject.getString("div_id"));
                        sched.setDist_id(jsonObject.getString("dist_id"));
                        sched.setUpz_id(jsonObject.getString("upz_id"));
                        sched.setUnn_id(jsonObject.getString("unn_id"));
                        sched.setCity_id(jsonObject.getString("city_id"));
                        sched.setArea_id(jsonObject.getString("area_id"));
                        sched.setProfile_img(jsonObject.getString("profile_img"));
                        sched.setPost_code(jsonObject.getString("post_code"));
                        sched.setVat_reg_no(jsonObject.getString("vat_reg_no"));
                        sched.setNote(jsonObject.getString("note"));
                        sched.setAddr_line_1(jsonObject.getString("addr_line_1"));
                        sched.setBalance(jsonObject.getString("balance"));
                        sched.setDtt_add(jsonObject.getString("dtt_add"));
                        sched.setUid_add(jsonObject.getString("uid_add"));
                        sched.setDtt_mod(jsonObject.getString("dtt_mod"));
                        sched.setUid_mod(jsonObject.getString("uid_mod"));
                        sched.setStatus_idc(jsonObject.getString("status_id"));
                        sched.setVersion(jsonObject.getString("version"));
                        sched.setStores(jsonObject.getString("stores"));

                        productList.add(sched);
                        progressdialog.setMessage("Collecting data...");

                        // retrieve the values like this so on..

                    }

                    progressdialog.dismiss();


                } catch (JSONException e) {
                    e.printStackTrace();


                }

                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressdialog.setMessage("No Data Found");
                progressdialog.dismiss();
                Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_SHORT).show();
                // hide the progress dialog
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);


    }


    @Override
    public void onResume() {

        super.onResume();


        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            private Boolean exit = false;

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    // handle back button


                    if (exit) {
                        getActivity().finish(); // finish activity
                    } else {
                        Toast.makeText(getActivity(), "Press Back again to Exit", Toast.LENGTH_SHORT).show();
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 3 * 1000);

                    }


                    return true;

                }

                return false;
            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


}
