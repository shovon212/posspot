package syntechbd.com.posspot.modal;

public class Overview_ListView {

    private int ColumnId;

    private String  ProductCode
            , ProductName
            , SelectedBatch
            , ProductPrice
            , ProductStock
            , ProductQty
            , ProductDiscount
            , ProductDiscountPromotionID
            , Vat
            , TotalTaka;

    public Overview_ListView() {
    }

    public Overview_ListView(int columnId,String productCode, String productName, String selectedBatch, String productPrice, String productStock, String productQty, String productDiscount, String productDiscountPromotionID, String vat, String totalTaka) {
        ColumnId = columnId;
        ProductCode = productCode;
        ProductName = productName;
        SelectedBatch = selectedBatch;
        ProductPrice = productPrice;
        ProductStock = productStock;
        ProductQty = productQty;
        ProductDiscount = productDiscount;
        ProductDiscountPromotionID = productDiscountPromotionID;
        Vat = vat;
        TotalTaka = totalTaka;
    }

    public int getColumnId() {
        return ColumnId;
    }

    public void setColumnId(int columnId) {
        ColumnId = columnId;
    }


    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getSelectedBatch() {
        return SelectedBatch;
    }

    public void setSelectedBatch(String selectedBatch) {
        SelectedBatch = selectedBatch;
    }

    public String getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(String productPrice) {
        ProductPrice = productPrice;
    }

    public String getProductStock() {
        return ProductStock;
    }

    public void setProductStock(String productStock) {
        ProductStock = productStock;
    }

    public String getProductQty() {
        return ProductQty;
    }

    public void setProductQty(String productQty) {
        ProductQty = productQty;
    }

    public String getProductDiscount() {
        return ProductDiscount;
    }

    public void setProductDiscount(String productDiscount) {
        ProductDiscount = productDiscount;
    }

    public String getProductDiscountPromotionID() {
        return ProductDiscountPromotionID;
    }

    public void setProductDiscountPromotionID(String productDiscountPromotionID) {
        ProductDiscountPromotionID = productDiscountPromotionID;
    }

    public String getVat() {
        return Vat;
    }

    public void setVat(String vat) {
        Vat = vat;
    }

    public String getTotalTaka() {
        return TotalTaka;
    }

    public void setTotalTaka(String totalTaka) {
        TotalTaka = totalTaka;
    }
}
