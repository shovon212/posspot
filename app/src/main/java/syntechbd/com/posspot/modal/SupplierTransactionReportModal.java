package syntechbd.com.posspot.modal;

import java.io.Serializable;

public class SupplierTransactionReportModal implements Serializable {
    private String trx_no;
    private String trx_with;
    private String account_id;
    private String invoice_no;
    private String ref_id;
    private String tot_amount;
    private String dtt_trx;
    private String store_id;
    private String supplier_name;
    private String store_name;
    private String account_no;


    public SupplierTransactionReportModal(){

    }

    public String getTrx_no() {
        return trx_no;
    }

    public void setTrx_no(String trx_no) {
        this.trx_no = trx_no;
    }

    public String getTrx_with() {
        return trx_with;
    }

    public void setTrx_with(String trx_with) {
        this.trx_with = trx_with;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getInvoice_no() {
        return invoice_no;
    }

    public void setInvoice_no(String invoice_no) {
        this.invoice_no = invoice_no;
    }

    public String getRef_id() {
        return ref_id;
    }

    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    public String getTot_amount() {
        return tot_amount;
    }

    public void setTot_amount(String tot_amount) {
        this.tot_amount = tot_amount;
    }

    public String getDtt_trx() {
        return dtt_trx;
    }

    public void setDtt_trx(String dtt_trx) {
        this.dtt_trx = dtt_trx;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getAccount_no() {
        return account_no;
    }

    public void setAccount_no(String account_no) {
        this.account_no = account_no;
    }
}
