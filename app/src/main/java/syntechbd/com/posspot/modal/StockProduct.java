package syntechbd.com.posspot.modal;

public class StockProduct {

    private String ProductName,ProductCode,ProductQty;

    public StockProduct(String productName, String productCode, String productQty) {
        ProductName = productName;
        ProductCode = productCode;
        ProductQty = productQty;
    }

    public StockProduct() {

    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getProductQty() {
        return ProductQty;
    }

    public void setProductQty(String productQty) {
        ProductQty = productQty;
    }
}
