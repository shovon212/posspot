package syntechbd.com.posspot.modal;

public class DashboardModals {

    private String user_name;
    private String user_id;
    private String user_full_name;
    private String user_email;
    private String user_phone;
    private String store_id;
    private String store_name;
    private String station_id;
    private String station_name;
    private String logged_in;
    private long total_stock;
    private int total_station;
    private int used_station;
    private int total_store;
    private int used_store;
    private int total_user;
    private int used_user;

    public  DashboardModals(){

    }


    public DashboardModals(String user_name, String user_id, String user_full_name, String user_email, String user_phone, String store_id, String store_name, String station_id, String station_name, String logged_in, long total_stock, int total_station, int used_station, int total_store, int used_store, int total_user, int used_user) {
        this.user_name = user_name;
        this.user_id = user_id;
        this.user_full_name = user_full_name;
        this.user_email = user_email;
        this.user_phone = user_phone;
        this.store_id = store_id;
        this.store_name = store_name;
        this.station_id = station_id;
        this.station_name = station_name;
        this.logged_in = logged_in;
        this.total_stock = total_stock;
        this.total_station = total_station;
        this.used_station = used_station;
        this.total_store = total_store;
        this.used_store = used_store;
        this.total_user = total_user;
        this.used_user = used_user;
    }


    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_full_name() {
        return user_full_name;
    }

    public void setUser_full_name(String user_full_name) {
        this.user_full_name = user_full_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getStation_id() {
        return station_id;
    }

    public void setStation_id(String station_id) {
        this.station_id = station_id;
    }

    public String getStation_name() {
        return station_name;
    }

    public void setStation_name(String station_name) {
        this.station_name = station_name;
    }

    public String getLogged_in() {
        return logged_in;
    }

    public void setLogged_in(String logged_in) {
        this.logged_in = logged_in;
    }

    public long getTotal_stock() {
        return total_stock;
    }

    public void setTotal_stock(long total_stock) {
        this.total_stock = total_stock;
    }

    public int getTotal_station() {
        return total_station;
    }

    public void setTotal_station(int total_station) {
        this.total_station = total_station;
    }

    public int getUsed_station() {
        return used_station;
    }

    public void setUsed_station(int used_station) {
        this.used_station = used_station;
    }

    public int getTotal_store() {
        return total_store;
    }

    public void setTotal_store(int total_store) {
        this.total_store = total_store;
    }

    public int getUsed_store() {
        return used_store;
    }

    public void setUsed_store(int used_store) {
        this.used_store = used_store;
    }

    public int getTotal_user() {
        return total_user;
    }

    public void setTotal_user(int total_user) {
        this.total_user = total_user;
    }

    public int getUsed_user() {
        return used_user;
    }

    public void setUsed_user(int used_user) {
        this.used_user = used_user;
    }
}
