package syntechbd.com.posspot.modal;

import java.util.ArrayList;

public class AddTOCartModal {

    private int ColumnId; //1
    String id_stock;
    String id_product; //2
    String product_code;//3
    String product_name;//4
    String cat_id;
    String subcat_id;
    String brand_id;
    String unit_id;
    String is_vatable;
    String store_id;
    String batch_no;
    String total_qty;//7
    String selling_price_est; //6
    String discount_amt;
    String[] batch_nos;
    String discount;
    String def_vat;

    ArrayList<String> batchListModal;

    private String SelectedBatch
            , ProductQty
            , ProductDiscount
            , ProductDiscountPromotionID
            , Vat
            , TotalTaka;

    public ArrayList<String> getBatchListModal() {
        return batchListModal;
    }

    public void setBatchListModal(ArrayList<String> batchListModal) {
        this.batchListModal = batchListModal;
    }

    public AddTOCartModal() {
    }

    public AddTOCartModal(int columnId, String product_code) {
        ColumnId = columnId;
        this.product_code = product_code;
    }

    public int getColumnId() {
        return ColumnId;
    }

    public void setColumnId(int columnId) {
        ColumnId = columnId;
    }

    public String getSelectedBatch() {
        return SelectedBatch;
    }

    public void setSelectedBatch(String selectedBatch) {
        SelectedBatch = selectedBatch;
    }

    public String getProductQty() {
        return ProductQty;
    }

    public void setProductQty(String productQty) {
        ProductQty = productQty;
    }

    public String getProductDiscount() {
        return ProductDiscount;
    }

    public void setProductDiscount(String productDiscount) {
        ProductDiscount = productDiscount;
    }

    public String getProductDiscountPromotionID() {
        return ProductDiscountPromotionID;
    }

    public void setProductDiscountPromotionID(String productDiscountPromotionID) {
        ProductDiscountPromotionID = productDiscountPromotionID;
    }

    public String getVat() {
        return Vat;
    }

    public void setVat(String vat) {
        Vat = vat;
    }

    public String getTotalTaka() {
        return TotalTaka;
    }

    public void setTotalTaka(String totalTaka) {
        TotalTaka = totalTaka;
    }

    public AddTOCartModal(int columnId, String id_product, String product_code, String product_name, String total_qty, String selling_price_est, String selectedBatch, String productQty, String productDiscount, String productDiscountPromotionID, String vat, String totalTaka) {
        ColumnId = columnId;
        this.id_product = id_product;
        this.product_code = product_code;
        this.product_name = product_name;
        this.total_qty = total_qty;
        this.selling_price_est = selling_price_est;
        SelectedBatch = selectedBatch;
        ProductQty = productQty;
        ProductDiscount = productDiscount;
        ProductDiscountPromotionID = productDiscountPromotionID;
        Vat = vat;
        TotalTaka = totalTaka;
    }

    public String getId_stock() {
        return id_stock;
    }

    public void setId_stock(String id_stock) {
        this.id_stock = id_stock;
    }

    public String getId_product() {
        return id_product;
    }

    public void setId_product(String id_product) {
        this.id_product = id_product;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSubcat_id() {
        return subcat_id;
    }

    public void setSubcat_id(String subcat_id) {
        this.subcat_id = subcat_id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getIs_vatable() {
        return is_vatable;
    }

    public void setIs_vatable(String is_vatable) {
        this.is_vatable = is_vatable;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getBatch_no() {
        return batch_no;
    }

    public void setBatch_no(String batch_no) {
        this.batch_no = batch_no;
    }

    public String getTotal_qty() {
        return total_qty;
    }

    public void setTotal_qty(String total_qty) {
        this.total_qty = total_qty;
    }

    public String getSelling_price_est() {
        return selling_price_est;
    }

    public void setSelling_price_est(String selling_price_est) {
        this.selling_price_est = selling_price_est;
    }

    public String getDiscount_amt() {
        return discount_amt;
    }

    public void setDiscount_amt(String discount_amt) {
        this.discount_amt = discount_amt;
    }

    public String[] getBatch_nos() {
        return batch_nos;
    }

    public void setBatch_nos(String[] batch_nos) {
        this.batch_nos = batch_nos;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDef_vat() {
        return def_vat;
    }

    public void setDef_vat(String def_vat) {
        this.def_vat = def_vat;
    }
}
