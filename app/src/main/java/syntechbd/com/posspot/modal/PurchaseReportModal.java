package syntechbd.com.posspot.modal;

import java.io.Serializable;

public class PurchaseReportModal implements Serializable {
    private String DateOfSold;
    private String ProductName;
    private String CatName;
    private String SubcatName;
    private String StoreName;
    private String SupplierName;
    private String PurchasePrice;
    private String Qty;
    private String BrandName;
    private String SupplierId;


    public PurchaseReportModal(){

    }


    public String getDateOfSold() {
        return DateOfSold;
    }

    public void setDateOfSold(String dateOfSold) {
        DateOfSold = dateOfSold;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getCatName() {
        return CatName;
    }

    public void setCatName(String catName) {
        CatName = catName;
    }

    public String getSubcatName() {
        return SubcatName;
    }

    public void setSubcatName(String subcatName) {
        SubcatName = subcatName;
    }

    public String getStoreName() {
        return StoreName;
    }

    public void setStoreName(String storeName) {
        StoreName = storeName;
    }

    public String getSupplierName() {
        return SupplierName;
    }

    public void setSupplierName(String supplierName) {
        SupplierName = supplierName;
    }

    public String getPurchasePrice() {
        return PurchasePrice;
    }

    public void setPurchasePrice(String purchasePrice) {
        PurchasePrice = purchasePrice;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    public String getSupplierId() {
        return SupplierId;
    }

    public void setSupplierId(String supplierId) {
        SupplierId = supplierId;
    }
}
