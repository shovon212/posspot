package syntechbd.com.posspot.modal;

import java.io.Serializable;

public class SupplierPayableReportModal implements Serializable {
    private String due_amt;
    private String invoice_no;
    private String store_id;
    private String tot_amt;
    private String paid_amt;
    private String dtt_add;
    private String store_name;
    private String supplier_name;


    public SupplierPayableReportModal(){

    }

    public SupplierPayableReportModal(String due_amt, String invoice_no, String store_id, String tot_amt, String paid_amt, String dtt_add, String store_name, String supplier_name) {
        this.due_amt = due_amt;
        this.invoice_no = invoice_no;
        this.store_id = store_id;
        this.tot_amt = tot_amt;
        this.paid_amt = paid_amt;
        this.dtt_add = dtt_add;
        this.store_name = store_name;
        this.supplier_name = supplier_name;
    }

    public String getDue_amt() {
        return due_amt;
    }

    public void setDue_amt(String due_amt) {
        this.due_amt = due_amt;
    }

    public String getInvoice_no() {
        return invoice_no;
    }

    public void setInvoice_no(String invoice_no) {
        this.invoice_no = invoice_no;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getTot_amt() {
        return tot_amt;
    }

    public void setTot_amt(String tot_amt) {
        this.tot_amt = tot_amt;
    }

    public String getPaid_amt() {
        return paid_amt;
    }

    public void setPaid_amt(String paid_amt) {
        this.paid_amt = paid_amt;
    }

    public String getDtt_add() {
        return dtt_add;
    }

    public void setDtt_add(String dtt_add) {
        this.dtt_add = dtt_add;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }
}
