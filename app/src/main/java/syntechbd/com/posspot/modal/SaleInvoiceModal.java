package syntechbd.com.posspot.modal;

import java.io.Serializable;

public class SaleInvoiceModal implements Serializable{

   private String InvoiceNo;
   private String DateOfSold;
   private String InvoiceAmount;
   private String CustomerName;
   private String StoreName;

    public SaleInvoiceModal(String invoiceNo, String dateOfSold, String invoiceAmount,String customerName,String storeName) {
        InvoiceNo = invoiceNo;
        DateOfSold = dateOfSold;
        InvoiceAmount = invoiceAmount;
        CustomerName = customerName;
        StoreName = storeName;
    }

    public SaleInvoiceModal() {

    }

    public String getInvoiceNo() {
        return InvoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        InvoiceNo = invoiceNo;
    }

    public String getDateOfSold() {
        return DateOfSold;
    }

    public void setDateOfSold(String dateOfSold) {
        DateOfSold = dateOfSold;
    }

    public String getInvoiceAmount() {
        return InvoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        InvoiceAmount = invoiceAmount;
    }



    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }


    public String getStoreName() {
        return StoreName;
    }

    public void setStoreName(String storeName) {
        StoreName = storeName;
    }
}
