package syntechbd.com.posspot.modal;

import java.util.ArrayList;

public class SalesInfoModal {

    String pro_id,batch,qty,stock_id,cat_id
            ,subcat_id,brand_id,unit_id,unit_price
            ,discount,discount_type,total_price,pro_sale_id
            ,store_from,station_id,def_vat,def_vat_amt
            ,original_product_price,pro_name,pro_code,total_qty;

    String grand_total,cart_total,special_dis,cus_dis,pur_dis,card_dis
            ,customer_id,customer_code,ck_sp_dis
            ,ck_cus_dis,ck_pur_dis,ck_cart_dis;



    public SalesInfoModal(String grand_total, String cart_total, String special_dis, String cus_dis, String pur_dis, String card_dis, String customer_id, String customer_code, String ck_sp_dis, String ck_cus_dis, String ck_pur_dis, String ck_cart_dis) {
        this.grand_total = grand_total;
        this.cart_total = cart_total;
        this.special_dis = special_dis;
        this.cus_dis = cus_dis;
        this.pur_dis = pur_dis;
        this.card_dis = card_dis;
        this.customer_id = customer_id;
        this.customer_code = customer_code;
        this.ck_sp_dis = ck_sp_dis;
        this.ck_cus_dis = ck_cus_dis;
        this.ck_pur_dis = ck_pur_dis;
        this.ck_cart_dis = ck_cart_dis;
    }

    public SalesInfoModal(String pro_id, String batch, String qty, String stock_id, String cat_id, String subcat_id, String brand_id, String unit_id, String unit_price, String discount, String discount_type, String total_price, String pro_sale_id, String store_from, String station_id, String def_vat, String def_vat_amt, String original_product_price,String pro_name,String pro_code,String total_qty) {
        this.pro_id = pro_id;
        this.batch = batch;
        this.qty = qty;
        this.stock_id = stock_id;
        this.cat_id = cat_id;
        this.subcat_id = subcat_id;
        this.brand_id = brand_id;
        this.unit_id = unit_id;
        this.unit_price = unit_price;
        this.discount = discount;
        this.discount_type = discount_type;
        this.total_price = total_price;
        this.pro_sale_id = pro_sale_id;
        this.store_from = store_from;
        this.station_id = station_id;
        this.def_vat = def_vat;
        this.def_vat_amt = def_vat_amt;
        this.original_product_price = original_product_price;
        this.pro_name = pro_name;
        this.pro_code = pro_code;
        this.total_qty = total_qty;
    }



    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getPro_code() {
        return pro_code;
    }

    public void setPro_code(String pro_code) {
        this.pro_code = pro_code;
    }

    public String getTotal_qty() {
        return total_qty;
    }

    public void setTotal_qty(String total_qty) {
        this.total_qty = total_qty;
    }

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getStock_id() {
        return stock_id;
    }

    public void setStock_id(String stock_id) {
        this.stock_id = stock_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSubcat_id() {
        return subcat_id;
    }

    public void setSubcat_id(String subcat_id) {
        this.subcat_id = subcat_id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(String discount_type) {
        this.discount_type = discount_type;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getPro_sale_id() {
        return pro_sale_id;
    }

    public void setPro_sale_id(String pro_sale_id) {
        this.pro_sale_id = pro_sale_id;
    }

    public String getStore_from() {
        return store_from;
    }

    public void setStore_from(String store_from) {
        this.store_from = store_from;
    }

    public String getStation_id() {
        return station_id;
    }

    public void setStation_id(String station_id) {
        this.station_id = station_id;
    }

    public String getDef_vat() {
        return def_vat;
    }

    public void setDef_vat(String def_vat) {
        this.def_vat = def_vat;
    }

    public String getDef_vat_amt() {
        return def_vat_amt;
    }

    public void setDef_vat_amt(String def_vat_amt) {
        this.def_vat_amt = def_vat_amt;
    }

    public String getOriginal_product_price() {
        return original_product_price;
    }

    public void setOriginal_product_price(String original_product_price) {
        this.original_product_price = original_product_price;
    }

    public String getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(String grand_total) {
        this.grand_total = grand_total;
    }

    public String getCart_total() {
        return cart_total;
    }

    public void setCart_total(String cart_total) {
        this.cart_total = cart_total;
    }

    public String getSpecial_dis() {
        return special_dis;
    }

    public void setSpecial_dis(String special_dis) {
        this.special_dis = special_dis;
    }

    public String getCus_dis() {
        return cus_dis;
    }

    public void setCus_dis(String cus_dis) {
        this.cus_dis = cus_dis;
    }

    public String getPur_dis() {
        return pur_dis;
    }

    public void setPur_dis(String pur_dis) {
        this.pur_dis = pur_dis;
    }

    public String getCard_dis() {
        return card_dis;
    }

    public void setCard_dis(String card_dis) {
        this.card_dis = card_dis;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_code() {
        return customer_code;
    }

    public void setCustomer_code(String customer_code) {
        this.customer_code = customer_code;
    }

    public String getCk_sp_dis() {
        return ck_sp_dis;
    }

    public void setCk_sp_dis(String ck_sp_dis) {
        this.ck_sp_dis = ck_sp_dis;
    }

    public String getCk_cus_dis() {
        return ck_cus_dis;
    }

    public void setCk_cus_dis(String ck_cus_dis) {
        this.ck_cus_dis = ck_cus_dis;
    }

    public String getCk_pur_dis() {
        return ck_pur_dis;
    }

    public void setCk_pur_dis(String ck_pur_dis) {
        this.ck_pur_dis = ck_pur_dis;
    }

    public String getCk_cart_dis() {
        return ck_cart_dis;
    }

    public void setCk_cart_dis(String ck_cart_dis) {
        this.ck_cart_dis = ck_cart_dis;
    }
}
