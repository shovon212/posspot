package syntechbd.com.posspot.modal;

import java.io.Serializable;

public class Cutomers implements Serializable{

    private String id_customer,customer_code,customer_type_id,store_id,full_name,email,phone,gender,marital_status,spouse_name,birth_date,anniversary_date,profile_img,dtt_add,uid_add,dtt_mod,uid_mod,status_id,version,customer_type_name;
    private String points,balance;

    public Cutomers() {
    }

//    public Cutomers(String id_customer, String full_name, String email, String phone, String gender, String marital_status, String birth_date, String anniversary_date, String customer_type_name, String points, String balance) {
//        this.id_customer = id_customer;
//        this.full_name = full_name;
//        this.email = email;
//        this.phone = phone;
//        this.gender = gender;
//        this.marital_status = marital_status;
//        this.birth_date = birth_date;
//        this.anniversary_date = anniversary_date;
//        this.customer_type_name = customer_type_name;
//        this.points = points;
//        this.balance = balance;
//    }

    public Cutomers(String id_customer,String customer_code, String full_name, String phone, String balance, String points, String customer_type_name, String gender, String email, String birth_date, String marital_status, String anniversary_date, String profile_img) {
        this.id_customer = id_customer;
        this.customer_code = customer_code;
        this.full_name = full_name;
        this.phone = phone;
        this.balance = balance;
        this.points = points;
        this.customer_type_name = customer_type_name;
        this.gender = gender;
        this.email = email;
        this.birth_date = birth_date;
        this.marital_status = marital_status;
        this.anniversary_date = anniversary_date;
        this.profile_img = profile_img;
    }

    public String getId_customer() {
        return id_customer;
    }

    public void setId_customer(String id_customer) {
        this.id_customer = id_customer;
    }

    public String getCustomer_code() {
        return customer_code;
    }

    public void setCustomer_code(String customer_code) {
        this.customer_code = customer_code;
    }

    public String getCustomer_type_id() {
        return customer_type_id;
    }

    public void setCustomer_type_id(String customer_type_id) {
        this.customer_type_id = customer_type_id;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getAnniversary_date() {
        return anniversary_date;
    }

    public void setAnniversary_date(String anniversary_date) {
        this.anniversary_date = anniversary_date;
    }

    public String getProfile_img() {
        return profile_img;
    }

    public void setProfile_img(String profile_img) {
        this.profile_img = profile_img;
    }

    public String getDtt_add() {
        return dtt_add;
    }

    public void setDtt_add(String dtt_add) {
        this.dtt_add = dtt_add;
    }

    public String getUid_add() {
        return uid_add;
    }

    public void setUid_add(String uid_add) {
        this.uid_add = uid_add;
    }

    public String getDtt_mod() {
        return dtt_mod;
    }

    public void setDtt_mod(String dtt_mod) {
        this.dtt_mod = dtt_mod;
    }

    public String getUid_mod() {
        return uid_mod;
    }

    public void setUid_mod(String uid_mod) {
        this.uid_mod = uid_mod;
    }

    public String getStatus_id() {
        return status_id;
    }

    public void setStatus_id(String status_id) {
        this.status_id = status_id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCustomer_type_name() {
        return customer_type_name;
    }

    public void setCustomer_type_name(String customer_type_name) {
        this.customer_type_name = customer_type_name;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}
