package syntechbd.com.posspot.modal;

import java.io.Serializable;

public class CustomerWiseSellModal implements Serializable {

    String date, invoice_no,store_name,total_amount,customer_name;


    public CustomerWiseSellModal() {
    }

    public CustomerWiseSellModal(String date, String invoice_no, String store_name, String total_amount, String customer_name) {

        this.date = date;
        this.invoice_no = invoice_no;
        this.store_name = store_name;
        this.total_amount = total_amount;
        this.customer_name = customer_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInvoice_no() {
        return invoice_no;
    }

    public void setInvoice_no(String invoice_no) {
        this.invoice_no = invoice_no;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }
}
