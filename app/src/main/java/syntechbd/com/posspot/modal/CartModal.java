package syntechbd.com.posspot.modal;

public class CartModal {

    private int ColumnId;
    private String  ProductName
            ,ProductCode
            , ProductStock
            , TotalTaka
            , SelectedBatch
            , ProductQty
            , ProductPrice
            , ProductDiscount
            , ProductDiscountPromotionID
            , Vat
            ,DefaultDiscount
            ,deFaUlT_VAT;
    private  String discount_type,stock_id,cat_id,subcat_id,brand_id,unit_id,store_from,station_id,cart_total,original_product_price,pro_id;


    public CartModal(int columnId, String productName, String productCode, String productStock, String totalTaka, String selectedBatch, String productQty, String productPrice, String productDiscount, String productDiscountPromotionID, String vat, String defaultDiscount, String deFaUlT_VAT, String discount_type, String stock_id, String cat_id, String subcat_id, String brand_id, String unit_id, String store_from, String station_id, String cart_total, String original_product_price, String pro_id) {
        ColumnId = columnId;
        ProductName = productName;
        ProductCode = productCode;
        ProductStock = productStock;
        TotalTaka = totalTaka;
        SelectedBatch = selectedBatch;
        ProductQty = productQty;
        ProductPrice = productPrice;
        ProductDiscount = productDiscount;
        ProductDiscountPromotionID = productDiscountPromotionID;
        Vat = vat;
        DefaultDiscount = defaultDiscount;
        this.deFaUlT_VAT = deFaUlT_VAT;
        this.discount_type = discount_type;
        this.stock_id = stock_id;
        this.cat_id = cat_id;
        this.subcat_id = subcat_id;
        this.brand_id = brand_id;
        this.unit_id = unit_id;
        this.store_from = store_from;
        this.station_id = station_id;
        this.cart_total = cart_total;
        this.original_product_price = original_product_price;
        this.pro_id = pro_id;
    }



    public String getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(String discount_type) {
        this.discount_type = discount_type;
    }

    public String getStock_id() {
        return stock_id;
    }

    public void setStock_id(String stock_id) {
        this.stock_id = stock_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSubcat_id() {
        return subcat_id;
    }

    public void setSubcat_id(String subcat_id) {
        this.subcat_id = subcat_id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getStore_from() {
        return store_from;
    }

    public void setStore_from(String store_from) {
        this.store_from = store_from;
    }

    public String getStation_id() {
        return station_id;
    }

    public void setStation_id(String station_id) {
        this.station_id = station_id;
    }

    public String getCart_total() {
        return cart_total;
    }

    public void setCart_total(String cart_total) {
        this.cart_total = cart_total;
    }

    public String getOriginal_product_price() {
        return original_product_price;
    }

    public void setOriginal_product_price(String original_product_price) {
        this.original_product_price = original_product_price;
    }

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }


    public String getDeFaUlT_VAT() {
        return deFaUlT_VAT;
    }

    public void setDeFaUlT_VAT(String deFaUlT_VAT) {
        this.deFaUlT_VAT = deFaUlT_VAT;
    }

    public String getDefaultDiscount() {
        return DefaultDiscount;
    }

    public void setDefaultDiscount(String defaultDiscount) {
        DefaultDiscount = defaultDiscount;
    }

    public CartModal(int columnId, String productName, String productCode, String productStock, String totalTaka, String selectedBatch, String productQty, String productPrice, String productDiscount, String productDiscountPromotionID, String vat, String Default_discount, String deFaUlT_vat) {
        ColumnId = columnId;
        ProductName = productName;
        ProductCode = productCode;
        ProductStock = productStock;
        TotalTaka = totalTaka;
        SelectedBatch = selectedBatch;
        ProductQty = productQty;
        ProductPrice = productPrice;
        ProductDiscount = productDiscount;
        ProductDiscountPromotionID = productDiscountPromotionID;
        DefaultDiscount = Default_discount;
        deFaUlT_VAT = deFaUlT_vat;
        Vat = vat;
    }

    public int getColumnId() {
        return ColumnId;
    }

    public void setColumnId(int columnId) {
        ColumnId = columnId;
    }



    public CartModal() {
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getProductStock() {
        return ProductStock;
    }

    public void setProductStock(String productStock) {
        ProductStock = productStock;
    }

    public String getTotalTaka() {
        return TotalTaka;
    }

    public void setTotalTaka(String totalTaka) {
        TotalTaka = totalTaka;
    }

    public String getSelectedBatch() {
        return SelectedBatch;
    }

    public void setSelectedBatch(String selectedBatch) {
        SelectedBatch = selectedBatch;
    }

    public String getProductQty() {
        return ProductQty;
    }

    public void setProductQty(String productQty) {
        ProductQty = productQty;
    }

    public String getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(String productPrice) {
        ProductPrice = productPrice;
    }

    public String getProductDiscount() {
        return ProductDiscount;
    }

    public void setProductDiscount(String productDiscount) {
        ProductDiscount = productDiscount;
    }

    public String getProductDiscountPromotionID() {
        return ProductDiscountPromotionID;
    }

    public void setProductDiscountPromotionID(String productDiscountPromotionID) {
        ProductDiscountPromotionID = productDiscountPromotionID;
    }

    public String getVat() {
        return Vat;
    }

    public void setVat(String vat) {
        Vat = vat;
    }
}
