package syntechbd.com.posspot.modal;

import java.io.Serializable;

public class StockProductsModal2 implements Serializable {

    String product_batch,product_id,product_name,product_code;

    public StockProductsModal2(){

    }


    public String getProduct_batch() {
        return product_batch;
    }

    public void setProduct_batch(String product_batch) {
        this.product_batch = product_batch;
    }

    public String getProductName( ) {
        return product_name;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_code() {
        return product_name;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }
}
