package syntechbd.com.posspot.modal;

import java.io.Serializable;

public class SuppliersModal implements Serializable{

    private String id_supplier;
    private String supplier_code;
    private String supplier_name;
    private String contact_person;
    private String phone;
    private String email;
    private String div_id;
    private String dist_id;
    private String upz_id;
    private String unn_id;
    private String city_id;
    private String area_id;
    private String profile_img;
    private String post_code;
    private String vat_reg_no;
    private String note;
    private String addr_line_1;
    private String balance;
    private String dtt_add;
    private String uid_add;
    private String dtt_mod;
    private String uid_mod;
    private String status_idc;
    private String version;
    private String stores;

    public SuppliersModal() {
    }

    public SuppliersModal(String id_supplier, String supplier_code, String supplier_name, String contact_person, String phone, String email, String div_id, String dist_id, String upz_id, String unn_id, String city_id, String area_id, String profile_img, String post_code, String vat_reg_no, String note, String addr_line_1, String balance, String dtt_add, String uid_add, String dtt_mod, String uid_mod, String status_idc, String version, String stores) {
        this.id_supplier = id_supplier;
        this.supplier_code = supplier_code;
        this.supplier_name = supplier_name;
        this.contact_person = contact_person;
        this.phone = phone;
        this.email = email;
        this.div_id = div_id;
        this.dist_id = dist_id;
        this.upz_id = upz_id;
        this.unn_id = unn_id;
        this.city_id = city_id;
        this.area_id = area_id;
        this.profile_img = profile_img;
        this.post_code = post_code;
        this.vat_reg_no = vat_reg_no;
        this.note = note;
        this.addr_line_1 = addr_line_1;
        this.balance = balance;
        this.dtt_add = dtt_add;
        this.uid_add = uid_add;
        this.dtt_mod = dtt_mod;
        this.uid_mod = uid_mod;
        this.status_idc = status_idc;
        this.version = version;
        this.stores = stores;
    }

    public String getId_supplier() {
        return id_supplier;
    }

    public void setId_supplier(String id_supplier) {
        this.id_supplier = id_supplier;
    }

    public String getSupplier_code() {
        return supplier_code;
    }

    public void setSupplier_code(String supplier_code) {
        this.supplier_code = supplier_code;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    public String getContact_person() {
        return contact_person;
    }

    public void setContact_person(String contact_person) {
        this.contact_person = contact_person;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDiv_id() {
        return div_id;
    }

    public void setDiv_id(String div_id) {
        this.div_id = div_id;
    }

    public String getDist_id() {
        return dist_id;
    }

    public void setDist_id(String dist_id) {
        this.dist_id = dist_id;
    }

    public String getUpz_id() {
        return upz_id;
    }

    public void setUpz_id(String upz_id) {
        this.upz_id = upz_id;
    }

    public String getUnn_id() {
        return unn_id;
    }

    public void setUnn_id(String unn_id) {
        this.unn_id = unn_id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getArea_id() {
        return area_id;
    }

    public void setArea_id(String area_id) {
        this.area_id = area_id;
    }

    public String getProfile_img() {
        return profile_img;
    }

    public void setProfile_img(String profile_img) {
        this.profile_img = profile_img;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getVat_reg_no() {
        return vat_reg_no;
    }

    public void setVat_reg_no(String vat_reg_no) {
        this.vat_reg_no = vat_reg_no;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAddr_line_1() {
        return addr_line_1;
    }

    public void setAddr_line_1(String addr_line_1) {
        this.addr_line_1 = addr_line_1;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getDtt_add() {
        return dtt_add;
    }

    public void setDtt_add(String dtt_add) {
        this.dtt_add = dtt_add;
    }

    public String getUid_add() {
        return uid_add;
    }

    public void setUid_add(String uid_add) {
        this.uid_add = uid_add;
    }

    public String getDtt_mod() {
        return dtt_mod;
    }

    public void setDtt_mod(String dtt_mod) {
        this.dtt_mod = dtt_mod;
    }

    public String getUid_mod() {
        return uid_mod;
    }

    public void setUid_mod(String uid_mod) {
        this.uid_mod = uid_mod;
    }

    public String getStatus_idc() {
        return status_idc;
    }

    public void setStatus_idc(String status_idc) {
        this.status_idc = status_idc;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getStores() {
        return stores;
    }

    public void setStores(String stores) {
        this.stores = stores;
    }
}
