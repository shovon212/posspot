package syntechbd.com.posspot;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends Activity {

    SharedPreferences pref  ; // 0 - for private mode
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                pref = getApplicationContext().getSharedPreferences("UserInfo", 0); // 0 - for private mode



                try {
                    if(pref.getBoolean("keep_logged_in", Boolean.parseBoolean(null))){

                        Intent intent = new Intent(getBaseContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else {
                        Intent intent = new Intent(getBaseContext(), LoginFragmenta.class);
                        startActivity(intent);
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        },1800);





    }
}
