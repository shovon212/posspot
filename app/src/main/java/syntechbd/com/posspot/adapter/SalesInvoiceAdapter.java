package syntechbd.com.posspot.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import syntechbd.com.posspot.R;
import syntechbd.com.posspot.modal.Cutomers;
import syntechbd.com.posspot.modal.SaleInvoiceModal;


public class SalesInvoiceAdapter extends BaseAdapter{
//implements Filterable {
    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<SaleInvoiceModal> mlist ;
    public ArrayList<SaleInvoiceModal> mStringFilterList ;
   // private ValueFilter valueFilter;
    //StockProduct sched;

    public SalesInvoiceAdapter(Activity activity, ArrayList<SaleInvoiceModal> mlist) {
        this.activity = activity;
        this.mlist = mlist;
        this.mStringFilterList = mlist;

    }




    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mlist.size();
        //return (mlist.size() <1) ? 0 : mlist.size();
//        if(mlist != null || mlist.size() > 0) {
//            return mlist.size();
//        }else return 0;
  //      Log.e("mListSize:", String.valueOf(mlist.size()));
 //       return (mlist == null) ? 0 : mlist.size();

//        if (mlist.isEmpty()) {
//            return 0;
//        } else return mlist.size();

    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

//        sched=mlist.get(position);
//        LayoutInflater inflater=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        convertView=inflater.inflate(R.layout.stock_product_layout,parent,false);


        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.sales_invoice_row_layout, null);


        // getting movie data for the row
        SaleInvoiceModal sched = mlist.get(position);

        TextView tv1 = (TextView) convertView.findViewById(R.id.Invoice_NoTV);
        tv1.setText(sched.getInvoiceNo());

        TextView tv2 = (TextView) convertView.findViewById(R.id.DateofSoldTV);
        tv2.setText(sched.getDateOfSold());

        TextView tv3 = (TextView) convertView.findViewById(R.id.InvoiceAmountTV);
        tv3.setText(sched.getInvoiceAmount());

        TextView tv4 = (TextView) convertView.findViewById(R.id.StoreNameTV);
        tv4.setText(String.valueOf(sched.getStoreName()));

        ImageView bt = (ImageView) convertView.findViewById(R.id.viewIMG);



        return convertView;
    }


}

