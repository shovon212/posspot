package syntechbd.com.posspot.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import syntechbd.com.posspot.R;
import syntechbd.com.posspot.app.DBHelper;
import syntechbd.com.posspot.modal.CartModal;

public class AddToCartAdapter extends ArrayAdapter<CartModal> implements View.OnClickListener {


    private Context context;
    private ArrayList<CartModal> dataSet ;
    //= new ArrayList<CartModal>();

    private Double est_price, totalTakaInSelectProductRow, defaultVAT, priceAccordingToQuantity, discountInPercent;
    private String totalTakaInSelectProductRowSTRNING = "0";

    private String updatedQty;

    private String newPrice, newDiscount;
    private String ProductCode, ProductName, SelectedBatch, ProductPrice, ProductStock, ProductQty, ProductDiscount, ProductDiscountPromotionID, def_vat_amt, TotalTaka;

    String finalDiscountInTaka = "0";
    private Double qtyCounter;
    private String deFdisCount;
    private String original_product_price;
    private int DeletedPosition = -1;
    private String discount = "0 % 0";
    private String def_vat = "0";
    private String inStock;
    private String oldQty;


    private static class ViewHolder {
        TextView ProductNameTV, ProductsCodeTV, InStockTV, tolatTakaTV;
        Button AddButon;
        ImageView deleteButton;
        EditText batchNoET, qtyET, priceET, vatET, discET;
        // LinearLayout motherOfAddToCart;
    }




    public AddToCartAdapter( Context context, ArrayList<CartModal> data) {
        super(context, R.layout.add_to_cart_row, data);
        this.dataSet = data;
        this.context = context;

    }


    @Override
    public void onClick(View v) {

        final int position = (Integer) v.getTag();
        Object object = getItem(position);
        CartModal dataModel = (CartModal) object;

        switch (v.getId()) {
//            case R.id.addBTN:
//                notifyDataSetChanged();
//                break;
//
////                Snackbar.make(v, "SelectedBatch " +dataSet.get(position).getSelectedBatch(), Snackbar.LENGTH_LONG)
////                        .setAction("No action", null).show();
//
//
//                DBHelper db = new DBHelper(context.getApplicationContext());
//                db.insertAdd_To_Cart(ProductID, ProductCode, ProductName, SelectedBatch, ProductPrice, ProductStock, ProductQty, ProductDiscount, ProductDiscountPromotionID, Vat, TotalTaka);
//                Snackbar.make(v, "Added Product " +dataSet.get(position).getProduct_code()+" Added Batch " +dataSet.get(position).getSelectedBatch(), Snackbar.LENGTH_LONG)
//                        .setAction("No action", null).show();
//
//             //   R.id.motherOfAddToCart.setBackgroundColor(Color.parseColor("#000000"));
//                break;
//            case R.id.batch_ATC_SP:
//
//                Snackbar.make(v, "ProductName " + dataSet.get(position).getTotalTaka(), Snackbar.LENGTH_LONG)
//                        .setAction("No action", null).show();
//                break;

            case R.id.deleteBTN:

                final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(context)
                        .setMessage("Are you sure you want to delete this item?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialogDel, int which) {

                                // Integer index = (Integer) v.getTag();
                                SelectedBatch = dataSet.get(position).getSelectedBatch();
                                dataSet.remove(position);
                                DeletedPosition = position;
                                DBHelper db2 = new DBHelper(context);

                                //Toast.makeText(context, "Found Product Code: " + SelectedBatch + " Position:" + position, Toast.LENGTH_LONG).show();

                                boolean result = db2.deleteAddCategory(SelectedBatch);
                                Log.e("Deleted", String.valueOf(result));

                                dialogDel.dismiss();

                                loadListView();


                            }

                        })

                        .setNegativeButton("No", new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialogdel1, int which) {
                                dialogdel1.dismiss();
                            }
                        });
                dialogDelete.show();

                notifyDataSetChanged();
                break;

        }
    }


//    @Override
//    public int getCount() {
//        // TODO Auto-generated method stub
//        return mlist.size();
//
//    }
//
////    @Override
////    public Object getItem(int position) {
////        // TODO Auto-generated method stub
////        return mlist.get(position);
////    }
//
//    @Override
//    public long getItemId(int position) {
//        // TODO Auto-generated method stub
//        return position;
//    }

    private void loadListView(){
        notifyDataSetInvalidated();
        notifyDataSetChanged();


    }

    private int lastPosition = -1;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final CartModal sched2 = new CartModal();
        //  final CartModal sched = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        final ViewHolder viewHolder; // view lookup cache stored in tag
       // loadListView();
//        DBHelper db = new DBHelper(getContext());
//        dataSet = db.getOverviewList();
        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.add_to_cart_row, parent, false);
            viewHolder.ProductNameTV = (TextView) convertView.findViewById(R.id.product_name_ATC_TV);
            viewHolder.ProductsCodeTV = (TextView) convertView.findViewById(R.id.product_code_ATC_TV);
            viewHolder.InStockTV = (TextView) convertView.findViewById(R.id.stock_ATC_TV);
            viewHolder.tolatTakaTV = (TextView) convertView.findViewById(R.id.total_Taka_ATC_TV);
            viewHolder.deleteButton = (ImageView) convertView.findViewById(R.id.deleteBTN);

            viewHolder.batchNoET = (EditText) convertView.findViewById(R.id.batch_ATC_ET);
            viewHolder.qtyET = (EditText) convertView.findViewById(R.id.qty_ATC_ET);
            viewHolder.priceET = (EditText) convertView.findViewById(R.id.price_ATC_ET);
            viewHolder.discET = (EditText) convertView.findViewById(R.id.discount_ATC_ET);
            viewHolder.vatET = (EditText) convertView.findViewById(R.id.vat_ATC_ET);
            viewHolder.AddButon = (Button) convertView.findViewById(R.id.addBTN);
            // viewHolder.motherOfAddToCart = (LinearLayout) convertView.findViewById(R.id.motherOfAddToCart);


            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

//        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.down_from_top : R.anim.up_from_bottom);
//        result.startAnimation(animation);
        lastPosition = position;


        viewHolder.AddButon.setTag(position);
        viewHolder.deleteButton.setTag(position);
//        viewHolder.spinner.setOnClickListener(this);
        //viewHolder.AddButon.setOnClickListener(this);
        viewHolder.deleteButton.setOnClickListener(this);

        try {

            Log.e("DeletedPosition", String.valueOf(DeletedPosition));
            Log.e("Current position", String.valueOf(position));
            if(!dataSet.isEmpty()) {

                discount = dataSet.get(position).getDefaultDiscount();
                //discount = "3 % 1";
                deFdisCount = discount;
                Log.e("Adptr position", String.valueOf(position));
                def_vat = dataSet.get(position).getDeFaUlT_VAT();
                Log.e("Adptr vat", def_vat);
                qtyCounter = Double.valueOf(dataSet.get(position).getProductQty());
                ProductPrice = dataSet.get(position).getProductPrice();

                ProductCode = dataSet.get(position).getProductCode();
                ProductName = dataSet.get(position).getProductName();
                SelectedBatch = dataSet.get(position).getSelectedBatch();
                ProductStock = dataSet.get(position).getProductStock();
                ProductQty = dataSet.get(position).getProductQty();
                ProductDiscount = dataSet.get(position).getProductDiscount();

                findProductDiscountANdID(deFdisCount);

                defaultVAT = Double.valueOf(def_vat) / 100;
                est_price = Double.valueOf(ProductPrice) - Double.valueOf(ProductDiscount);
                def_vat_amt = String.valueOf(defaultVAT * est_price);
//
                Double totalTakaInSelectProductRow = est_price + Double.valueOf(def_vat_amt);
                totalTakaInSelectProductRowSTRNING = new DecimalFormat("##.##").format(totalTakaInSelectProductRow);
                TotalTaka = String.valueOf(totalTakaInSelectProductRowSTRNING);

                viewHolder.vatET.setText(def_vat_amt);
                viewHolder.discET.setText(ProductDiscount);

            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }


        viewHolder.qtyET.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

                if(Double.valueOf(inStock)>=Double.valueOf(updatedQty)) {
                    viewHolder.tolatTakaTV.setText(totalTakaInSelectProductRowSTRNING);
                    viewHolder.vatET.setText(def_vat_amt);
                    viewHolder.discET.setText(ProductDiscount);
                    //Updating information holder for DB
                    ProductQty = updatedQty;
                    //ProductStock = dataSet.get(position).getProductStock();
                    TotalTaka = totalTakaInSelectProductRowSTRNING;

                    sched2.setProductName(ProductName);
                    sched2.setProductCode(ProductCode);
                    sched2.setProductStock(ProductStock);
                    sched2.setTotalTaka(TotalTaka);
                    sched2.setSelectedBatch(SelectedBatch);
                    sched2.setProductQty(ProductQty);
                    sched2.setProductPrice(newPrice);
                    sched2.setProductDiscount(ProductDiscount);
                    sched2.setVat(def_vat_amt);


                    sched2.setOriginal_product_price(original_product_price);

                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {
                            //TODO your background code

                            DBHelper db = new DBHelper(context);
                            db.updateFromAdapter(SelectedBatch, newPrice, ProductQty, def_vat_amt, ProductDiscount, TotalTaka, original_product_price);
                        }
                    });

                     inStock="-1";

                 }else {

                    viewHolder.tolatTakaTV.setText("0");

                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {
                            //TODO your background code

                            DBHelper db = new DBHelper(context);
                            db.updateFromAdapter(SelectedBatch, newPrice, "0", def_vat_amt, ProductDiscount, "0", original_product_price);
                        }
                    });

                    updatedQty = "0";
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // inStock = dataSet.get(position).getProductStock();
                 inStock = viewHolder.InStockTV.getText().toString().trim();
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                newPrice = viewHolder.priceET.getText().toString().trim();
                //newDiscount = viewHolder.discET.getText().toString().trim();
                oldQty = viewHolder.qtyET.getText().toString().trim();
                updatedQty = viewHolder.qtyET.getText().toString().trim();
                 //inStock = viewHolder.InStockTV.getText().toString().trim();

                SelectedBatch = viewHolder.batchNoET.getText().toString().trim();
                if (updatedQty.length() == 0) {
                    updatedQty = "0";
                    viewHolder.discET.setText("0");
                }
                if (newPrice.length() == 0) {
                    newPrice = "0";
                }

//                if (newDiscount.length() == 0) {
//                    newDiscount = "0";
//                }

                 if(Double.valueOf(inStock)>=Double.valueOf(updatedQty)) {

                    findProductDiscountWhileEditing(deFdisCount, updatedQty, newPrice);
                    original_product_price = String.valueOf(Double.valueOf(newPrice) * Double.valueOf(updatedQty));
                    priceAccordingToQuantity = Double.valueOf(newPrice) * Double.valueOf(updatedQty) - Double.valueOf(ProductDiscount);
                    def_vat_amt = String.valueOf(defaultVAT * Double.valueOf(priceAccordingToQuantity));

                    totalTakaInSelectProductRow = Double.valueOf(priceAccordingToQuantity) + Double.valueOf(def_vat_amt);
                    totalTakaInSelectProductRowSTRNING = new DecimalFormat("##.##").format(totalTakaInSelectProductRow);
                } else if(inStock.equals("-1")){

                } else {
                   // viewHolder.qtyET.removeTextChangedListener(qtyTextWatcher);

                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                            alertDialog.setTitle("Stock Out!");
                            alertDialog.setMessage("Available product: "+inStock);
//                            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int which) {
//
//                                            dialog.dismiss();
//
//
//                                        }
//                                    });
                            alertDialog.show();





                }
            }
        });


        viewHolder.priceET.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                viewHolder.tolatTakaTV.setText(totalTakaInSelectProductRowSTRNING);
                viewHolder.vatET.setText(def_vat_amt);
                viewHolder.discET.setText(ProductDiscount);
                //Updating information holder for DB
                ProductQty = updatedQty;
               // ProductStock = dataSet.get(position).getProductStock();
                TotalTaka = totalTakaInSelectProductRowSTRNING;

                sched2.setProductName(ProductName);
                sched2.setProductCode(ProductCode);
                sched2.setProductStock(ProductStock);
                sched2.setTotalTaka(TotalTaka);
                sched2.setSelectedBatch(SelectedBatch);
                sched2.setProductQty(ProductQty);
                sched2.setProductPrice(newPrice);
                sched2.setProductDiscount(ProductDiscount);
                sched2.setVat(def_vat_amt);
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        //TODO your background code

                        DBHelper db = new DBHelper(context);
                        db.updateFromAdapter(SelectedBatch, newPrice, ProductQty,def_vat_amt,ProductDiscount,TotalTaka,original_product_price);
                    }
                });


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                newPrice = viewHolder.priceET.getText().toString().trim();
                //newDiscount = viewHolder.discET.getText().toString().trim();
                updatedQty = viewHolder.qtyET.getText().toString().trim();
                SelectedBatch = viewHolder.batchNoET.getText().toString().trim();
                if (updatedQty.length() == 0) {
                    updatedQty = "0";
                    viewHolder.discET.setText("0");
                }
                if (newPrice.length() == 0) {
                    newPrice = "0";
                    viewHolder.discET.setText("0");
                }
//                if (newDiscount.length() == 0) {
//                    newDiscount = "0";
//                }

                findProductDiscountWhileEditing(deFdisCount,updatedQty,newPrice);

                original_product_price = String.valueOf(Double.valueOf(newPrice) * Double.valueOf(updatedQty));
                priceAccordingToQuantity = Double.valueOf(newPrice) * Double.valueOf(updatedQty) - Double.valueOf(ProductDiscount);
                def_vat_amt = String.valueOf(defaultVAT * Double.valueOf(priceAccordingToQuantity));

                totalTakaInSelectProductRow = Double.valueOf(priceAccordingToQuantity) + Double.valueOf(def_vat_amt);
                totalTakaInSelectProductRowSTRNING = new DecimalFormat("##.##").format(totalTakaInSelectProductRow);
            }
        });

        viewHolder.discET.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                viewHolder.tolatTakaTV.setText(totalTakaInSelectProductRowSTRNING);
                viewHolder.vatET.setText(def_vat_amt);
                //viewHolder.discET.setText(ProductDiscount);
                //Updating information holder for DB
                ProductQty = updatedQty;
                TotalTaka = totalTakaInSelectProductRowSTRNING;
                //ProductStock = dataSet.get(position).getProductStock();

                sched2.setProductName(ProductName);
                sched2.setProductCode(ProductCode);
                sched2.setProductStock(ProductStock);
                sched2.setTotalTaka(TotalTaka);
                sched2.setSelectedBatch(SelectedBatch);
                sched2.setProductQty(ProductQty);
                sched2.setProductPrice(newPrice);
                sched2.setProductDiscount(ProductDiscount);
                sched2.setVat(def_vat_amt);

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        //TODO your background code

                        DBHelper db = new DBHelper(context);
                        db.updateFromAdapter(SelectedBatch, newPrice, ProductQty,def_vat_amt,ProductDiscount,TotalTaka,original_product_price);
                    }
                });

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                newPrice = viewHolder.priceET.getText().toString().trim();
                newDiscount = viewHolder.discET.getText().toString().trim();
                updatedQty = viewHolder.qtyET.getText().toString().trim();
                SelectedBatch = viewHolder.batchNoET.getText().toString().trim();
                if (updatedQty.length() == 0) {
                    updatedQty = "0";
                }
                if (newPrice.length() == 0) {
                    newPrice = "0";
                }
                if (newDiscount.length() == 0) {
                    newDiscount = "0";
                }

                priceAccordingToQuantity = Double.valueOf(newPrice) * Double.valueOf(updatedQty) - Double.valueOf(newDiscount);
                original_product_price = String.valueOf(Double.valueOf(newPrice) * Double.valueOf(updatedQty));
                def_vat_amt = String.valueOf(defaultVAT * priceAccordingToQuantity);

                totalTakaInSelectProductRow = priceAccordingToQuantity + Double.valueOf(def_vat_amt);
                totalTakaInSelectProductRowSTRNING = new DecimalFormat("##.##").format(totalTakaInSelectProductRow);
            }
        });


        if(!dataSet.isEmpty()) {

            viewHolder.tolatTakaTV.setText(totalTakaInSelectProductRowSTRNING);
            viewHolder.ProductNameTV.setText(dataSet.get(position).getProductName());
            viewHolder.qtyET.setText(dataSet.get(position).getProductQty());
            viewHolder.discET.setText(dataSet.get(position).getProductDiscount());
            viewHolder.ProductsCodeTV.setText(dataSet.get(position).getProductCode());

            viewHolder.InStockTV.setText(dataSet.get(position).getProductStock());
            viewHolder.batchNoET.setText(dataSet.get(position).getSelectedBatch());
            viewHolder.priceET.setText(dataSet.get(position).getProductPrice());

        }
//        viewHolder.deleteButton.setOnClickListener(
//                new Button.OnClickListener() {
//
//
//                    @Override
//                    public void onClick(View v) {
//                        Integer index = (Integer) v.getTag();
//                        SelectedBatch = dataSet.get(position).getSelectedBatch();
//                        dataSet.remove(index.intValue());
//
//                        DBHelper db2 = new DBHelper(context);
//
//                        Toast.makeText(context,"Found Batch: "+SelectedBatch,Toast.LENGTH_LONG).show();
//                        db2.deleteAddedProduct(SelectedBatch);
//                        notifyDataSetChanged();
//                    }
//                }
//
//        );


        //  db.insertAdd_To_Cart(ProductID, ProductCode, ProductName, SelectedBatch, ProductPrice, ProductStock, ProductQty, ProductDiscount, ProductDiscountPromotionID, Vat, TotalTaka);


        //refresh(dataSet);
        return convertView;
    }

    private void findProductDiscountANdID( String deFdisCount) {

        if (!deFdisCount.equals("0")) {


            String[] splited = deFdisCount.split("\\s+");

            String split_one = splited[0];
            String split_second = splited[1];
            String split_three = splited[2];


            if (split_second.equals("%")) {

                //Toast.makeText(context, "Got % :" + split_second, Toast.LENGTH_LONG).show();
                discountInPercent = Double.valueOf(split_one.trim()) / 100;
                discount = String.valueOf(discountInPercent);
                ProductDiscountPromotionID = split_three.trim();
//                Log.i("Product Price", ProductPrice);
                //  Toast.makeText(context,"Product Price: "+ ProductPrice, Toast.LENGTH_LONG).show();
                Double discoun = qtyCounter * Double.valueOf(ProductPrice) * discountInPercent;
                finalDiscountInTaka = String.valueOf(discoun);

                //  Toast.makeText(context, "Discount :" + discount + ",PromotionID :" + ProductDiscountPromotionID, Toast.LENGTH_LONG).show();
            }
            if (split_second.equals("TK")) {


                Toast.makeText(context, "Got TAKA :" + split_second, Toast.LENGTH_LONG).show();
                discount = split_one.trim();
                ProductDiscountPromotionID = split_three.trim();
                finalDiscountInTaka = discount ;
                //  Toast.makeText(context, "Discount :" + discount + "Takka,PromotionID :" + ProductDiscountPromotionID, Toast.LENGTH_LONG).show();
            }

            ProductDiscount = finalDiscountInTaka;


        } else {

            ProductDiscount = discount ;
            ProductDiscountPromotionID = "null";
        }

    }

    private void findProductDiscountWhileEditing( String deFdisCount, String qtyCounter, String ProductPrice) {

        if (!deFdisCount.equals("0")) {


            String[] splited = deFdisCount.split("\\s+");

            String split_one = splited[0];
            String split_second = splited[1];
            String split_three = splited[2];


            if (split_second.equals("%")) {

                //Toast.makeText(context, "Got % :" + split_second, Toast.LENGTH_LONG).show();
                discountInPercent = Double.valueOf(split_one.trim()) / 100;
                discount = String.valueOf(discountInPercent);
                ProductDiscountPromotionID = split_three.trim();
//                Log.i("Product Price", ProductPrice);
                //  Toast.makeText(context,"Product Price: "+ ProductPrice, Toast.LENGTH_LONG).show();
                Double discoun = Double.valueOf(qtyCounter) * Double.valueOf(ProductPrice) * discountInPercent;
                finalDiscountInTaka = String.valueOf(discoun);

                //  Toast.makeText(context, "Discount :" + discount + ",PromotionID :" + ProductDiscountPromotionID, Toast.LENGTH_LONG).show();
            }
            if (split_second.equals("TK")) {


                Toast.makeText(context, "Got TAKA :" + split_second, Toast.LENGTH_LONG).show();
                discount = split_one.trim();
                ProductDiscountPromotionID = split_three.trim();
                finalDiscountInTaka = discount ;
                //  Toast.makeText(context, "Discount :" + discount + "Takka,PromotionID :" + ProductDiscountPromotionID, Toast.LENGTH_LONG).show();
            }

            ProductDiscount = finalDiscountInTaka;


        } else {

            ProductDiscount = discount ;
            ProductDiscountPromotionID = "null";
        }

    }

    // public static boolean isRecursionEnable = true;

    void runInBackground() {
//        if (!isRecursionEnable)
//            return;    // Handle not to start multiple parallel threads

        // isRecursionEnable = false; when u want to stop
        // on exception on thread make it true again
        new Thread(new Runnable() {
            @Override
            public void run() {


                // DO your work here
                // get the data
//                if (activity_is_not_in_background) {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            //uddate UI
//                            runInBackground();
//                        }
//                    });
//                } else {
//                    runInBackground();
//                }
            }
        }).start();
    }

    public void refresh(ArrayList<CartModal> dataSet)
    {
        this.dataSet = dataSet;
        notifyDataSetChanged();
    }

//    private class AsyncCaller extends AsyncTask<Void, Void, Void>
//    {
//        //ProgressDialog pdLoading = new ProgressDialog(AsyncExample.this);
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//            //this method will be running on UI thread
////            pdLoading.setMessage("\tLoading...");
////            pdLoading.show();
//        }
//        @Override
//        protected Void doInBackground(Void... params) {
//
//            //  db.updateFromAdapter(SelectedBatch, ProductPrice, ProductStock, ProductQty,Vat,ProductDiscount,TotalTaka);
//            //this method will be running on background thread so don't update UI frome here
//            //do your long running http tasks here,you dont want to pass argument and u can access the parent class' variable url over here
//
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//
//            refresh(dataSet);
//
//            //this method will be running on UI thread
//
//            //pdLoading.dismiss();
//        }
//
//    }


}