package syntechbd.com.posspot.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import syntechbd.com.posspot.AFragment;
import syntechbd.com.posspot.CustomerView;
import syntechbd.com.posspot.FragmentDiscount;
import syntechbd.com.posspot.FragmentPayment;
import syntechbd.com.posspot.FragmentSelectCustomer;
import syntechbd.com.posspot.FragmentSelectProduct;
import syntechbd.com.posspot.SalesReportFragment;

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public SimpleFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    // This determines the fragment for each tab

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new FragmentSelectProduct();
        } else if (position == 1){
            return new FragmentSelectCustomer();
        } else if (position == 2){
            return new FragmentDiscount();
        } else  {
            return new FragmentPayment();
        }
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 4;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Product";
            case 1:
                return "Customer";
            case 2:
                return "Discount";
            case 3:
                return "Payment";
            default:
                return null;
        }
    }

}