package syntechbd.com.posspot.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import syntechbd.com.posspot.R;
import syntechbd.com.posspot.SupplierDetailsActivity;
import syntechbd.com.posspot.modal.SaleInvoiceModal;
import syntechbd.com.posspot.modal.SuppliersModal;


public class SupplierViewsAdapter extends BaseAdapter{
//implements Filterable {
    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<SuppliersModal> mlist ;
    public ArrayList<SuppliersModal> mStringFilterList ;
   // private ValueFilter valueFilter;
    //StockProduct sched;

    private Context context;




    public SupplierViewsAdapter(Activity activity, ArrayList<SuppliersModal> mlist) {
        this.activity = activity;
        this.mlist = mlist;
        this.mStringFilterList = mlist;

    }




    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mlist.size();
        //return (mlist.size() <1) ? 0 : mlist.size();
//        if(mlist != null || mlist.size() > 0) {
//            return mlist.size();
//        }else return 0;
  //      Log.e("mListSize:", String.valueOf(mlist.size()));
 //       return (mlist == null) ? 0 : mlist.size();

//        if (mlist.isEmpty()) {
//            return 0;
//        } else return mlist.size();

    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        context = parent.getContext();

//        sched=mlist.get(position);
//        LayoutInflater inflater=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        convertView=inflater.inflate(R.layout.stock_product_layout,parent,false);


        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.suppliers_row_layout, null);


        // getting movie data for the row
        SuppliersModal sched = mlist.get(position);

        TextView tv1 = (TextView) convertView.findViewById(R.id.NameSupTV);
        tv1.setText(sched.getSupplier_name());

        TextView tv2 = (TextView) convertView.findViewById(R.id.PhoneSupTV);
        tv2.setText(sched.getPhone());

        TextView tv3 = (TextView) convertView.findViewById(R.id.EmailSupTV);
        tv3.setText(sched.getEmail());

        TextView tv4 = (TextView) convertView.findViewById(R.id.DuesSupTV);
        tv4.setText(String.valueOf(sched.getBalance()));

        ImageView bt = (ImageView) convertView.findViewById(R.id.viewIMGSup);

//        bt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                SuppliersModal cutomers = new SuppliersModal();
//                Intent intent = new Intent(context, SupplierDetailsActivity.class);
//                intent.putExtra("id_supplier", cutomers.getId_supplier());
//                intent.putExtra("supplier_code", cutomers.getSupplier_code());
//
//                intent.putExtra("supplier_name", cutomers.getSupplier_name());
//                intent.putExtra("contact_person", cutomers.getContact_person());
//                intent.putExtra("phone", cutomers.getPhone());
//                intent.putExtra("email", cutomers.getEmail());
//                intent.putExtra("div_id", cutomers.getDiv_id());
//                intent.putExtra("dist_id", cutomers.getDist_id());
//                intent.putExtra("upz_id", cutomers.getUpz_id());
//                intent.putExtra("unn_id", cutomers.getUnn_id());
//
//                intent.putExtra("city_id", cutomers.getCity_id());
//                intent.putExtra("area_id", cutomers.getArea_id());
//                intent.putExtra("profile_img", cutomers.getProfile_img());
//                intent.putExtra("post_code", cutomers.getPost_code());
//                intent.putExtra("vat_reg_no", cutomers.getVat_reg_no());
//                intent.putExtra("note", cutomers.getNote());
//                intent.putExtra("addr_line_1", cutomers.getAddr_line_1());
//                intent.putExtra("balance", cutomers.getBalance());
//                intent.putExtra("dtt_add", cutomers.getDtt_add());
//                intent.putExtra("uid_add", cutomers.getUid_add());
//                intent.putExtra("dtt_mod", cutomers.getDtt_mod());
//                intent.putExtra("uid_mod", cutomers.getUid_mod());
//                intent.putExtra("version", cutomers.getVersion());
//                intent.putExtra("stores", cutomers.getStores());
//
//              //  Toast.makeText(context,cutomers.getSupplier_name(),Toast.LENGTH_LONG).show();
//                context.startActivity(intent);
//
//
//
//
//
//            }
//        });



        return convertView;
    }


}

