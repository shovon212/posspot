package syntechbd.com.posspot.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import syntechbd.com.posspot.R;
import syntechbd.com.posspot.modal.StockInModal;

public class StockInAdapter extends BaseAdapter {


    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<StockInModal> mlist;
    public StockInAdapter(Activity activity, ArrayList<StockInModal> mlist )
    {
        this.activity = activity;
        this.mlist=mlist;

    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int i) {
        return mlist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
            view = inflater.inflate(R.layout.stock_in_product_layout, null);


        // getting movie data for the row
        StockInModal sched = mlist.get(i);

        TextView tv1=(TextView)view.findViewById(R.id.dateTV);
        tv1.setText(sched.getDate());

        TextView tv2=(TextView)view.findViewById(R.id.product_codeTV);
        tv2.setText(sched.getProductCode());

        TextView tv3=(TextView)view.findViewById(R.id.product_nameTV);
        tv3.setText(sched.getProductName());

//        TextView tv4 = (TextView) view.findViewById(R.id.SupplierTV);
//        tv4.setText(sched.getStoreId());

        TextView tv5 = (TextView) view.findViewById(R.id.BatchNoTV);
        tv5.setText(sched.getBatchNo());

        TextView tv6 = (TextView) view.findViewById(R.id.product_qntTV);
        String Qty = sched.getProductQty();
        tv6.setText(Qty.substring(0, Qty.length() - 2));


        return view;
    }
}
