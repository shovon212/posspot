package syntechbd.com.posspot.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import syntechbd.com.posspot.R;
import syntechbd.com.posspot.modal.Cutomers;


public class CustomerViewAdapter extends BaseAdapter implements Filterable {

    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<Cutomers> mlist ;
    private ArrayList<Cutomers> mStringFilterList ;
    private ValueFilter valueFilter;
    //StockProduct sched;

    public CustomerViewAdapter(Activity activity, ArrayList<Cutomers> mlist) {
        this.activity = activity;
        this.mlist = mlist;
        this.mStringFilterList = mlist;

    }




    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        // return mlist.size();
        //return (mlist.size() <1) ? 0 : mlist.size();
//        if(mlist != null || mlist.size() > 0) {
//            return mlist.size();
//        }else return 0;
        Log.e("mListSize:", String.valueOf(mlist.size()));
        return (mlist == null) ? 0 : mlist.size();

//        if (mlist.isEmpty()) {
//            return 0;
//        } else return mlist.size();

    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

//        sched=mlist.get(position);
//        LayoutInflater inflater=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        convertView=inflater.inflate(R.layout.stock_product_layout,parent,false);


        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.customers_row_layout, null);


        // getting movie data for the row
        Cutomers sched = mlist.get(position);

        TextView tv1 = (TextView) convertView.findViewById(R.id.customer_CodeTV);
        tv1.setText(sched.getCustomer_code());

        TextView tv2 = (TextView) convertView.findViewById(R.id.customer_NameTV);
        tv2.setText(sched.getFull_name());

        TextView tv3 = (TextView) convertView.findViewById(R.id.customer_MobileTV);
        tv3.setText(sched.getPhone());

        TextView tv4 = (TextView) convertView.findViewById(R.id.customers_BalanceTV);
        tv4.setText(String.valueOf(sched.getBalance()));


        return convertView;
    }


    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

//            if (constraint != null && constraint.length() > 0) {
//                List<Cutomers> filterList = new ArrayList<Cutomers>();
//                for (int i = 0; i < mStringFilterList.size(); i++) {
//                    if ( (mStringFilterList.get(i).getFull_name().toUpperCase() )
//                            .contains(constraint.toString().toUpperCase())) {
//
//                        Cutomers country = new Cutomers(mStringFilterList.get(i)
//                                .getCustomer_code() ,  mStringFilterList.get(i)
//                                .getFull_name() ,  mStringFilterList.get(i)
//                                .getPhone(),mStringFilterList.get(i)
//                                .getBalance());
//
//                        filterList.add(country);
//                    }
//                }
//                results.count = filterList.size();
//                results.values = filterList;
//            }

            if (constraint != null && constraint.length() > -1) {
                List<Cutomers> filterList = new ArrayList<Cutomers>();
                for (int i = 0; i < mStringFilterList.size(); i++) {
                    if ((mStringFilterList.get(i).getFull_name().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {

                        Cutomers country = new Cutomers(mStringFilterList.get(i)
                                .getId_customer(), mStringFilterList.get(i)
                                .getCustomer_code(), mStringFilterList.get(i)
                                .getFull_name(), mStringFilterList.get(i)
                                .getPhone(), mStringFilterList.get(i)
                                .getBalance(), mStringFilterList.get(i)
                                .getPoints(), mStringFilterList.get(i)
                                .getCustomer_type_name(), mStringFilterList.get(i)
                                .getGender(), mStringFilterList.get(i)
                                .getEmail(), mStringFilterList.get(i)
                                .getBirth_date(), mStringFilterList.get(i)
                                .getMarital_status(), mStringFilterList.get(i)
                                .getAnniversary_date(), mStringFilterList.get(i)
                                .getProfile_img());

                        filterList.add(country);

                    } else if ((mStringFilterList.get(i).getPhone().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {

                        Cutomers country2 = new Cutomers(mStringFilterList.get(i)
                                .getId_customer(), mStringFilterList.get(i)
                                .getCustomer_code(), mStringFilterList.get(i)
                                .getFull_name(), mStringFilterList.get(i)
                                .getPhone(), mStringFilterList.get(i)
                                .getBalance(), mStringFilterList.get(i)
                                .getPoints(), mStringFilterList.get(i)
                                .getCustomer_type_name(), mStringFilterList.get(i)
                                .getGender(), mStringFilterList.get(i)
                                .getEmail(), mStringFilterList.get(i)
                                .getBirth_date(), mStringFilterList.get(i)
                                .getMarital_status(), mStringFilterList.get(i)
                                .getAnniversary_date(), mStringFilterList.get(i)
                                .getProfile_img());

                        filterList.add(country2);
                    } else if ((mStringFilterList.get(i).getCustomer_type_name().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {

                        Cutomers country2 = new Cutomers(mStringFilterList.get(i)
                                .getId_customer(), mStringFilterList.get(i)
                                .getCustomer_code(), mStringFilterList.get(i)
                                .getFull_name(), mStringFilterList.get(i)
                                .getPhone(), mStringFilterList.get(i)
                                .getBalance(), mStringFilterList.get(i)
                                .getPoints(), mStringFilterList.get(i)
                                .getCustomer_type_name(), mStringFilterList.get(i)
                                .getGender(), mStringFilterList.get(i)
                                .getEmail(), mStringFilterList.get(i)
                                .getBirth_date(), mStringFilterList.get(i)
                                .getMarital_status(), mStringFilterList.get(i)
                                .getAnniversary_date(), mStringFilterList.get(i)
                                .getProfile_img());

                        filterList.add(country2);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            mlist = (ArrayList<Cutomers>) results.values;
           notifyDataSetChanged();
        }


    }

}

