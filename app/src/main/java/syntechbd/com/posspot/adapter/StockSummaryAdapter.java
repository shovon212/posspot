package syntechbd.com.posspot.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import syntechbd.com.posspot.R;
import syntechbd.com.posspot.modal.Cutomers;
import syntechbd.com.posspot.modal.StockProduct;


public class StockSummaryAdapter extends BaseAdapter implements Filterable {

    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<StockProduct> mlist;
    private ArrayList<StockProduct> mStringFilterList ;
    private ValueFilter valueFilter;
    //StockProduct sched;

    public StockSummaryAdapter(Activity activity, ArrayList<StockProduct> mlist )
    {
        this.activity = activity;
        this.mlist=mlist;
        this.mStringFilterList = mlist;

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

//        sched=mlist.get(position);
//        LayoutInflater inflater=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        convertView=inflater.inflate(R.layout.stock_product_layout,parent,false);

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.stock_product_layout, null);


        // getting movie data for the row
        StockProduct sched = mlist.get(position);

        TextView tv1=(TextView)convertView.findViewById(R.id.product_nameTV);
        tv1.setText(sched.getProductName());

        TextView tv2=(TextView)convertView.findViewById(R.id.product_codeTV);
        tv2.setText(sched.getProductCode());

        TextView tv3=(TextView)convertView.findViewById(R.id.product_qntTV);
        tv3.setText(sched.getProductQty());


        return convertView;
    }



    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

//            if (constraint != null && constraint.length() > 0) {
//                List<Cutomers> filterList = new ArrayList<Cutomers>();
//                for (int i = 0; i < mStringFilterList.size(); i++) {
//                    if ( (mStringFilterList.get(i).getFull_name().toUpperCase() )
//                            .contains(constraint.toString().toUpperCase())) {
//
//                        Cutomers country = new Cutomers(mStringFilterList.get(i)
//                                .getCustomer_code() ,  mStringFilterList.get(i)
//                                .getFull_name() ,  mStringFilterList.get(i)
//                                .getPhone(),mStringFilterList.get(i)
//                                .getBalance());
//
//                        filterList.add(country);
//                    }
//                }
//                results.count = filterList.size();
//                results.values = filterList;
//            }

            if (constraint != null && constraint.length() > -1) {
                List<StockProduct> filterList = new ArrayList<StockProduct>();
                for (int i = 0; i < mStringFilterList.size(); i++) {
                    if ((mStringFilterList.get(i).getProductName().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {

                        StockProduct country = new StockProduct(mStringFilterList.get(i)
                                .getProductName(), mStringFilterList.get(i)
                                .getProductCode(), mStringFilterList.get(i)
                                .getProductQty());

                        filterList.add(country);

                    } else if ((mStringFilterList.get(i).getProductCode().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {

                        StockProduct country2 = new StockProduct(mStringFilterList.get(i)
                                .getProductName(), mStringFilterList.get(i)
                                .getProductCode(), mStringFilterList.get(i)
                                .getProductQty());

                        filterList.add(country2);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            mlist = (ArrayList<StockProduct>) results.values;
            notifyDataSetChanged();
        }


    }

}


