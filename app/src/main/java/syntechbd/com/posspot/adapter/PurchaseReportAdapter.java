package syntechbd.com.posspot.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import syntechbd.com.posspot.R;
import syntechbd.com.posspot.modal.PurchaseReportModal;


public class PurchaseReportAdapter extends BaseAdapter{
//implements Filterable {
    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<PurchaseReportModal> mlist ;
    public ArrayList<PurchaseReportModal> mStringFilterList ;
   // private ValueFilter valueFilter;
    //StockProduct sched;

    public PurchaseReportAdapter(Activity activity, ArrayList<PurchaseReportModal> mlist) {
        this.activity = activity;
        this.mlist = mlist;
        this.mStringFilterList = mlist;

    }




    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mlist.size();
        //return (mlist.size() <1) ? 0 : mlist.size();
//        if(mlist != null || mlist.size() > 0) {
//            return mlist.size();
//        }else return 0;
  //      Log.e("mListSize:", String.valueOf(mlist.size()));
 //       return (mlist == null) ? 0 : mlist.size();

//        if (mlist.isEmpty()) {
//            return 0;
//        } else return mlist.size();

    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

//        sched=mlist.get(position);
//        LayoutInflater inflater=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        convertView=inflater.inflate(R.layout.stock_product_layout,parent,false);


        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.purchase_report_row_layout, null);


        // getting movie data for the row
        PurchaseReportModal sched = mlist.get(position);

        TextView tv1 = (TextView) convertView.findViewById(R.id.Purchase_DateTV);
        tv1.setText(sched.getDateOfSold());

        TextView tv2 = (TextView) convertView.findViewById(R.id.Product_NameTV);
        tv2.setText(sched.getProductName());

        TextView tv4 = (TextView) convertView.findViewById(R.id.Store_NameTV);
        tv4.setText(String.valueOf(sched.getStoreName()));


        TextView tv3 = (TextView) convertView.findViewById(R.id.Supplier_NameTV);
        tv3.setText(sched.getSupplierName());

        TextView tv43 = (TextView) convertView.findViewById(R.id.Purchase_PriceTV);
        tv43.setText(sched.getPurchasePrice());

        TextView tv5 = (TextView) convertView.findViewById(R.id.Qty_TV);
        tv5.setText(sched.getQty());




        return convertView;
    }



}

