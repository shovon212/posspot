package syntechbd.com.posspot;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import syntechbd.com.posspot.adapter.SimpleFragmentPagerAdapter;
import syntechbd.com.posspot.app.DBHelper;

import static syntechbd.com.posspot.FragmentDiscount.GRAND_TOTAL;
import static syntechbd.com.posspot.FragmentSelectCustomer.CUSTOMER_ID;
import static syntechbd.com.posspot.app.DBHelper.DATABASE_NAME;

public class SaleNowActivity extends AppCompatActivity {




    private ViewPager mViewPager;
    SharedPreferences prefCUSTOMER;
    SharedPreferences.Editor editor;
    Button NextButtion,PrevButtion;
    private int Tabposition=0;
    DBHelper db;
    public static String cart_total;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_now);

        db = new DBHelper(getApplicationContext());
        prefCUSTOMER = getApplicationContext().getSharedPreferences("CustomerInfo", 0);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        final ViewPager viewPager = (ViewPager) findViewById(R.id.container);
        NextButtion = (Button)  findViewById(R.id.nextBTN);
        PrevButtion = (Button)  findViewById(R.id.prevBTN);
        viewPager.setOffscreenPageLimit(1);

        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (viewPager.getCurrentItem() == 0) {
                    viewPager.setCurrentItem(-1, false);
                    return true;
                }
                else if (viewPager.getCurrentItem() == 1) {
                    viewPager.setCurrentItem(1, false);
                    return true;
                }
                else if (viewPager.getCurrentItem() == 2) {
                    viewPager.setCurrentItem(2, false);
                    return true;
                }
                return true;
            }
        });



        SimpleFragmentPagerAdapter adapter = new SimpleFragmentPagerAdapter(this, getSupportFragmentManager());



        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        LinearLayout tabStrip = ((LinearLayout)tabLayout.getChildAt(0));

        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Tabposition = tab.getPosition();

                if(Tabposition==3){
                    PrevButtion.setVisibility(View.GONE);
                    NextButtion.setVisibility(View.GONE);
                }if(Tabposition==0){
                    PrevButtion.setVisibility(View.GONE);
                    NextButtion.setVisibility(View.VISIBLE);
                }
                if(Tabposition<1 || Tabposition>2){
                    PrevButtion.setVisibility(View.GONE);

                }
                if(Tabposition<1 || Tabposition>2){
                    PrevButtion.setVisibility(View.GONE);

                } else {
                    PrevButtion.setVisibility(View.VISIBLE);
                    NextButtion.setVisibility(View.VISIBLE);
                }

//                if(Tabposition==2){
//
//                    db.updateinsertCustomerInfoGrandTotal("1",GRAND_TOTAL);
//                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        tabLayout.setupWithViewPager(viewPager);

        NextButtion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Tabposition==0){

                    cart_total = db.getTotalAmountInCart();

                    int totalRow = db.numberOfRowsAdd();
                    if(totalRow<1){

                        cart_total = "0";

                        Snackbar.make(v, "You need to add product to proceed !", Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();

                        viewPager.setCurrentItem(0);

//                        final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(getApplicationContext())
//                                .setMessage("You need to add product to proceed !")
//                                .setPositiveButton("Add", new DialogInterface.OnClickListener(){
//                                    @Override
//                                    public void onClick(DialogInterface dialogDel, int which) {
//
//                                        viewPager.setCurrentItem(0);
//                                        dialogDel.dismiss();
//
//                                    }
//
//                                })
//
//                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
//                                    @Override
//                                    public void onClick(DialogInterface dialogdel1, int which) {
//                                        viewPager.setCurrentItem(0);
//                                        dialogdel1.dismiss();
//                                    }
//                                });
//                        dialogDelete.show();
                    }else {
                        cart_total = db.getTotalAmountInCart();
                        Tabposition++;
                        viewPager.setCurrentItem(Tabposition);
                    }

                }else {

                    Tabposition++;
                    viewPager.setCurrentItem(Tabposition);
                }
//                if (Tabposition == 2) {
//
//                    db.updateinsertCustomerInfoGrandTotal("1", GRAND_TOTAL);
//                }







            }
        });

        PrevButtion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                cart_total = db.getTotalAmountInCart();
//                Log.e("cart_total", cart_total);
                if (Tabposition != 0) {
                    Tabposition--;
                    viewPager.setCurrentItem(Tabposition);
                    ;
                }
            }
        });





    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sale_now, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        DBHelper db = new DBHelper(getApplicationContext());
        db.delete();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();


//        editor = prefCUSTOMER.edit();
//        editor.clear();
//        editor.apply();


    }
}
