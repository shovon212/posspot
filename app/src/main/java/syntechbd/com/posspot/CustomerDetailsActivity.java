package syntechbd.com.posspot;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import syntechbd.com.posspot.modal.Cutomers;

public class CustomerDetailsActivity extends AppCompatActivity {

    TextView CustomerCode,FullNameT,CustomerTypeT,EmailT,PhoneT,GenderT,DateOfBirthT,MaritalStatusT,AnniversaryDateT, BalanceT, PointsT;

    ImageView dp;

    SharedPreferences pref;
    String YouruserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_details);
        if (!isNetworkConnected()){
            Toast.makeText(this,"Connection Lost !! Check your internet connection! ",Toast.LENGTH_LONG).show();
        }

        pref = getSharedPreferences("UserInfo", 0); // 0 - for private mode

        YouruserName = pref.getString("user_name", null);

        Intent intent = getIntent();
        String Id_Customer = intent.getStringExtra("id_customer");
        String Customer_Code = intent.getStringExtra("customer_code");
        //intent.getStringExtra("MembershipID");
        String FullName=intent.getStringExtra("FullName");
        String CustomerType=intent.getStringExtra("CustomerType");
        String Email=intent.getStringExtra("Email");
        String Phone=intent.getStringExtra("Phone");
        String Gender=intent.getStringExtra("Gender");
        String DateOfBirth=intent.getStringExtra("DateOfBirth");
        String MaritalStatus=intent.getStringExtra("MaritalStatus");
        String AnniversaryDate=intent.getStringExtra("AnniversaryDate");

        String Balance=intent.getStringExtra("Balance");
        String Point=intent.getStringExtra("Point");
        String profile_img=intent.getStringExtra("profile_img");

        String profile_url = "https://s3.amazonaws.com/posspot/"+YouruserName+"/customer_files/"+profile_img;


        CustomerCode = (TextView) findViewById(R.id.customerCodeTV);
        FullNameT = (TextView)findViewById(R.id.FullNameTV);
        CustomerTypeT = (TextView)findViewById(R.id.CustomerTypeTV);
        EmailT = (TextView)findViewById(R.id.EmailTV);
        PhoneT = (TextView)findViewById(R.id.PhoneTV);
        GenderT = (TextView)findViewById(R.id.GenderTV);


        DateOfBirthT = (TextView)findViewById(R.id.DateOfBirthTV);
        MaritalStatusT = (TextView)findViewById(R.id.MaritalStatusTV);
        AnniversaryDateT = (TextView)findViewById(R.id.AnniversaryDateTV);

        BalanceT = (TextView)findViewById(R.id.balanceTV);
        PointsT = (TextView)findViewById(R.id.pointTV);

        dp = (ImageView) findViewById(R.id.dpID) ;




        CustomerCode.setText("MembershipID : "+ Id_Customer);
        FullNameT.setText("FullName : "+FullName);
        CustomerTypeT.setText("CustomerType : "+CustomerType);
        EmailT.setText("Email : "+Email);
        PhoneT.setText("Phone : "+Phone);
        GenderT.setText("Gender : " +Gender);
        DateOfBirthT.setText("DateOfBirth : "+DateOfBirth);
        MaritalStatusT.setText("MaritalStatus : " +MaritalStatus);
        AnniversaryDateT.setText("Anniversary Date : "+AnniversaryDate);


        BalanceT.setText("Balance : "+Balance);
        PointsT.setText("Point : "+Point);

        Picasso.get().load(profile_url).into(dp);


    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
