package syntechbd.com.posspot;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog; 
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import syntechbd.com.posspot.app.AppController;
import syntechbd.com.posspot.modal.DashboardModals;

import static syntechbd.com.posspot.app.AppController.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    EditText username, password;
    Button login;
    static TextView statusTV;
    private String baseUrl;
    private String YouruserName;
    private String YourpassWorD;
    RequestQueue requestQueue;
    private ProgressDialog pDialog;
    private String jsonResponse;
    private DashboardModals dashboardModals;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View v = inflater.inflate(R.layout.fragment_login, container, false);

        //  baseUrl = "http://posspot.com/api/authentication/check_user";
        //baseUrl = "http://posspot.com/api/authentication/check_user_auth";
        baseUrl = "http://posspot.com/api/authentication/check_user_authentication?userName=pos01&passWorD=12345";
        username = (EditText) v.findViewById(R.id.unameET);
        password = (EditText) v.findViewById(R.id.passET);
        login = (Button) v.findViewById(R.id.logMeInBTN);
        statusTV = (TextView) v.findViewById(R.id.statusTV);

        requestQueue = Volley.newRequestQueue(getContext());


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                YouruserName = username.getText().toString().trim();
                YourpassWorD = password.getText().toString().trim();

                pDialog = new ProgressDialog(getContext());
                pDialog.setMessage("Logging...");
                pDialog.setCancelable(false);
                makeJsonObjectRequest();

            }
        });
        return v;
    }


    private void makeJsonObjectRequest() {

        showpDialog();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                baseUrl, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject info = response.getJSONObject("response");
                    String user_name = info.getString("user_name");
                    String user_id = info.getString("user_id");
                    String user_full_name = info.getString("user_full_name");
                    String user_email = info.getString("user_email");
                    String user_phone = info.getString("user_phone");
                    String store_id = info.getString("store_id");
                    String store_name = info.getString("store_name");
                    String station_id = info.getString("station_id");
                    String station_name = info.getString("station_name");
                    String logged_in = info.getString("logged_in");

                    JSONObject InnerInfo = info.getJSONObject("info");


                    long total_stock = Long.parseLong(InnerInfo.getString("total_stock"));
                    int total_station = Integer.parseInt(InnerInfo.getString("total_station"));
                    int used_station = Integer.parseInt(InnerInfo.getString("used_station"));
                    int total_store = Integer.parseInt(InnerInfo.getString("total_store"));
                    int used_store = Integer.parseInt(InnerInfo.getString("used_store"));
                    int total_user = Integer.parseInt(InnerInfo.getString("total_user"));
                    int used_user = Integer.parseInt(InnerInfo.getString("used_user"));


                    dashboardModals.setUser_name(user_name);
                    dashboardModals.setUser_id(user_id);
                    dashboardModals.setUser_full_name(user_full_name);
                    dashboardModals.setUser_email(user_email);
                    dashboardModals.setUser_phone(user_phone);
                    dashboardModals.setStore_id(store_id);
                    dashboardModals.setStore_name(store_name);
                    dashboardModals.setStation_id(station_id);
                    dashboardModals.setStation_name(station_name);
                    dashboardModals.setLogged_in(logged_in);

                    jsonResponse = "";
                    jsonResponse += "user_full_name: " + user_full_name + "\n\n";
                    jsonResponse += "total_stock: " + total_stock + "\n\n";
                    jsonResponse += "total_station: " + total_station + "\n\n";

                    statusTV.setText(jsonResponse);

                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.mainContainer,new DashBoardFragment());
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);// it will anim while calling fragment.
                    fragmentTransaction.addToBackStack(null); // it will manage back stack of fragments.
                    fragmentTransaction.commit();



                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();


                }
                hidepDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                statusTV.setText("Wrong UserName or PassWord! Try Again!");


                // hide the progress dialog
                hidepDialog();
            }
        }) {
//            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                String userName = "pos01";
//                String password = "12345";
//                params.put("userName", userName);
//                params.put("passWorD", password);
//                return params;
//
//            }


            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();


//                String userName = "pos01";
//                String password = "12345";


//                headers.put("userName", userName);
//                headers.put("passWorD", password);
//                headers.put("Content-Type", "application/json");
//                 headers.put("Accept", "application/json");
//                headers.put("Host", "sms.posspot.com");
//               headers.put("Accept-Encoding", "utf-8");
                String creds = String.format("%s:%s", YouruserName, YourpassWorD);
                System.out.println(creds);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);
                return headers;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue

        AppController.getInstance().getRequestQueue().getCache().remove(baseUrl);
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
