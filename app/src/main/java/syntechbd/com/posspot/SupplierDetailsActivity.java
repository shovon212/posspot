package syntechbd.com.posspot;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class SupplierDetailsActivity extends AppCompatActivity {

    TextView id_supplier,supplier_code,supplier_name,contact_person,phone,email,div_id,dist_id,upz_id, unn_id, city_id,area_id,profile_img,post_code,vat_reg_no,note,addr_line_1,balance,dtt_add,uid_add,dtt_mod,uid_mod,status_id,version,stores;

    ImageView dp;

    SharedPreferences pref;
    String YouruserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isNetworkConnected()){
            Toast.makeText(this,"Connection Lost !! Check your internet connection! ",Toast.LENGTH_LONG).show();
        }
        setContentView(R.layout.activity_supplier_details);

        pref = getSharedPreferences("UserInfo", 0); // 0 - for private mode

        YouruserName = pref.getString("user_name", null);

        Intent intent = getIntent();
        String id_supplierS = intent.getStringExtra("id_supplier");
        String supplier_codeS = intent.getStringExtra("supplier_code");
        //intent.getStringExtra("MembershipID");
        String supplier_nameS=intent.getStringExtra("supplier_name");
        String contact_personS=intent.getStringExtra("contact_person");
        String emailS=intent.getStringExtra("email");
        String phoneS=intent.getStringExtra("phone");
        String div_idS=intent.getStringExtra("div_id");
        String dist_idS=intent.getStringExtra("dist_id");
        String upz_idS=intent.getStringExtra("upz_id");
        String unn_idS=intent.getStringExtra("unn_id");

        String city_idS=intent.getStringExtra("city_id");
        String area_idS=intent.getStringExtra("area_id");
        String profile_imgS=intent.getStringExtra("profile_img");
        String post_codeS=intent.getStringExtra("post_code");
        String vat_reg_noS=intent.getStringExtra("vat_reg_no");
        String noteS=intent.getStringExtra("note");
        String addr_line_1S=intent.getStringExtra("addr_line_1");
        String balanceS=intent.getStringExtra("balance");
        String dtt_addS=intent.getStringExtra("dtt_add");
        String uid_addS=intent.getStringExtra("uid_add");
        String dtt_modS=intent.getStringExtra("dtt_mod");
        String uid_modS=intent.getStringExtra("uid_mod");
        String status_idS=intent.getStringExtra("status_id");
        String versionS=intent.getStringExtra("version");
        String storesS=intent.getStringExtra("stores");

        String profile_url = "https://s3.amazonaws.com/posspot/"+YouruserName+"/customer_files/"+profile_imgS;


        id_supplier = (TextView) findViewById(R.id.id_supplierTV);
        supplier_code = (TextView)findViewById(R.id.supplier_codeTV);
        supplier_name = (TextView)findViewById(R.id.supplier_nameTV);
        contact_person = (TextView)findViewById(R.id.contact_personTV);
        phone = (TextView)findViewById(R.id.phoneTV);
        email = (TextView)findViewById(R.id.emailTV);
        div_id = (TextView)findViewById(R.id.div_idTV);
        dist_id = (TextView)findViewById(R.id.dist_idTV);
        upz_id = (TextView)findViewById(R.id.upz_idTV);
        unn_id = (TextView)findViewById(R.id.unn_idTV);
        city_id = (TextView)findViewById(R.id.city_idTV);
        area_id = (TextView)findViewById(R.id.area_idTV);
        post_code = (TextView)findViewById(R.id.post_codeTV);
        vat_reg_no = (TextView)findViewById(R.id.vat_reg_noTV);
        note = (TextView)findViewById(R.id.noteTV);
        addr_line_1 = (TextView)findViewById(R.id.addr_line_1TV);
        balance = (TextView)findViewById(R.id.balanceTV);
        dtt_add = (TextView)findViewById(R.id.dtt_addTV);
        uid_add = (TextView)findViewById(R.id.uid_addTV);
        dtt_mod = (TextView)findViewById(R.id.dtt_modTV);
        uid_mod = (TextView)findViewById(R.id.uid_modTV);
        status_id = (TextView)findViewById(R.id.status_idTV);
        version = (TextView)findViewById(R.id.versionTV);
        stores = (TextView)findViewById(R.id.storesTV);

        dp = (ImageView) findViewById(R.id.dpID) ;
        id_supplier.setText("Supplier ID : "+ id_supplierS);
        supplier_code.setText("Supplier Code : "+supplier_codeS);
        supplier_name.setText("Supplier Name : "+supplier_nameS);
        email.setText("Email : "+emailS);
        phone.setText("Phone : "+phoneS);
        div_id.setText("Division ID : " +div_idS);
        dist_id.setText("District ID : "+dist_idS);
        upz_id.setText("Upazilla ID : " +upz_idS);
        unn_id.setText("Union ID : " +unn_idS);
        city_id.setText("City ID : " +city_idS);
        area_id.setText("Area ID : " +area_idS);
        post_code.setText("Post Code : " +post_codeS);
        vat_reg_no.setText("Vat Reg No : " +vat_reg_noS);
        note.setText("Note : " +noteS);
        addr_line_1.setText("Address Line 1 : " +addr_line_1S);
        balance.setText("Balance : " +balanceS);
        dtt_add.setText("Date Added : " +dtt_addS);
        uid_mod.setText("UID Mod : " +uid_modS);
        status_id.setText("Status ID : "+status_idS);
        contact_person.setText("Contact Person :" + contact_personS);
        dtt_mod.setText("Date Mod :"+ dtt_modS);
        uid_add.setText("UID Add :"+uid_addS);


        version.setText("Version : "+versionS);
        stores.setText("Stores : "+storesS);

        Picasso.get().load(profile_url).into(dp);


    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
