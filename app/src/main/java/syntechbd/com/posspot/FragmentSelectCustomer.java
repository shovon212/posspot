package syntechbd.com.posspot;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import syntechbd.com.posspot.adapter.AddToCartAdapter;
import syntechbd.com.posspot.adapter.CustomListAdapter;
import syntechbd.com.posspot.app.AppController;
import syntechbd.com.posspot.app.AppSingleton;
import syntechbd.com.posspot.app.DBHelper;
import syntechbd.com.posspot.modal.CartModal;
import syntechbd.com.posspot.modal.Cutomers;
import syntechbd.com.posspot.modal.StockProductsModal2;

import static syntechbd.com.posspot.SaleNowActivity.cart_total;
import static syntechbd.com.posspot.app.AppController.TAG;


public class FragmentSelectCustomer extends Fragment {


    private static String TAG = FragmentSelectProduct.class.getSimpleName();

    RequestQueue requestQueue;

    static final int INTERNET_REQ = 23;
    static final String REQ_TAG = "VACTIVITY";


    //RadioButton p, b;
    Button next, next2;
    public static AutoCompleteTextView ProductNameATV;
    String productlistURL, addtoCurtURL, YouruserName, Store_Id,productId,ProductNameInET;
    SharedPreferences pref;
    SharedPreferences prefCUSTOMER;
    SharedPreferences.Editor editor ;
    Context context;

    TextView CustomerName,PhoneNoOfCus,BalanceOfCus,PointsOfcus,CusType;


    //ArrayList<AddTOCartModal> productList = new ArrayList<AddTOCartModal>();
    ArrayList<Cutomers> productList = new ArrayList<Cutomers>();

    ArrayAdapter<String> collectionOfProductNameAdapter;
    private ArrayList<String> collectionOfProductName = new ArrayList<>();
    private ArrayList<String> collectionOfProductNameCUSTOM = new ArrayList<>();

    public static ArrayList<String> customersList;
    public static String productBatch, selection;

    //todo...................
    public static String discount,def_vat;

    public static String Name = "";
    public static String CustomersBalance = "";
    public static String PhoneNo = "";
    public static String CustomersPoints = "";
    String typeCus;
    public static String CUSTOMER_ID;
    String customer_code;
    public static DBHelper db;


    public FragmentSelectCustomer() {
        // Required empty public constructor
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_select_customer, container, false);

        context = getContext();
        db = new DBHelper(context);
        customersList = new ArrayList<>();

        pref = getActivity().getSharedPreferences("UserInfo", 0); // 0 - for private mode
        prefCUSTOMER = getActivity().getSharedPreferences("CustomerInfo", 0); // 0 - for private mode


         YouruserName = pref.getString("user_name", null);

         Store_Id = pref.getString("store_id", null);


        // productlistURL = "http://" + YouruserName + ".posspot.com/api/sales_api/sale_products_auto_list";
        // addtoCurtURL = "http://" + YouruserName + ".posspot.com/api/sales_api/add_cart_for_sales";
        loadAutoSuggestionData();
        //AddToCartData(productCode,addtoCurtURL);


        ProductNameATV = (AutoCompleteTextView) v.findViewById(R.id.productNameATV);

        CustomerName = (TextView) v.findViewById(R.id.custNameTV);
        PhoneNoOfCus= (TextView) v.findViewById(R.id.MobileNoTV);
        CusType = (TextView) v.findViewById(R.id.cusTypeTV);
        BalanceOfCus= (TextView) v.findViewById(R.id.balanceTV);
        PointsOfcus= (TextView) v.findViewById(R.id.pointsTV) ;

         ProductNameATV.setThreshold(1);

        collectionOfProductNameAdapter = new ArrayAdapter<String>
                (context, R.layout.dropdown, collectionOfProductName);

        CustomListAdapter adapter = new CustomListAdapter(context,
                R.layout.autocompletetextview, collectionOfProductName);
//
//        ProductNameATV.setAdapter(adapter);
//        ProductNameATV.setOnItemClickListener(onItemClickListener);
//
//     AdapterView.OnItemClickListener onItemClickListener =
//            new AdapterView.OnItemClickListener(){
//                @Override
//                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                    Toast.makeText(context,
//                            "Clicked item from auto completion list "
//                                    + adapterView.getItemAtPosition(i)
//                            , Toast.LENGTH_SHORT).show();
//                }
//            };

        ProductNameATV.setAdapter(collectionOfProductNameAdapter);
//        ProductNameATV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                ProductNameATV.requestFocus();
//                ProductNameATV.setCursorVisible(true);
//
//                listView.invalidateViews();
//
//
//                try {
//                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//        });

        ProductNameATV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selection = (String) adapterView.getItemAtPosition(i);
                 List<String> elephantList = Arrays.asList(selection.split(","));
             //   Log.e("elephantList:",elephantList.toString());


                Name = elephantList.get(0);
                CustomersBalance =elephantList.get(3);
                PhoneNo = elephantList.get(2);
                CustomersPoints = elephantList.get(4);
                typeCus= elephantList.get(1);
                CUSTOMER_ID= elephantList.get(5);
                customer_code = elephantList.get(6);



                ProductNameATV.setText("");

                CusType.setVisibility(View.VISIBLE);
                CustomerName.setText(Name);
                PhoneNoOfCus.setText(PhoneNo);
                CusType .setText(typeCus);
                BalanceOfCus.setText(CustomersBalance);
                PointsOfcus.setText(CustomersPoints);

                //special_dis,cus_dis,pur_dis,card_dis,grand_total,customer_code,paid_amt
                db.insertCustomerInfo("1",CUSTOMER_ID,Name,typeCus,PhoneNo,CustomersBalance,CustomersPoints,"0","0","0","0","0",customer_code,"0","2","2","2","2",cart_total);

            //    Log.e("CUSTOMER_ID:",CUSTOMER_ID);


            }
        });





//        collectionOfProductNameAdapter = new ArrayAdapter<String>
//                (context,android.R.layout.select_dialog_item, collectionOfProductName);




        next = (Button) v.findViewById(R.id.nextBTN);
        next2 = (Button) v.findViewById(R.id.nextBTN2);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        CusType.setVisibility(View.GONE);

//        SrcBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                CusType.setVisibility(View.VISIBLE);
//
//                editor = prefCUSTOMER.edit();
//
//                editor.putString("CUSTOMER_ID", CUSTOMER_ID);  // Saving string
//                editor.putString("CustomersBalance", CustomersBalance);
//                editor.putString("CustomersPoints", CustomersPoints);
//                editor.apply();
//
//
//
//
//            }
//        });


        requestQueue = AppSingleton.getInstance(this.getContext())
                .getRequestQueue();


        return v;
    }

//    private AdapterView.OnItemClickListener onItemClickListener =
//            new AdapterView.OnItemClickListener(){
//                @Override
//                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                    Log.e("clicked",adapterView.getItemAtPosition(i).toString());
//                }
//            };


    @Override
    public void onStop() {
        super.onStop();
        if (requestQueue != null) {
            requestQueue.cancelAll(REQ_TAG);
        }
    }


    public void loadAutoSuggestionData() {

        String m = "http://";
        String urlJsonObj= m.concat(YouruserName+ ".posspot.com/api/DP_api/customer_view");

        // String urlJsonObj = "http://pos01.posspot.com/api/customer_view";
        // done

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlJsonObj, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONArray info = response.getJSONArray("response");

                    for (int i = 0; i < info.length(); i++) {

                        JSONObject jsonObject = info.getJSONObject(i);

                        Cutomers sched = new Cutomers();

                        sched.setId_customer(jsonObject.getString("id_customer"));
                        sched.setCustomer_code(jsonObject.getString("customer_code"));
                        sched.setCustomer_type_id(jsonObject.getString("customer_type_id"));
                        sched.setStore_id(jsonObject.getString("store_id"));
                        sched.setFull_name(jsonObject.getString("full_name"));
                        sched.setEmail(jsonObject.getString("email"));
                        sched.setPhone(jsonObject.getString("phone"));
                        sched.setGender(jsonObject.getString("gender"));
                        sched.setMarital_status(jsonObject.getString("marital_status"));
                        sched.setSpouse_name(jsonObject.getString("spouse_name"));
                        sched.setBirth_date(jsonObject.getString("birth_date"));
                        sched.setAnniversary_date(jsonObject.getString("anniversary_date"));
                        sched.setProfile_img(jsonObject.getString("profile_img"));
                        sched.setPoints(jsonObject.getString("points"));
                        sched.setBalance(jsonObject.getString("balance"));
                        sched.setDtt_add(jsonObject.getString("dtt_add"));
                        sched.setUid_add(jsonObject.getString("uid_add"));
                        sched.setDtt_mod(jsonObject.getString("dtt_mod"));
                        sched.setUid_mod(jsonObject.getString("uid_mod"));
                        sched.setStatus_id(jsonObject.getString("status_id"));
                        sched.setVersion(jsonObject.getString("version"));
                        sched.setCustomer_type_name(jsonObject.getString("customer_type_name"));

                        //productList.add(sched);
                        // collectionOfProductName.add(sched.getFull_name());

                        //customersList.add(i,sched.getFull_name()+","+sched.getCustomer_type_name()+","+sched.getPhone()+","+sched.getBalance()+","+sched.getPoints()); special_dis,cus_dis,pur_dis,card_dis,grand_total,customer_code,paid_amt
                        collectionOfProductName.add(sched.getFull_name()+","+sched.getCustomer_type_name()+","+sched.getPhone()+","+sched.getBalance()+","+sched.getPoints()+","+sched.getId_customer()+","+sched.getCustomer_code());
                        collectionOfProductNameCUSTOM.add(sched.getFull_name() + "," + sched.getCustomer_type_name() + "," + sched.getPhone() + "," + sched.getBalance() + "," + sched.getPoints() + "," + sched.getId_customer() + "," + sched.getCustomer_code());



                        // retrieve the values like this so on..

                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                // hide the progress dialog
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);


    }




}