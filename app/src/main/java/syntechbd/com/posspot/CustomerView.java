package syntechbd.com.posspot;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import syntechbd.com.posspot.adapter.CustomerViewAdapter;
import syntechbd.com.posspot.app.AppController;
import syntechbd.com.posspot.modal.Cutomers;

import static syntechbd.com.posspot.app.AppController.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerView extends Fragment implements
        SearchView.OnQueryTextListener {


    ListView listView;
    CustomerViewAdapter adapter;
    ArrayList<Cutomers> productList = new ArrayList<Cutomers>();
    //Based on the search string, only filtered products will be moved here from productResults
    // SearchView search,phoneNoSV;
    EditText custNameInCustomerViewTV;

    SharedPreferences pref;
    String YouruserName;


    public CustomerView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (!isNetworkConnected()){
            Toast.makeText(getContext(),"Connection Lost !! Check your internet connection! ",Toast.LENGTH_LONG).show();
        }
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_customer_view, container, false);


        pref = getActivity().getSharedPreferences("UserInfo", 0); // 0 - for private mode

        YouruserName = pref.getString("user_name", null);


        getActivity().setTitle("Customers");

        makeJsonObjecttRequestForDailySales();

        listView = (ListView) v.findViewById(R.id.AllCustomersLV);
        listView.setTextFilterEnabled(true);
        adapter = new CustomerViewAdapter(getActivity(), productList);
        listView.setTextFilterEnabled(true);
        listView.setAdapter(adapter);
        listView.setVisibility(View.INVISIBLE);

//        search=(android.support.v7.widget.SearchView) v.findViewById(R.id.searchView);
//        phoneNoSV=(android.support.v7.widget.SearchView) v.findViewById(R.id.phoneNoSV);

        custNameInCustomerViewTV = (EditText) v.findViewById(R.id.custNameInCustomerViewTV);

        Spinner customerTypeSP = (Spinner) v.findViewById(R.id.spinnerInCustomerViewTV);

//        search.setBackgroundResource(R.drawable.texfield_searchview_holo_light);
//        phoneNoSV.setBackgroundResource(R.drawable.texfield_searchview_holo_light);


        // search.setOnQueryTextListener(this);
        //  phoneNoSV.setOnQueryTextListener(this);


        customerTypeSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String text = adapterView.getItemAtPosition(i).toString();
                if (!text.equals("Select One")) {
                    CustomerView.this.adapter.getFilter().filter(text);
                    listView.setVisibility(View.VISIBLE);
                }

            }

            public void onNothingSelected(AdapterView<?> arg0) {
                CustomerView.this.adapter.getFilter().filter("");
            }



        });


        custNameInCustomerViewTV.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub
                CustomerView.this.adapter.getFilter().filter(arg0);
                listView.setVisibility(View.VISIBLE);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }
        });

        // Capture Text in EditText
//        search.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener()
//        {
//
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                // TODO Auto-generated method stub
//
//                //Toast.makeText(activity, String.valueOf(hasFocus),Toast.LENGTH_SHORT).show();
//            }
//        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Cutomers cutomers = (Cutomers) parent.getItemAtPosition(position);
                Intent intent = new Intent(v.getContext(), CustomerDetailsActivity.class);
                intent.putExtra("id_customer", cutomers.getId_customer());
                intent.putExtra("customer_code", cutomers.getCustomer_code());

                intent.putExtra("FullName", cutomers.getFull_name());
                intent.putExtra("CustomerType", cutomers.getCustomer_type_name());
                intent.putExtra("Email", cutomers.getEmail());
                intent.putExtra("Phone", cutomers.getPhone());
                intent.putExtra("Gender", cutomers.getGender());
                intent.putExtra("DateOfBirth", cutomers.getBirth_date());
                intent.putExtra("MaritalStatus", cutomers.getMarital_status());
                intent.putExtra("AnniversaryDate", cutomers.getAnniversary_date());

                intent.putExtra("Balance", cutomers.getBalance());
                intent.putExtra("Point", cutomers.getPoints());
                intent.putExtra("profile_img", cutomers.getProfile_img());

                //Toast.makeText(getContext(),cutomers.getCustomer_type_name(),Toast.LENGTH_LONG).show();
                startActivity(intent);

            }
        });


        return v;
    }


    public void makeJsonObjecttRequestForDailySales() {

        String m = "http://";
        String urlJsonObj= m.concat(YouruserName+ ".posspot.com/api/DP_api/customer_view");

       // String urlJsonObj = "http://pos01.posspot.com/api/customer_view";
        // done

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlJsonObj, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONArray info = response.getJSONArray("response");

                    for (int i = 0; i < info.length(); i++) {

                        JSONObject jsonObject = info.getJSONObject(i);

                        Cutomers sched = new Cutomers();

                        sched.setId_customer(jsonObject.getString("id_customer"));
                        sched.setCustomer_code(jsonObject.getString("customer_code"));
                        sched.setCustomer_type_id(jsonObject.getString("customer_type_id"));
                        sched.setStore_id(jsonObject.getString("store_id"));
                        sched.setFull_name(jsonObject.getString("full_name"));
                        sched.setEmail(jsonObject.getString("email"));
                        sched.setPhone(jsonObject.getString("phone"));
                        sched.setGender(jsonObject.getString("gender"));
                        sched.setMarital_status(jsonObject.getString("marital_status"));
                        sched.setSpouse_name(jsonObject.getString("spouse_name"));
                        sched.setBirth_date(jsonObject.getString("birth_date"));
                        sched.setAnniversary_date(jsonObject.getString("anniversary_date"));
                        sched.setProfile_img(jsonObject.getString("profile_img"));
                        sched.setPoints(jsonObject.getString("points"));
                        sched.setBalance(jsonObject.getString("balance"));
                        sched.setDtt_add(jsonObject.getString("dtt_add"));
                        sched.setUid_add(jsonObject.getString("uid_add"));
                        sched.setDtt_mod(jsonObject.getString("dtt_mod"));
                        sched.setUid_mod(jsonObject.getString("uid_mod"));
                        sched.setStatus_id(jsonObject.getString("status_id"));
                        sched.setVersion(jsonObject.getString("version"));
                        sched.setCustomer_type_name(jsonObject.getString("customer_type_name"));

                        productList.add(sched);

                        // retrieve the values like this so on..

                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                // hide the progress dialog
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);


    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        adapter.getFilter().filter(s);
        return false;
    }

    @Override
    public void onResume() {

        super.onResume();



        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            private Boolean exit = false;
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button


                    if (exit) {
                        getActivity().finish(); // finish activity
                    } else {
                        Toast.makeText(getActivity(),"Press Back again to Exit", Toast.LENGTH_SHORT).show();
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 3 * 1000);

                    }





                    return true;

                }

                return false;
            }
        });
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }




}
