package syntechbd.com.posspot;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import syntechbd.com.posspot.adapter.PurchaseReportAdapter;
import syntechbd.com.posspot.adapter.SupplierPayableReportAdapter;
import syntechbd.com.posspot.app.AppController;
import syntechbd.com.posspot.modal.PurchaseReportModal;
import syntechbd.com.posspot.modal.SupplierPayableReportModal;


/**
 * A simple {@link Fragment} subclass.
 */
public class AFragment extends Fragment {
    private static String TAG = SupplierPayableReport_Fragment.class.getSimpleName();


    RadioButton p,b;




    int day, date;

    Button Search,next,next2;
    ImageButton cAm;

    ListView listView;
    SupplierPayableReportAdapter adapter;
    ArrayList<SupplierPayableReportModal> productList = new ArrayList<SupplierPayableReportModal>();
    ArrayList<String> storeList = new ArrayList<>();
    ArrayList<String> supplierList = new ArrayList<>();
    ArrayList<String> timeRangeItemList = new ArrayList<>();


    String FromDateS, ToDateS,SupplierIdS;
    String StoreName = "1";
    String StoreListURL, SuppliersListURL;

    Spinner customerTypeSP, timeRangeSpinner, SupplierSpinner;

    SharedPreferences pref;
    String YouruserName;

    ProgressDialog progressdialog;


    public AFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (!isNetworkConnected()){
            Toast.makeText(getContext(),"Connection Lost !! Check your internet connection! ",Toast.LENGTH_LONG).show();
        }

        pref = getActivity().getSharedPreferences("UserInfo", 0); // 0 - for private mode


        YouruserName = pref.getString("user_name", null);



        StoreListURL = "http://"+YouruserName+".posspot.com/api/DP_api/stores";
        SuppliersListURL = "http://"+YouruserName+".posspot.com/api/DP_api/suppliers";

        timeRangeItemList.add("Today");
        timeRangeItemList.add("This Week");
        timeRangeItemList.add("This Month");
        timeRangeItemList.add("Past 3 Month");
        timeRangeItemList.add("Specific Time");


        getActivity().setTitle("Supplier Payable Report");

        View v = inflater.inflate(R.layout.fragment_a, container, false);

        p = (RadioButton) v.findViewById(R.id.productOrCOdeSelectenBTN);
        b = (RadioButton) v.findViewById(R.id.barCodeSelectedBTN);
        cAm = (ImageButton) v.findViewById(R.id.cam);

        final ImageButton cameraBtn= (ImageButton) v.findViewById(R.id.cam);



//        REFRESH.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                cAm.setVisibility(View.VISIBLE);
//            }
//        });


        if(p.isChecked()){
            cameraBtn.setVisibility(View.GONE);

        }else if(b.isChecked()){

            cameraBtn.setVisibility(View.VISIBLE);
        }

        p.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.setChecked(false);
                cameraBtn.setVisibility(View.GONE);
            }
        });

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p.setChecked(false);
                cameraBtn.setVisibility(View.VISIBLE);
            }
        });




        Search = (Button) v.findViewById(R.id.btnSearchInSellesInvoices);
        listView = (ListView) v.findViewById(R.id.SalesReportLV);
        customerTypeSP = (Spinner) v.findViewById(R.id.storeListSP);
        timeRangeSpinner = (Spinner) v.findViewById(R.id.timeRangeSP);
        SupplierSpinner = (Spinner) v.findViewById(R.id.supplierListSP);
        next = (Button) v.findViewById(R.id.nextBTN);
        next2 = (Button) v.findViewById(R.id.nextBTN2);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 cAm.setVisibility(View.INVISIBLE);
            }
        });
        next2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 cAm.setVisibility(View.VISIBLE);
            }
        });




        loadSpinnerData(StoreListURL);
        loadSpinnerDataForSuppliers(SuppliersListURL);


        ArrayAdapter<String> timeAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, timeRangeItemList);
        timeAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        timeRangeSpinner.setAdapter(timeAdapter);


        timeRangeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                switch (i) {
                    case 0:

                        Date c = Calendar.getInstance().getTime();
                        System.out.println("Current time => " + c);

                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        FromDateS = df.format(c) + "%2000:00:00";
                        ToDateS = df.format(c) + "%2023:59:59";


                        break;

                    case 1:


                        Calendar calendar = Calendar.getInstance();

                        Date date1 = calendar.getTime();
                        SimpleDateFormat checkformate = new SimpleDateFormat("MM/yyyy");
                        String currentCheckdate = checkformate.format(date1);

                        int weekn = calendar.get(Calendar.WEEK_OF_MONTH);
                        int month = calendar.get(Calendar.MONTH);
                        int year = calendar.get(Calendar.YEAR);

                        calendar.clear();
                        calendar.setFirstDayOfWeek(Calendar.SATURDAY);
                        calendar.set(Calendar.WEEK_OF_MONTH, weekn);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.YEAR, year);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

                        Date datef = calendar.getTime();
                        Long time = calendar.getTimeInMillis() + 518400000L;
                        Date dateL = new Date(time);
                        String firtdate = simpleDateFormat.format(datef);
                        String lastdate = simpleDateFormat.format(dateL);
                        String firtdateCheck = checkformate.format(datef);
                        String lastdateCheck = checkformate.format(dateL);


                        if (!firtdateCheck.toString().equalsIgnoreCase(currentCheckdate)) {
                            firtdate = calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH) + "-" + "1";
                        }

                        if (!lastdateCheck.toString().equalsIgnoreCase(currentCheckdate)) {

                            int ma = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                            lastdate = calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH) + "-" + String.valueOf(ma);
                        }
                        ToDateS = lastdate.toString() + "%2000:00:00";
                        FromDateS = firtdate.toString() + "%2023:59:59";


                        break;

                    case 2:
                        Calendar calendar2 = Calendar.getInstance();
                        calendar2.add(Calendar.MONTH, 0);
                        calendar2.set(Calendar.DATE, calendar2.getActualMinimum(Calendar.DAY_OF_MONTH));
                        Date nextMonthFirstDay = calendar2.getTime();
                        calendar2.set(Calendar.DATE, calendar2.getActualMaximum(Calendar.DAY_OF_MONTH));
                        Date nextMonthLastDay = calendar2.getTime();

                        SimpleDateFormat checkformatee = new SimpleDateFormat("yyyy-MM-dd");
                        FromDateS = checkformatee.format(nextMonthFirstDay) + "%2000:00:00";
                        ToDateS = checkformatee.format(nextMonthLastDay) + "%2023:59:59";

                        break;
                    case 3:
                        Calendar calendar3 = Calendar.getInstance();
                        calendar3.add(Calendar.MONTH, -3);
                        calendar3.set(Calendar.DATE, calendar3.getActualMinimum(Calendar.DAY_OF_MONTH));
                        Date nextMonthFirstDay3 = calendar3.getTime();
                        calendar3.set(Calendar.DATE, calendar3.getActualMaximum(Calendar.DAY_OF_MONTH));
                        Date nextMonthLastDay3 = calendar3.getTime();

                        SimpleDateFormat checkformateee = new SimpleDateFormat("yyyy-MM-dd");
                        FromDateS = checkformateee.format(nextMonthFirstDay3) + "%2000:00:00";
                        //ToDateS = checkformateee.format(nextMonthLastDay3);


                        Calendar calendar4 = Calendar.getInstance();
                        calendar4.add(Calendar.MONTH, 0);
                        calendar4.set(Calendar.DATE, calendar4.getActualMinimum(Calendar.DAY_OF_MONTH));
                        Date nextMonthFirstDay4 = calendar4.getTime();
                        calendar4.set(Calendar.DATE, calendar4.getActualMaximum(Calendar.DAY_OF_MONTH));
                        Date nextMonthLastDay4 = calendar4.getTime();

                        SimpleDateFormat checkformateee4 = new SimpleDateFormat("yyyy-MM-dd");
                        // FromDateS = checkformateee4.format(nextMonthFirstDay4);
                        ToDateS = checkformateee4.format(nextMonthLastDay4) + "%2023:59:59";

                        break;


                    case 4:

                        calendar = Calendar.getInstance();
                        year = calendar.get(Calendar.YEAR);
                        month = calendar.get(Calendar.MONTH);
                        day = calendar.get(Calendar.DAY_OF_MONTH);
                        date = calendar.get(Calendar.DAY_OF_MONTH);


                        DatePickerDialog mdiDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                monthOfYear++;
                                //fromDate.setText(dayOfMonth+"/"+monthOfYear+"/"+year);
                                ToDateS = year + "-" + monthOfYear + "-" + dayOfMonth + "%2023:59:59";


                            }
                        }, year, month, date);
                        mdiDialog.setTitle("Select To Date :");
                        mdiDialog.show();


                        DatePickerDialog mdiDialog2 = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                monthOfYear++;
                                FromDateS = year + "-" + monthOfYear + "-" + dayOfMonth + "%2000:00:00";

                            }
                        }, year, month, date);
                        mdiDialog2.setTitle("Select From Date :");
                        mdiDialog2.show();


                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });


        customerTypeSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                StoreName = Integer.toString(i + 1);
                // Toast.makeText(getContext(), StoreName, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });
        SupplierSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SupplierIdS = Integer.toString(i + 3);
                // Toast.makeText(getContext(), StoreName, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });


        //makeJsonObjecttRequestForDailySales();


        // listView.setTextFilterEnabled(true);
        adapter = new SupplierPayableReportAdapter(getActivity(), productList);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
        productList.clear();


        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //next.setVisibility(View.GONE);
                cameraBtn.setVisibility(View.VISIBLE);

//                progressdialog = new ProgressDialog(getActivity());
//                progressdialog.setMessage("Searching...");
//                progressdialog.setCancelable(false);
//                progressdialog.show();
//
//                makeJsonObjecttRequestForDailySales(FromDateS, ToDateS, SupplierIdS, StoreName);
//
//                // listView.setTextFilterEnabled(true);
//
//
//                adapter = new SupplierPayableReportAdapter(getActivity(), productList);
//                adapter.notifyDataSetChanged();
//                listView.setAdapter(adapter);
//                productList.clear();

            }
        });


        return v;
    }


    private void loadSpinnerData(String url) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("response");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String country = jsonObject1.getString("store_name");

                        storeList.add(country);
                    }

                    ArrayAdapter<String> customerTypeAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, storeList);
                    customerTypeAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                    customerTypeSP.setAdapter(customerTypeAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void loadSpinnerDataForSuppliers(String url) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("response");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String country = jsonObject1.getString("supplier_name");

                        supplierList.add(country);
                    }

                    ArrayAdapter<String> customerTypeAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, supplierList);
                    customerTypeAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                    SupplierSpinner.setAdapter(customerTypeAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }


    public void makeJsonObjecttRequestForDailySales() {

        String urlJsonObj = "http://"+YouruserName+".posspot.com/api/DP_api/supplier_payable_reports?FromDate=&ToDate=&supplier_id=&store_id=";
        // done

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlJsonObj, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONArray info = response.getJSONArray("response");

                    for (int i = 0; i < info.length(); i++) {

                        JSONObject jsonObject = info.getJSONObject(i);

                        SupplierPayableReportModal sched = new SupplierPayableReportModal();

                        sched.setDue_amt(jsonObject.getString("due_amt"));
                        sched.setInvoice_no(jsonObject.getString("invoice_no"));
                        sched.setStore_id(jsonObject.getString("store_id"));
                        sched.setTot_amt(jsonObject.getString("tot_amt"));
                        sched.setPaid_amt(jsonObject.getString("paid_amt"));
                        sched.setDtt_add(jsonObject.getString("dtt_add"));
                        sched.setStore_name(jsonObject.getString("store_name"));
                        sched.setSupplier_name(jsonObject.getString("supplier_name"));

                        productList.add(sched);

                        // retrieve the values like this so on..

                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                // hide the progress dialog
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);


    }

    public void makeJsonObjecttRequestForDailySales(String fromDate, String toDate, String SupplierIdS, String StoreName) {

        // String urlJsonObj = "http://pos01.posspot.com/api/sell_reports?invoice_no=&store_id=&FromDate=&ToDate=&customer_name=";

        String incompleteUrl = "http://"+YouruserName+".posspot.com/api/DP_api/supplier_payable_reports?FromDate=";

        String fromDateAdd = incompleteUrl.concat(fromDate + "&ToDate=");
        String toDateAdd = fromDateAdd.concat(toDate + "&supplier_id=");
        String supplier_idAdd = toDateAdd.concat(SupplierIdS + "&store_id=");
        String urlJsonObj = supplier_idAdd.concat(StoreName);

        // Toast.makeText(getContext(), urlJsonObj, Toast.LENGTH_LONG).show();
        // done

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlJsonObj, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONArray info = response.getJSONArray("response");

                    for (int i = 0; i < info.length(); i++) {

                        JSONObject jsonObject = info.getJSONObject(i);

                        SupplierPayableReportModal sched = new SupplierPayableReportModal();


                        sched.setDue_amt(jsonObject.getString("due_amt"));
                        sched.setInvoice_no(jsonObject.getString("invoice_no"));
                        sched.setStore_id(jsonObject.getString("store_id"));
                        sched.setTot_amt(jsonObject.getString("tot_amt"));
                        sched.setPaid_amt(jsonObject.getString("paid_amt"));
                        sched.setDtt_add(jsonObject.getString("dtt_add"));
                        sched.setStore_name(jsonObject.getString("store_name"));
                        sched.setSupplier_name(jsonObject.getString("supplier_name"));


                        productList.add(sched);
                        progressdialog.setMessage("Collecting data...");

                        // retrieve the values like this so on..

                    }

                    progressdialog.dismiss();


                } catch (JSONException e) {
                    e.printStackTrace();


                }

                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                progressdialog.setMessage("No Data Found");
                progressdialog.dismiss();
                Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_SHORT).show();
                // hide the progress dialog
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);


    }


    @Override
    public void onResume() {

        super.onResume();


        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            private Boolean exit = false;

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    // handle back button


                    if (exit) {
                        getActivity().finish(); // finish activity
                    } else {
                        Toast.makeText(getActivity(), "Press Back again to Exit", Toast.LENGTH_SHORT).show();
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 3 * 1000);

                    }


                    return true;

                }

                return false;
            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

}



